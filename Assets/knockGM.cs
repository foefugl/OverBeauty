﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class knockGM : MonoBehaviour
{
    public Button[] my按鈕清單;
    public Text my查詢文字框, my查詢文字框2;
    public GameObject my無;
    public GameObject my查詢結果;

    public int my查詢關鍵字計數器;
    public void BTN_關鍵字按鈕按下時(string keyword) {
        if (my查詢關鍵字計數器 == 0)
        {
            my查詢關鍵字計數器++;
            my查詢文字框.text += keyword ;
            my查詢文字框2.text += keyword ;
        }
        else if (my查詢關鍵字計數器 == 1)
        {
            my查詢關鍵字計數器++;
            my查詢文字框.text += "+" + keyword;
            my查詢文字框2.text += "+" + keyword;
        }
    }
    public void BTN_搜尋() {
        my查詢關鍵字計數器 = 0;
        switch (my查詢文字框.text)
        {
            case "幼兒園+╰☆來自仙㊛星★╮": my查詢結果.SetActive(true); break;
            case "╰☆來自仙㊛星★╮+幼兒園": my查詢結果.SetActive(true); break;
            case "幼兒園 ": my查詢結果.SetActive(true); break;
            default:
                my無.SetActive(true);
                Debug.Log("資料庫內 沒有*" + my查詢文字框.text + "*的搜尋結果");
                break;
        }
    }
    public void SONIA_更新關鍵字清單() {
        my查詢文字框.text = "";
        my查詢文字框2.text = "";
        for (int a = 0; a < my按鈕清單.Length; a++) {
            my按鈕清單[a].gameObject.transform.GetChild(0).GetComponent<Text>().text = (a + 1).ToString() + "." + GM.ins.my關鍵字清單[a];
            if (GM.ins.isKeyWordList_是否被找到[a])
            {
                my按鈕清單[a].gameObject.SetActive(true);
            }
            else {
                my按鈕清單[a].gameObject.SetActive(false);
            }
        }
    }
    public void BTNClick(string myKeyword)
    {
        switch (myKeyword)
        {
            case "╰☆來自仙㊛星★╮":
                //isKeyWordList_是否被找到11
                //isKeyWordList_是否被找到+關鍵字編號
                //編號要跟GM清單的陣列編號相同
                //不然會開錯關鍵字
                //如果探索的關鍵字是男友
                //那就是 GM.ins.Data_存檔("isKeyWordList_是否被找到6");
                //因為GM裡面男友 的順序編號就是6號
                GM.ins.Data_存檔("isKeyWordList_是否被找到6");
                GM.ins.Data_更新();
                break;
            case "幼兒園":
                GM.ins.Data_存檔("isKeyWordList_是否被找到4");
                GM.ins.Data_更新();

                break;
            case "關鍵字B":

                break;
            default:
                break;
        }
    }
}
