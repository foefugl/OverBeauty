using UnityEngine;
using System.Collections;
using UnityEditor;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections.Generic;

public class UpdateSCOContent
{
    [MenuItem("Tools/Update PostContent [Chinese]", false, 11)]
    private static void Find()
    {   
        string[] guids = AssetDatabase.FindAssets("t:" + typeof(SCO_Post).Name);
        for (int i = 0; i < guids.Length; i++)
        {
            string path = AssetDatabase.GUIDToAssetPath(guids[i]);
            SCO_Post post = AssetDatabase.LoadAssetAtPath<SCO_Post>(path);
            
            Debug.Log(i + " / " + path);

        }
    }
    public static T[] GetAllInstances<T>() where T : ScriptableObject
    {
        string[] guids = AssetDatabase.FindAssets("t:" + typeof(T).Name);  //FindAssets uses tags check documentation for more info
        T[] a = new T[guids.Length];
        for (int i = 0; i < guids.Length; i++)         //probably could get optimized 
        {
            string path = AssetDatabase.GUIDToAssetPath(guids[i]);
            a[i] = AssetDatabase.LoadAssetAtPath<T>(path);
        }

        return a;

    }
}
