using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GM : MonoBehaviour
{
    public static GM ins;//ins 是簡寫  深入研究可以google "unity 單例"

    public int my紅粉數;
    public int my肉搜值;
    public int my公信力;
    public int my體力值;
    public int my線索等級;
    public int my證據等級;
    public int my情報留言;
    public bool is贏;
    public string my時間日期;
    public int myDate_年;
    public int myDate_月;
    public int myDate_日;

    public bool[] isKeyWordList_是否被找到;
    public string[] my關鍵字清單;
    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("GM");
        if (ins != null && ins != this) {
            Destroy(this.gameObject);
        }
        else {
            ins = this;
            DontDestroyOnLoad(this.gameObject);
        }

        Data_更新();

        //PlayerPrefs.set
    }

    // Update is called once per frame
    void Update()
    {
        
        if (Input.GetKeyDown(KeyCode.Q)) {
            Data_存檔("isKeyWordList_是否被找到0");
            Data_更新();
        }
        if (Input.GetKeyDown(KeyCode.W)) {
            Data_存檔("isKeyWordList_是否被找到1");
            Data_更新();
        }
        if (Input.GetKeyDown(KeyCode.E)) {
            Data_存檔("isKeyWordList_是否被找到2");
            Data_更新();
        }
        if (Input.GetKeyDown(KeyCode.R)) {
            Data_存檔("isKeyWordList_是否被找到3");
            Data_更新();
        }
        if (Input.GetKeyDown(KeyCode.T)) {
            Data_存檔("isKeyWordList_是否被找到4");
            Data_更新();
        }
        if (Input.GetKeyDown(KeyCode.Y)) {
            Data_存檔("isKeyWordList_是否被找到5");
            Data_更新();
        }
        if (Input.GetKeyDown(KeyCode.U)) {
            Data_存檔("isKeyWordList_是否被找到6");
            Data_更新();
        }
        if (Input.GetKeyDown(KeyCode.I)) {
            Data_存檔("isKeyWordList_是否被找到7");
            Data_更新();
        }
        if (Input.GetKeyDown(KeyCode.O)) {
            Data_存檔("isKeyWordList_是否被找到8");
            Data_更新();
        }
        if (Input.GetKeyDown(KeyCode.D)) {
            PlayerPrefs.DeleteAll();
            Data_更新();
        }
    }
    public void GM_找到新關鍵字(string id) {
        Data_存檔("isKeyWordList_是否被找到"+id);
        Data_更新();
    }
    public void Data_更新() {
        for (int a = 0; a < isKeyWordList_是否被找到.Length; a++) {
            if (PlayerPrefs.GetString("isKeyWordList_是否被找到" + a.ToString()) == "O")
            {
                isKeyWordList_是否被找到[a] = true;
            }
            else {
                isKeyWordList_是否被找到[a] = false;
            }
        }
    }
    public void Data_存檔(string PlayerPrefsKey) {
        PlayerPrefs.SetString(PlayerPrefsKey, "O");
    }
}
