using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyWordBTN : MonoBehaviour
{
    public void 關鍵字按鈕_被按下的時候呼叫(string id) {
        if (GM.ins == null)
        {
            Debug.LogWarning("QQ 找不到GM喔... (眼淚...");
        }
        else {
            GM.ins.GM_找到新關鍵字(id);
        }
    }
}
