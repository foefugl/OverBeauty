using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Darea : MonoBehaviour
{
    public BulletHome myBH;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        myBH.BulletAmount--;
        Destroy(collision.gameObject);
    }
}
