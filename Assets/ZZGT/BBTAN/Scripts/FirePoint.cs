using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FirePoint : MonoBehaviour
{
    public Transform myLookAtPoint, myBulletHome;
    public Rigidbody2D rb, rb_LAP;
    public float bulletForce = 20f;
    Vector2 mousePos;
    public Heart myHeart;
    // Start is called before the first frame update
    void Start()
    {
        //myHeart = GameObject.Find("Heart").GetComponent<Heart>();
        myShootingState = "ready";

        string bb = "今天天氣好熱";
        Debug.Log("bb.Substring(0, 1) = " + bb.Substring(0, 1));
        Debug.Log("bb.Substring(0, 3) = " + bb.Substring(0, 3));
    }
    public string myShootingState;//發射的狀態
    // Update is called once per frame
    void Update()
    {
        mousePos = rb_LAP.position;
        //mousePos = Input.mousePosition;
        if (Input.GetMouseButtonUp(0) && myShootingState == "ready") 
      //  if (Input.GetMouseButtonUp(0)) 
        {

            myShootTimer = myShootTime;//直接先射一顆
            myBulletAmount = mySayString[mySayIndex].Length;//把這彈夾有多少顆子彈先抓出來(把這句話有幾個字先抓出來)
            myShootingState = "ing";//切換狀態 變成發射中
        }
        ShootLogic();
    }

    public Transform firePoint;
    public GameObject myBullet;

    public string[] mySayString;

    float myShootTimer;
    [Header("發射間隔時間")]
    public float myShootTime;
    public int myBulletAmount;
    public int myLetterIndex;
    public int mySayIndex;//每句話的代號
    public BulletHome myBH;
    public void ShootLogic() {

        //myShootingState 發射的狀態切換
        switch (myShootingState)
        {
            case "ing":
                
                if (myHeart.myHeartState == "light_SlowMotion")
                {
                    myShootTimer = Mathf.Clamp(myShootTimer + Time.deltaTime*0.1f, 0, myShootTime);
                }
                else {
                    myShootTimer = Mathf.Clamp(myShootTimer + Time.deltaTime, 0, myShootTime);


                    //myShootTimer += Time.deltaTime;
                    //if (myShootTimer >= myShootTime)
                    //{
                    //    myShootTimer = 0;
                    //}

                }


                if (myShootTimer == myShootTime) {
                    myShootTimer = 0;

                    // 發射子彈 參考 https://youtu.be/LNLVOjbrQj4?t=894
                    // 影片24我把他拆開來寫，因為我的做法是在UI裡面 物件需要掛到Canvas 底下
                    // 所以是先 Instantiate(myBullet, myBulletHome); 之後再設定 座標 跟 旋轉
                    GameObject b = Instantiate(myBullet, myBulletHome);
                    b.transform.position = firePoint.position;
                    b.transform.rotation = firePoint.rotation;
                    Rigidbody2D rb = b.GetComponent<Rigidbody2D>();



                    if (myHeart.myHeartState == "light_SlowMotion") {
                        rb.AddForce(firePoint.up * bulletForce*0.1f, ForceMode2D.Impulse);
                    }
                    else { 
                        rb.AddForce(firePoint.up * bulletForce, ForceMode2D.Impulse); 
                    }                    

                    //更換子彈身上的字 Substring 可以研究一下
                    b.GetComponent<Text>().text = mySayString[mySayIndex].Substring(myLetterIndex, 1);//今天天氣好熱
                    
                    //子彈數量+1顆
                    myBH.BulletAmount++;

                    //發射到第幾個字的計數器 +1
                    myLetterIndex++;
                }

                //發射到第幾個字的計數器 跟 句話有幾個字 相等的時候表示 話說完了(子彈打完了)
                //切換狀態 idle 其他東西歸零
                if (myLetterIndex == myBulletAmount) {
                    myLetterIndex = 0;
                    myShootingState = "idle";

                    //切換句子
                    mySayIndex++;
                    //如果到底了 那就從頭開始
                    //阿如果想要隨機句子可以這樣寫
                    //mySayIndex = Random.Range(0, mySayString.Length);
                    //這樣 mySayIndex++; 還有下面的if (mySayIndex == mySayString.Length) 
                    //就都可以拿掉了
                    if (mySayIndex == mySayString.Length) {
                        mySayIndex = 0;
                    }
                }
                break;
            default:
                break;
        }
    }
   
    private void FixedUpdate()
    {
        //計算旋轉角度  參考 https://youtu.be/LNLVOjbrQj4?t=575
        //因為不用移動 所以 影片26行那個 rb.Move...就不用了
        if (myShootingState == "ready")
        {
            Vector2 lookDir = mousePos - rb.position;
            float angle = Mathf.Atan2(lookDir.y, lookDir.x) * Mathf.Rad2Deg - 90f;
            rb.rotation = angle;
        }
        //Vector2 lookDir = mousePos - rb.position;
        //float angle = Mathf.Atan2(lookDir.y, lookDir.x) * Mathf.Rad2Deg - 90f;
        //rb.rotation = angle;
    }
}
