using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Heart : MonoBehaviour
{
    public int mySayBoxAmount;
    public string myHeartState;
    // Start is called before the first frame update
    void Start()
    {
        myHeartState = "dark";
    }

    // Update is called once per frame
    void Update()
    {
        HeartLogic();
    }
    public BulletHome myBH;
    public void HeartLogic()
    {
        switch (myHeartState)
        {
            case "dark":
                if (mySayBoxAmount <= 0)
                {
                    GameObject[] myB = GameObject.FindGameObjectsWithTag("Bullet");
                    for (int a = 0; a < myB.Length; a++)
                    {
                        Destroy(myB[a].gameObject);
                    }
                    myBH.BulletAmount = 0;
                    myHeartState = "light";
                    ShowHeart();
                }
                break;
            case "light":
                if (isBulletInTrigger)
                {
                    GameObject[] myB = GameObject.FindGameObjectsWithTag("Bullet");
                    for (int a = 0; a < myB.Length; a++)
                    {
                        myB[a].GetComponent<Bullet>().VDown();
                    }
                    myHeartState = "light_SlowMotion";
                    HeartBroken();
                }
                break;
            case "light_SlowMotion":
                myShootTimer += Time.deltaTime;
                if (myShootTimer >= myShootTime)
                {
                    myShootTimer = 0;
                    // ���� scene
                    UnityEngine.SceneManagement.SceneManager.LoadScene(LoadingSceneName);
                }
                break;
            case "broken": break;
            default:
                break;
        }
    }
    public float myShootTimer;
    public float myShootTime;
    public string LoadingSceneName;

    public Color myC_Light;
    public Image myI_Heart;
    public Sprite mySP_HeartBreaken;
    public bool isBulletInTrigger;
    public void ShowHeart()
    {
        myI_Heart.color = myC_Light;
    }
    public void HeartBroken()
    {
        myI_Heart.sprite = mySP_HeartBreaken;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Bullet" && myHeartState == "light")
        {
            isBulletInTrigger = true;
        }
    }
}
