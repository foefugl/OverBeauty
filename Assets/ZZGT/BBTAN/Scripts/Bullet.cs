using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    Rigidbody2D myRD;
    private void Start()
    {
        myRD = GetComponent<Rigidbody2D>();
    }
    private void Update()
    {
        
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "SayBox") {
            collision.gameObject.GetComponent<Animator>().Play("SayBoxGetHurt");
            collision.gameObject.GetComponent<SayBox>().HP--;
        }
    }
    public void VDown() {
        myRD.velocity = myRD.velocity * 0.1f;
    }
}
