using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SayBox : MonoBehaviour
{
    public int HP;
    public Heart myH;
    // Start is called before the first frame update
    void Start()
    {
        myH = GameObject.Find("Heart").GetComponent<Heart>();
    }

    // Update is called once per frame
    void Update()
    {
        if (HP <= 0) {
            myH.mySayBoxAmount--;
            Destroy(gameObject); 
        }
    }
}
