using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletHome : MonoBehaviour
{
    public int BulletAmount;
    public FirePoint myFP;
    private void Update()
    {
        if (myFP.myShootingState == "idle") {
            if (BulletAmount == 0) {
                myFP.myShootingState = "ready";
            }
        }
    }
    public void ClearAllBullet() {
        for (int a = 0; a < transform.childCount; a++) {
            Destroy(transform.GetChild(a));
        }
    }
}
