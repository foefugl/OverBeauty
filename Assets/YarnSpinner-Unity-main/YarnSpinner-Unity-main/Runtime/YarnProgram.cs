﻿using UnityEngine.Networking;
using Yarn;
using UnityEngine;
using System.Collections;
using UnityEditor;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using Yarn.Unity;

/// Stores compiled Yarn programs in a form that Unity can serialise.
public class YarnProgram : ScriptableObject
{
    [SerializeField]
    [HideInInspector]
    public byte[] compiledProgram;

    [SerializeField]
    public TextAsset baseLocalisationStringTable;

    [SerializeField]
    public string baseLocalizationId;

    /// <summary>
    /// Available localizations of this yarn program
    /// </summary>
    [SerializeField][HideInInspector]
    public YarnTranslation[] localizations = new YarnTranslation[0];

    // Deserializes a compiled Yarn program from the stored bytes in this
    // object.
    public Program GetProgram() {
        return Program.Parser.ParseFrom(compiledProgram);                
    }

    public IEnumerator DownloadContent()
    {
        for (int i = 0; i < localizations.Length; i++)
        {
            if (localizations[i].URL == "")
            {
                continue;
            }
            WWWForm form = new WWWForm();
            form.AddField("method", "read");

            // Sending the request to API url with form object.
            using (UnityWebRequest www = UnityWebRequest.Post(localizations[i].URL, form))
            {
                yield return www.SendWebRequest();

                if (www.isNetworkError || www.isHttpError)
                {
                    Debug.Log(www.error);
                }
                else
                {
                    Debug.Log(www.downloadHandler.text);
                    AnalyzeData(www.downloadHandler.text, localizations[i]);
                }
            }
        }
    }
    private void AnalyzeData(string _text, YarnTranslation _translation)
    {
        _text = _text.Replace('@', '\n');
        _text = _text.Replace('^', ',');

        string fileName = _translation.text.name;
        string[] guids = AssetDatabase.FindAssets(fileName);
        string path = AssetDatabase.GUIDToAssetPath(guids[0]);

        File.WriteAllText(path, _text);
        //StreamWriter writer = new StreamWriter(path, true);
        //writer.WriteAllText();
        //writer.(_text);
        //writer.Close();
        //AssetDatabase.ImportAsset(path);

        AssetDatabase.Refresh();
    }
}
