using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MesSCOCleaner : MonoBehaviour
{
    public bool Clear;
    public SCO_WinkMessage[] sCO_WinkMessages;

    private void Start()
    {
        ResetMesContent();
    }
    private void OnValidate()
    {
        if (Clear)
        {
            Clear = false;
            ResetMesContent();
        }
    }
    private void ResetMesContent()
    {
        Debug.Log("<Reset MesContent>");
        for (int i = 0; i < sCO_WinkMessages.Length; i++)
        {
            sCO_WinkMessages[i].PostSetting.Content = new List<string>();
            sCO_WinkMessages[i].ContentIndex = 0;
        }
    }
}
