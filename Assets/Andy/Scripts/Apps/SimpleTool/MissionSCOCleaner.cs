using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissionSCOCleaner : MonoBehaviour
{
    public bool Clear;
    public SCO_MissionPost[] sCO_MissionPost;

    private void Start()
    {
        ResetMissionSCO();
    }
    private void OnValidate()
    {
        if (Clear)
        {
            Clear = false;
            ResetMissionSCO();
        }
    }
    private void ResetMissionSCO()
    {
        Debug.Log("<Reset MissionContent>");
        for (int i = 0; i < sCO_MissionPost.Length; i++)
        {
            sCO_MissionPost[i].CompletedCount = 0;
            for (int j = 0; j < sCO_MissionPost[i].PostSetting.Content.Count; j++)
            {
                if (sCO_MissionPost[i].PostSetting.Content[j].Contains("<s>"))
                {
                    sCO_MissionPost[i].PostSetting.Content[j] = sCO_MissionPost[i].PostSetting.Content[j].Replace("<s>", "");
                    sCO_MissionPost[i].PostSetting.Content[j] = sCO_MissionPost[i].PostSetting.Content[j].Replace("</s>", "");
                }
            }
        }
    }
}
