using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissionUIInstance : NotificaMessageUIInstance
{
    public override void InitialDatas(SCO_Post sCO_Post)
    {
        CurrentPostData = sCO_Post;
        PostUserAvatar.sprite = sCO_Post.PostSetting.Avatar;
        
        PostUserName.text = sCO_Post.PostSetting.UserName + GetMissionTaskCountInfo();
        OverlayContentToIndex(sCO_Post.PostSetting.Content.Count);
    }
    public string GetMissionTaskCountInfo()
    {
        SCO_MissionPost postData = (SCO_MissionPost)CurrentPostData;
        var info = string.Format("{0}/{1}",
            postData.CompletedCount,
            CurrentPostData.PostSetting.Content.Count.ToString());

        return info;
    }
}
