using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.EventSystems;
using Utility;

[System.Serializable]
public class MissionData
{
    public string ActiveKey;
    public int DutyIndex = 0;
    public string CueContent;
    public MissionData(string _key, int _index, string _content)
    {
        ActiveKey = _key;
        DutyIndex = _index;
        CueContent = _content;
    }
}
public class MissionChop : MonoBehaviour, IPointerClickHandler
{
    public MissionData data;
    public Action PointerClickAction;

    public void OnPointerClick(PointerEventData eventData)
    {
        Debug.Log("MissionData: " + data.ActiveKey + "/" + data.DutyIndex);
        MissionManager.Instance.MissionCompleted(data.ActiveKey, data.DutyIndex);
        PointerClickAction?.Invoke();
    }

}
