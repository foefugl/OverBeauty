using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Yarn.Unity;
using Utility;

public class MissionManager : CommunityPostManager
{
    private static MissionManager instance;
    public static MissionManager Instance { get { return instance; } }

    [Header("手動達成任務")]
    public bool completed;
    public string missionKey;
    public int taskIndex;
    private Dictionary<string, CommunityPostManager> runningCommunitys;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        runningCommunitys = new Dictionary<string, CommunityPostManager>();
    }
    public void RegistManager(string _name, CommunityPostManager _runner)
    {
        if(!runningCommunitys.ContainsKey(_name))
            runningCommunitys.Add(_name, _runner);
    }
    public void RemoveManager(string _name)
    {
        if (runningCommunitys.ContainsKey(_name))
            runningCommunitys.Remove(_name);
    }

    protected override void Update()
    {
        base.Update();
        if (completed)
        {
            completed = false;
            MissionCompleted(missionKey, taskIndex);
        }
    }
    [YarnCommand("YarnSendCompletedCode")]
    public void YarnSendCompletedCode(string _key, string _index)
    {
        Debug.Log("_key: " + _key + " / Index: " + _index);
        int achive_index = 0;
        int.TryParse(_index, out achive_index);
        MissionCompleted(_key, achive_index);
    }
    [YarnCommand("GetCurrentDialogueRunner")]
    public void GetCurrentDialogueRunner(string _keyContent)
    {
        Debug.Log("GetCurrentDialogueRunner");

        string[] codes = _keyContent.Split(',');
        runningCommunitys[codes[0]].CurrentMData.ActiveKey = codes[1];
        int index = 0;
        int.TryParse(codes[2], out index);
        runningCommunitys[codes[0]].CurrentMData.DutyIndex = index;
        runningCommunitys[codes[0]].CurrentMData.CueContent = codes[3];
    }
    
    public void MissionCompleted(string key, int index)
    {
        Debug.LogError("index: " + index);
        if (!postsDic.ContainsKey(key) )
        {
            Debug.LogError("Still not get missio: " + key);
            return;
        }
        if (postsDic[key].CurrentPostData.PostSetting.Content[index].Contains("<s>"))
        {
            Debug.LogError("Ready completed.");
            return;
        }

        var uiInstance = (MissionUIInstance)postsDic[key];
        SCO_MissionPost postData = (SCO_MissionPost)uiInstance.CurrentPostData;
        postData.CompletedCount++;
        postsDic[key].PostUserName.text = postsDic[key].CurrentPostData.PostSetting.UserName + uiInstance.GetMissionTaskCountInfo();

        var content = postsDic[key].CurrentPostData.PostSetting.Content[index];
        postsDic[key].CurrentPostData.PostSetting.Content[index] = "<s>" + content + "</s>";
        postsDic[key].OverlayContentToIndex(postsDic[key].CurrentPostData.PostSetting.Content.Count);

        Post post = new Post();
        post.UserName = "完成 [" + postsDic[key].CurrentPostData.PostSetting.UserName + "] 第" + (index+1).ToString() + "項";
        post.Content = new List<string>() { content };

        SendNotifyPost(post);
    }
    //private void SendNotifyPost(Post _targetPost)
    //{
    //    SCO_NotifyPost sCO_NotifyPost = (SCO_NotifyPost)ScriptableObject.CreateInstance("SCO_NotifyPost");
    //    sCO_NotifyPost.SetData(SoftwareDescrs);
    //    sCO_NotifyPost.AppType = SoftwareDescrs.AppType;
    //    sCO_NotifyPost.PostSetting.UserName = _targetPost.UserName;

    //    if (_targetPost.Content != null)
    //    {
    //        if (_targetPost.Content.Count != 0)
    //        {
    //            sCO_NotifyPost.PostSetting.Content = new List<string>() { _targetPost.Content[0] };
    //        }
    //    }

    //    sCO_NotifyPost.PostSetting.Avatar = SoftwareDescrs.PostSetting.Avatar;
    //    NotifyPost notify = new NotifyPost(this, sCO_NotifyPost);
    //    if (NotifyManager.Instance != this)
    //    {
    //        NotifyManager.Instance.AddNotification(notify);
    //    }
    //}
}
