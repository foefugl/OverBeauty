using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CueChopsObserver : MonoBehaviour
{
    private static CueChopsObserver instance;
    public static CueChopsObserver Instance 
    { 
        get 
        {
            if (instance == null)
            {
                instance = FindObjectOfType<CueChopsObserver>();
                if (instance == null)
                    instance = new GameObject("CueChopsObserver").AddComponent<CueChopsObserver>();
                
                instance.Initial();
            }
            return instance; 
        }
    }
    private Dictionary<string, CueChopDetector> cueChopDic;
    private void Start()
    {
        Initial();
    }
    public void Initial()
    {
        cueChopDic = new Dictionary<string, CueChopDetector>();
        if (instance == null)
        {
            instance = this;
        }
        DontDestroyOnLoad(instance.gameObject);
    }

    public void RegistDetector(string _key, CueChopDetector _cueChopDetector)
    {
        Debug.Log("RegistDetector: " + _key);
        cueChopDic.Add(_key, _cueChopDetector);
    }
    public void RegistCueClickEvent(string _key, Action _action)
    {
        Debug.Log("_key: " + _key);
        cueChopDic[_key].RegistEventInCue(_action);
    }
}
