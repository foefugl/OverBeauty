using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CueChopDetector : MonoBehaviour
{
    public MissionData data;
    public TextMeshProUGUI text;
    private MissionChop dutyChop;

    private void Start()
    {
        text = gameObject.GetComponentInChildren<TextMeshProUGUI>();
        StartCoroutine(AdjustPosAfterFrame());
    }
    IEnumerator AdjustPosAfterFrame()
    {
        yield return null;
        ScanTxt();
        AdjustPos();
    }
    public void RegistEventInCue(Action _action)
    {
        dutyChop.PointerClickAction = _action;
    }
    public void ScanTxt()
    {
        text.ForceMeshUpdate();
        if (text.text.Contains(data.CueContent))
        {
            TextMeshProUGUI cueTex = Instantiate(text, text.transform);
            foreach (var comp in cueTex.GetComponents<Component>())
            {
                if (comp is RectTransform || comp is TextMeshProUGUI || comp is CanvasRenderer)
                {

                }
                else
                {
                    Destroy(comp);
                }
            }

            int index = text.text.IndexOf(data.CueContent);
            Vector3 pos = text.textInfo.characterInfo[index].bottomLeft;
            cueTex.text = data.CueContent;

            var rectTrans = cueTex.GetComponent<RectTransform>();
            var referTrans = text.GetComponent<RectTransform>();
            rectTrans.sizeDelta= new Vector2(cueTex.preferredWidth+1, cueTex.preferredHeight);
            rectTrans.anchorMin = referTrans.anchorMin;
            rectTrans.anchorMax = referTrans.anchorMax;
            rectTrans.pivot = Vector2.zero;
            cueTex.transform.localPosition = pos + Vector3.down * 10 + Vector3.left * 10;

            cueTex.color = new Color(cueTex.color.r, cueTex.color.g, cueTex.color.b, 0);
            dutyChop = cueTex.gameObject.AddComponent<MissionChop>();
            dutyChop.data = new MissionData(data.ActiveKey, data.DutyIndex, data.CueContent);

            cueTex.ForceMeshUpdate();
            cueTex.rectTransform.sizeDelta = new Vector2(cueTex.GetRenderedValues().x, cueTex.preferredHeight);

            cueTex.enabled = true;
        }
    }
    private void AdjustPos() 
    {
        var cueTex = dutyChop.GetComponent<TextMeshProUGUI>();

        int index = text.text.IndexOf(data.CueContent);
        Vector3 pos = text.textInfo.characterInfo[index].bottomLeft;
        cueTex.text = data.CueContent;

        var rectTrans = cueTex.GetComponent<RectTransform>();
        var referTrans = text.GetComponent<RectTransform>();

        rectTrans.sizeDelta = new Vector2(cueTex.preferredWidth + 1, cueTex.preferredHeight);
        rectTrans.anchorMin = referTrans.anchorMin;
        rectTrans.anchorMax = referTrans.anchorMax;
        rectTrans.pivot = Vector2.zero;
        cueTex.transform.localPosition = pos + Vector3.down * 10 + Vector3.left * 10;

        cueTex.rectTransform.sizeDelta = new Vector2(cueTex.GetRenderedValues().x, cueTex.preferredHeight);
    }
}
