using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

[System.Serializable]
public class App
{
    public string Name;
    public bool installed;
    public AppPanelName AppType;
    public SCO_NotifyPost SoftwareDescrs;
    public GameObject AppIconInstance;
}
public class AppInstaller : MonoBehaviour
{
    private static AppInstaller instance;
    public static AppInstaller Instance
    {
        get
        {
            return instance;
        }
    }
    public App[] Apps;
    public bool testInstall;
    public AppPanelName TargetAppType;

    private Queue instanllQueue;
    private bool installing;
    private Dictionary<AppPanelName, App> appsDic;
    private Queue<Action> installActions;


    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        appsDic = new Dictionary<AppPanelName, App>();
        installActions = new Queue<Action>();
        instanllQueue = new Queue();
        for (int i = 0; i < Apps.Length; i++)
        {
            appsDic.Add(Apps[i].AppType, Apps[i]);
        }

        StartCoroutine(CheckInstallNotify());
    }

    // Update is called once per frame
    void Update()
    {
        if (testInstall)
        {
            testInstall = false;
            instanllQueue.Enqueue(TargetAppType);
        }
    }
    public void InstallApp(AppPanelName _appType, Action _action)
    {
        installActions.Enqueue(_action);
        instanllQueue.Enqueue(_appType);
    }
    private IEnumerator CheckInstallNotify()
    {
        while (true)
        {
            if (instanllQueue.Count != 0)
            {
                installing = true;
                AppPanelName type = (AppPanelName)instanllQueue.Dequeue();
                Action finishedAction = (Action)installActions.Dequeue();
                AppInstallingMotion(appsDic[type], finishedAction);

                yield return new WaitUntil(() => !installing);
            }
            yield return null;
        }
    }
    private void AppInstallingMotion(App _target, Action _action = null)
    {
        _target.AppIconInstance.transform.parent.gameObject.SetActive(true);
        var iconImg = _target.AppIconInstance.transform.GetComponent<Image>();
        float process = 0;

        NotifyManager.Instance.appInstallNotify.transform.parent.gameObject.SetActive(true);
        NotifyManager.Instance.appInstallNotify.transform.localScale = Vector3.zero;
        NotifyManager.Instance.appInstallNotify.transform.DOScale(Vector3.one, 0.25f)
            .SetEase(Ease.OutBack);

        NotifyManager.Instance.appInstallNotify.PostUserAvatar.sprite = _target.SoftwareDescrs.PostSetting.Avatar;
        NotifyManager.Instance.appInstallNotify.SoftwareName.text = _target.SoftwareDescrs.SoftwareDesc.Split(',')[0];
        NotifyManager.Instance.appInstallNotify.PostUserName.text = "安裝中...";
        NotifyManager.Instance.appInstallNotify.JumpBtn.transform.GetChild(0).gameObject.SetActive(true);
        NotifyManager.Instance.appInstallNotify.JumpBtn.transform.GetChild(1).gameObject.SetActive(false);

        DOTween.To(() => process, x => process = x, 1, 10)
            .OnUpdate(() => 
            {
                iconImg.fillAmount = process;
                NotifyManager.Instance.appInstallNotify.processingBar.fillAmount = process;
            })
            .OnComplete(() => 
            {
                var btn = _target.AppIconInstance.transform.GetComponent<Button>();
                btn.interactable = true;
                _target.installed = true;

                NotifyManager.Instance.appInstallNotify.PostUserName.text = "已安裝應用程式";
                NotifyManager.Instance.appInstallNotify.JumpBtn.transform.GetChild(0).gameObject.SetActive(false);
                NotifyManager.Instance.appInstallNotify.JumpBtn.transform.GetChild(1).gameObject.SetActive(true);

                NotifyManager.Instance.appInstallNotify.transform.DOScale(Vector3.zero, 0.25f)
                .SetDelay(1.25f)
                .OnComplete(() => 
                {
                    NotifyManager.Instance.appInstallNotify.transform.parent.gameObject.SetActive(false);
                    installing = false;
                    _action?.Invoke();
                }); 
                
            });
    }
}
