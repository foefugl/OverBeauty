using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class AISearchResultUI : MonoBehaviour
{
    public Image photo;
    public TextMeshProUGUI Title;
    public TextMeshProUGUI Comment;
    public TextMeshProUGUI Address;
    public KeywordData currentData;

    private PostPointerClickEvent postPointerClick;

    public void SetData(KeywordData _targetData)
    {
        currentData  = _targetData;
        photo.sprite = _targetData.photo;
        Title.text   = _targetData.KeywordTxt;
        Comment.text = _targetData.Comment;
        Address.text = _targetData.Address;

        if (_targetData.KeywordClickEvent != null)
        {
            if (postPointerClick == null)
            {
                postPointerClick = Address.GetComponent<PostPointerClickEvent>();
            }
            postPointerClick.ClickEvent += () => _targetData.KeywordClickEvent?.Invoke();
        }
    }
    public void RegistEvent(Action _action)
    {
        if (postPointerClick == null)
        {
            postPointerClick = Address.GetComponent<PostPointerClickEvent>();
        }
        postPointerClick.ClickEvent += () => _action?.Invoke();
    }
    public void ClearEvent()
    {
        if (postPointerClick == null)
        {
            postPointerClick = Address.GetComponent<PostPointerClickEvent>();
        }
        postPointerClick.ClickEvent = null;
    }
}
