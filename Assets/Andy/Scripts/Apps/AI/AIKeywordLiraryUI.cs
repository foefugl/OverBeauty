using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class AIKeywordLiraryUI : MonoBehaviour
{
    public AICueButton buttonPrefab;
    public Transform GridLayout;
    private Dictionary<string, KeywordData> btnsDic;
    private void Awake()
    {
        btnsDic = new Dictionary<string, KeywordData>();

        for (int i = 0; i < GridLayout.childCount; i++)
        {
            Destroy(GridLayout.GetChild(i).gameObject);
        }
    }

    private void OnEnable()
    {
        KeywordBtnInitial();
    }

    private void KeywordBtnInitial()
    {
        var keywordDic = KeywordManager.Instance.KeywordsDic;

        for (int i = 0; i < keywordDic.Count; i++)
        {
            if (!btnsDic.ContainsKey(keywordDic.ElementAtOrDefault(i).Key))
            {
                var keyBtn = Instantiate(buttonPrefab, GridLayout);
                keyBtn.CurrentData = keywordDic.ElementAtOrDefault(i).Value;
                btnsDic.Add(keyBtn.CurrentData.ActiveKey, keyBtn.CurrentData);
            }
        }
    }
}
