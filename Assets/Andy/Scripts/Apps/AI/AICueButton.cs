using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using TMPro;

public class AICueButton : MonoBehaviour
{
    public TextMeshProUGUI text;
    public KeywordData CurrentData
    {
        get
        {
            return currentData;
        }
        set
        {
            currentData = value;
            text.text = value.KeywordTxt;
        }
    }
    private KeywordData currentData;



    public void CueClickFunc()
    {
        AIManager.Instance.PlayerReply(currentData);
    }
}
