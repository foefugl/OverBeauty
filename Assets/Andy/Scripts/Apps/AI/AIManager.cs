using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class AIManager : MonoBehaviour
{
    private static AIManager instance;
    public static AIManager Instance 
    {
        get
        {
            return instance;
        }
    }

    public bool Launch;
    public Transform ScrollTrans;
    public ScrollRect ScrollRect;
    public GameObject OptionPanel;
    public GameObject AiNormalDialogue, AiNoKeyword, AiWannaDoDialogue, AiSearchDialogue, AiResultDialogue, AiPlayerReply;
    public VerticalScrollSetPos verticalScrollSet;
    public AISearchResultUI resultUI;
    public UNI_AI uNI_AI;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }
    public void Update()
    {
        if (Launch)
        {
            Launch = false;
            Initial();
        }
    }

    public void Initial()
    {
        ScrollRect.normalizedPosition = new Vector2(0, 1);
        for (int i = 0; i < ScrollTrans.childCount; i++)
        {
            if (ScrollTrans.GetChild(i).name.Contains("Clone"))
            {
                Destroy(ScrollTrans.GetChild(i).gameObject);
            }
        }

        OptionPanel.SetActive(false);
        AiNormalDialogue.SetActive(false);
        AiNoKeyword.SetActive(false);
        AiWannaDoDialogue.SetActive(false);
        AiSearchDialogue.SetActive(false);
        AiResultDialogue.SetActive(false);
        AiPlayerReply.SetActive(false);

        resultUI.ClearEvent();
        resultUI.RegistEvent(() => uNI_AI.close_ai());

        StartCoroutine(StartDialogue());
    }

    public void PlayerReply(KeywordData _data)
    {
        if (AiPlayerReply.activeSelf)
        {
            GameObject reply = Instantiate(AiPlayerReply, ScrollTrans);
            reply.GetComponentInChildren<TextMeshProUGUI>().text = _data.KeywordTxt;
        }
        else
        {
            AiPlayerReply.SetActive(true);
            AiPlayerReply.GetComponentInChildren<TextMeshProUGUI>().text = _data.KeywordTxt;
        }
        verticalScrollSet.StartScrollToBottom();
        StartCoroutine(FeedbackResult(_data));
    }
    private IEnumerator FeedbackResult(KeywordData _data)
    {
        yield return new WaitForSeconds(1);
        if (AiResultDialogue.activeSelf)
        {
            GameObject result = Instantiate(AiResultDialogue, ScrollTrans);
            result.GetComponent<AISearchResultUI>().SetData(_data);
            result.SetActive(true);
        }
        else
        {
            AiResultDialogue.GetComponent<AISearchResultUI>().SetData(_data);
            AiResultDialogue.SetActive(true);
        }

        yield return new WaitForSeconds(2.5f);
        verticalScrollSet.StartScrollToBottom();
        StartCoroutine(SecondStartDialogue());
    }
    private IEnumerator SecondStartDialogue()
    {
        GameObject wannaDo = Instantiate(AiWannaDoDialogue, ScrollTrans);
        wannaDo.SetActive(true);
        yield return new WaitForSeconds(1);
        OptionPanel.SetActive(true);
        verticalScrollSet.StartScrollToBottom();
    }

    private IEnumerator StartDialogue()
    {
        yield return new WaitForSeconds(0.5f);
        AiNormalDialogue.SetActive(true);
        OptionPanel.SetActive(true);
    }
    public void WannaSearchClickFunc()
    {
        if (KeywordManager.Instance.KeywordsDic.Count == 0)
        {
            AiNoKeyword.SetActive(true);
            OptionPanel.SetActive(false);
            return;
        }
        if (AiSearchDialogue.activeSelf)
        {
            GameObject search = Instantiate(AiSearchDialogue, ScrollTrans);
        }
        else
        {
            AiSearchDialogue.SetActive(true);
        }

        verticalScrollSet.StartScrollToBottom();
        OptionPanel.SetActive(false);
    }
}
