using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class FansAmountRevealTab : MonoBehaviour
{
    public TextMeshProUGUI valueTxt;
    public Image PercentageImage;
    public SCO_WinkClubRoom clubData;

    // Update is called once per frame
    void Update()
    {
        float percentage = clubData.FansPercentage();
        PercentageImage.fillAmount = clubData.FansPercentageInOne();
        valueTxt.text = Mathf.RoundToInt(percentage).ToString();
    }
}
