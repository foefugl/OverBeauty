using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class NotifyUnityEvent : UnityEvent<NotifyPost>{ }

public class NotifyPost
{
    public SCO_NotifyPost post;
    public NotifyPost(SCO_NotifyPost _post)
    {
        post    = _post;
    }
}
public struct PopOutContentStruct
{
    public SCO_NotifyPost postSetting;
    public string name;
    public string content;
}

public class NotifyManager : CommunityPostManager
{
    private static NotifyManager instance;
    public static NotifyManager Instance 
    {
        get
        {
            return instance;
        }
    }

    public PopOutNotify popout;
    public AppInstallNotificationUIInstance appInstallNotify;

    private Queue pop_notifyQueue;
    private bool popOutFinish, startCollectPop;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    protected override void Initial()
    {
        base.Initial();
        pop_notifyQueue = new Queue();
    }
    protected override void Update()
    {
        base.Update();

        if (popout == null)
            return;

        if (pop_notifyQueue.Count > 0 && !popOutFinish)
        {
            StartCoroutine(PopOutPipe());
        }
    }
    public void EnterAppClearNotify(AppPanelName _currentApp)
    {
        for (int i = 0; i < postsDic.Count; i++)
        {
            SCO_NotifyPost sCOPostData = (SCO_NotifyPost)postsDic.ElementAt(i).Value.CurrentPostData;
            if (sCOPostData.AppType == _currentApp)
            {
                var tempDestroy = postsDic.ElementAt(i);
                postsDic.Remove(tempDestroy.Key);
                Destroy(tempDestroy.Value.gameObject);
            }
        }
    }

    public void AddNotification(NotifyPost _post)
    {
        if (_post.post != null && (BaseUnderToolBar.Instance.CurrentApp != _post.post.AppType))
        {
            GameObject postObj = AddPost(_post.post);
            postObj.transform.SetAsFirstSibling();
            Button btn = postObj.GetComponent<Button>();
            btn.onClick.RemoveAllListeners();
            btn.onClick.AddListener(() => JumpToApp(_post.post.AppType));

            if (startCollectPop == true)
            {
                PopOutContentStruct contentPackage;
                contentPackage.postSetting = _post.post;
                contentPackage.name = _post.post.PostSetting.UserName;
                contentPackage.content = _post.post.PostSetting.Content[0];
                pop_notifyQueue.Enqueue(contentPackage);
            }
        }
        
    }
    private void JumpToApp(AppPanelName appType)
    {
        Debug.Log(appType);
        BaseUnderToolBar.Instance.OpenApp(appType);
    }
    public void DelayStartPopOut()
    {
        StartCoroutine(DelayStartPopOut_Coroutine());
    }
    IEnumerator DelayStartPopOut_Coroutine()
    {
        yield return new WaitForSeconds(5);
        startCollectPop = true;
    }
    IEnumerator PopOutPipe()
    {
        PopOutContentStruct popData = (PopOutContentStruct)pop_notifyQueue.Dequeue();
        //popOutFinish = true;
        //popout.PopOut(popData, () => popOutFinish = false);
        if (BaseUnderToolBar.Instance.CurrentApp != popData.postSetting.AppType)
        {
            popOutFinish = true;
            popout.PopOut(popData, () => popOutFinish = false);
        }
        else
        {
            popOutFinish = false;
        }

        yield return new WaitUntil(() => popOutFinish == false);
    }
}
