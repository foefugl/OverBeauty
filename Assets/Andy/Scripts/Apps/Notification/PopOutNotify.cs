using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class PopOutNotify : MonoBehaviour
{
    public bool Pop;
    public Transform ShowPos, HidePos;
    public Action FinishAction;

    private Button btn;
    private NotificaMessageUIInstance UiInstance;
    private Tween moveTwn;

    private void Start()
    {
        btn = GetComponent<Button>();
        UiInstance = GetComponent<NotificaMessageUIInstance>();
    }

    public void PopOut(PopOutContentStruct _popOutData, Action _action = null)
    {
        UiInstance.InitialDatas(_popOutData.postSetting); 
        UiInstance.PostContent.text = _popOutData.content;
        UiInstance.PostUserName.text = _popOutData.name;

        if (moveTwn != null)
        {
            if (moveTwn.IsPlaying())
            {
                moveTwn.Kill();
            }
        }

        moveTwn = transform.DOMove(ShowPos.position, 0.45f)
                .SetEase(Ease.OutBack)
                .OnComplete(() =>
                {
                    moveTwn = transform.DOMove(HidePos.position, 0.45f)
                        .SetEase(Ease.InBack)
                        .SetDelay(2f)
                        .OnComplete(() => _action?.Invoke());
                });

        btn.onClick.RemoveAllListeners();
        btn.onClick.AddListener(() =>
        {
            BaseUnderToolBar.Instance.OpenApp(_popOutData.postSetting.AppType);
        });
    }
}
