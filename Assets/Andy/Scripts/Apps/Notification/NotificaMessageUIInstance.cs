using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NotificaMessageUIInstance : PostUIInstance
{
    public Text SoftwareName;
    public Text SoftwareDescrs;
    public Button JumpBtn;

    public override void InitialDatas(SCO_Post sCO_Post)
    {
        base.InitialDatas(sCO_Post);
        SCO_NotifyPost notifyPost = (SCO_NotifyPost)sCO_Post;
        string[] nameDescrs = notifyPost.SoftwareDesc.Split(',');
        SoftwareName.text = nameDescrs[0];
        SoftwareDescrs.text = nameDescrs[1];
    }
}
