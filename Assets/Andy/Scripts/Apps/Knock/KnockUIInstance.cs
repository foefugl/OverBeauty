using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KnockUIInstance : MonoBehaviour
{
    public PostPointerClickEvent CueClickEvent;

    public void RegistCueEvent(Action _action)
    {
        CueClickEvent.ClickEvent += () => _action.Invoke();
    }
}
