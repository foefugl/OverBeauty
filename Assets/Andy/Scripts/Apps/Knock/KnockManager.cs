using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KnockManager : MonoBehaviour
{
    private static KnockManager instance;
    public static KnockManager Instance
    {
        get
        {
            return instance;
        }
    }
    public KnockUIInstance KnockUI;
    public MissionData currentMissionData;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        KnockUI.RegistCueEvent(() => MissionManager.Instance.MissionCompleted(currentMissionData.ActiveKey, currentMissionData.DutyIndex));
    }
    public void OpenCanvas()
    {
        Debug.LogError("OpenKnockPage");
        StartCoroutine(OpenKnockCoroutine());
    }
    IEnumerator OpenKnockCoroutine()
    {
        FadeManager.Instance.Run();
        yield return new WaitUntil(() => FadeManager.Instance.Visible);
        transform.GetChild(0).gameObject.SetActive(true);
        FadeManager.Instance.Run();
        BaseUnderToolBar.Instance.HomeBtnRegistEvent(CloseKnock);
        BaseUnderToolBar.Instance.PopOutShow();
    }
    private void CloseKnock()
    {
        FadeManager.Instance.FadeOutevent.AddListener(() => transform.GetChild(0).gameObject.SetActive(false));
        BaseUnderToolBar.Instance.HomeBtnRemoveEvent(CloseKnock);
    }
}
