using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Utility;

[CreateAssetMenu(fileName = "Post", menuName = "SCO/NotifyPost", order = 2)]
public class SCO_NotifyPost : SCO_Post
{
    public AppPanelName AppType;
    public string SoftwareDesc;
    public virtual void SetData(SCO_NotifyPost _sCO_Notify)
    {
        Key = _sCO_Notify.Key;
        PostSetting = _sCO_Notify.PostSetting;
        ContentIndex = _sCO_Notify.ContentIndex;
        SoftwareDesc = _sCO_Notify.SoftwareDesc;
    }
}
