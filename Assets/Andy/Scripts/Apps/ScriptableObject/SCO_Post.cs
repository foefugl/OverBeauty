using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Utility;
using Yarn.Unity;
using UnityEngine.Networking;



public class SCO_Post : ScriptableObject
{
    [Header("[ GAS DATA ]")]
    public string ChineseURL;
    public string EnglishURL;
    [Space(10)]

    public string Key;
    public Post PostSetting;
    public MissionData PhotoMissionData;
    public int ContentIndex;

    public virtual IEnumerator DownloadContent(LanguageType _type)
    {
        string TargetURL = (_type == LanguageType.Chinese) ? ChineseURL : EnglishURL;
        if (TargetURL == "")
        {
            yield return null;
        }
        else
        {
            WWWForm form = new WWWForm();
            form.AddField("method", "read");

            // Sending the request to API url with form object.
            using (UnityWebRequest www = UnityWebRequest.Post(TargetURL, form))
            {
                yield return www.SendWebRequest();

                if (www.isNetworkError || www.isHttpError)
                {
                    Debug.Log(www.error);
                }
                else
                {
                    Debug.Log(www.downloadHandler.text);
                    // Done and get the response text.
                    Debug.Log("Form upload complete!");
                }
            }
        }
    }
}
