using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SCO_MessageRoom : ScriptableObject
{
    public string Key;
    public string RoomName;
    public YarnProgram[] bluePrints;
    public List<SCO_Post> members;
}
