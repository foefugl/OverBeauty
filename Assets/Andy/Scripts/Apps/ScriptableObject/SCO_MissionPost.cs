using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(fileName = "Post", menuName = "SCO/MissionPost", order = 4)]
public class SCO_MissionPost : SCO_NotifyPost
{
    private int completedCount;
    public int CompletedCount
    {
        get
        {
            return completedCount;
        }
        set
        {
            completedCount = value;
            if (completedCount == PostSetting.Content.Count)
            {
                MissionCompleteEvents?.Invoke();
            }
        }
    }
    public UnityEvent MissionCompleteEvents;
}
