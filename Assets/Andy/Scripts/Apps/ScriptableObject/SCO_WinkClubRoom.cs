using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public enum ClubType
{
    Private,
    Public
}
[CreateAssetMenu(fileName = "WinkClubRoom", menuName = "SCO/WinkClueRoom", order = 7)]
public class SCO_WinkClubRoom : SCO_Post
{
    [Space(10)]
    [Header("[ SCO_WinkClubRoom ]")]
    public ClubType ClubType;
    public bool GotPass;
    public string PassWord;
    public float FansAmount;
    public float FansMax;
    public Sprite Thumbnail;
    public UnityEvent EnterEvent;
    public GameObject[] AttachPrefabs;
    public SCO_Post[] DefaultPosts;

    public float FansPercentage()
    {
        return (FansAmount / FansMax) * 100;
    }
    public float FansPercentageInOne()
    {
        return (FansAmount / FansMax);
    }
}
