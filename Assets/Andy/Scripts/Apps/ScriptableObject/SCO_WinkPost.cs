using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Utility;
using Yarn.Unity;
using UnityEngine.Networking;

[CreateAssetMenu(fileName = "Post", menuName = "SCO/WinkPost", order = 1)]
public class SCO_WinkPost : SCO_Post
{
    public SCO_WinkPost SharePost;
    public YarnProgram[] BluePrints;
    public Post[] Comments;

    public override IEnumerator DownloadContent(LanguageType _type)
    {
        string TargetURL = (_type == LanguageType.Chinese) ? ChineseURL : EnglishURL;
        if (TargetURL == "")
        {
            yield return null;
        }
        else
        {
            WWWForm form = new WWWForm();
            form.AddField("method", "read");

            // Sending the request to API url with form object.
            using (UnityWebRequest www = UnityWebRequest.Post(TargetURL, form))
            {
                yield return www.SendWebRequest();

                if (www.isNetworkError || www.isHttpError)
                {
                    Debug.Log(www.error);
                }
                else
                {
                    //Debug.Log(www.downloadHandler.text);
                    AnalyzeData(www.downloadHandler.text);
                    // Done and get the response text.
                    Debug.Log("Form upload complete!");
                }
            }
        }
    }
    private void AnalyzeData(string _text)
    {
        string[] rows = _text.Split('@');

        bool startComments = false;
        int currentIndex = 0;

        for (int i = 0; i < rows.Length; i++)
        {
            string[] grids = rows[i].Split('^');
            if (grids[0] == "Comments")
            {
                startComments = true;
            }

            if (!startComments)
            {
                //var postList = dictionary["Post"];
                if (grids[0] == "UserName")
                {
                    PostSetting.UserName = grids[1];
                }
                else if (grids[0] == "Date")
                {
                    PostSetting.Date = grids[1];
                }
                else if (grids[0] == "LikeAmount")
                {
                    int.TryParse(grids[1], out PostSetting.LikeAmount);
                }
                else if (grids[0] == "CommentAmount")
                {
                    int.TryParse(grids[1], out PostSetting.CommentAmount);
                }
                else if (grids[0] == "ShareAmount")
                {
                    int.TryParse(grids[1], out PostSetting.ShareAmount);
                }
                else if (grids[0] == "Content")
                {
                    PostSetting.Content[0] = grids[1];
                    Debug.LogError(grids[1]);
                }
            }
            else
            {
                if (grids[0] == "UserName")
                {
                    Comments[currentIndex].UserName = grids[1];
                }
                else if (grids[0] == "Date")
                {
                    Comments[currentIndex].Date = grids[1];
                }
                else if (grids[0] == "LikeAmount")
                {
                    int.TryParse(grids[1], out Comments[currentIndex].LikeAmount);
                }
                else if (grids[0] == "CommentAmount")
                {
                    int.TryParse(grids[1], out Comments[currentIndex].CommentAmount);
                }
                else if (grids[0] == "ShareAmount")
                {
                    int.TryParse(grids[1], out Comments[currentIndex].ShareAmount);
                }
                else if (grids[0] == "Content")
                {
                    Comments[currentIndex].Content[0] = grids[1];
                    currentIndex++;
                }
            }
        }
    }
}
