using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using Utility;

public class PBTitleUIInstance : PBContentPageUI, IPointerClickHandler
{
    [SerializeField] PBContentPageUI PostContentPage;
    public DialogPassCatchRunner passRunner;
    public UnityEvent EnterPageEvent;

    public void OnPointerClick(PointerEventData eventData)
    {
        PostContentPage.gameObject.SetActive(true);
        transform.parent.gameObject.SetActive(false);
        EnterPageEvent?.Invoke();
        BaseUnderToolBar.Instance.ReturnBtnRegistEvent(ReturnToPreviewPanel);
    }
    private void ReturnToPreviewPanel()
    {
        PostContentPage.gameObject.SetActive(false);
        transform.parent.gameObject.SetActive(true);
        BaseUnderToolBar.Instance.ReturnBtnRemoveEvent(ReturnToPreviewPanel);
    }
     

    private void Start()
    {
        PostContentPage.transform.SetParent(transform.parent.parent);
        PostContentPage.transform.localPosition = Vector3.zero;
        PostContentPage.gameObject.SetActive(false);
        PostContentPage.CurrentPostData = CurrentPostData;
        PostContentPage.ResetData();

        var pBPostData = (SCO_ProBearPost)CurrentPostData;

        if (passRunner == null)
        {
            return;
        }

        if (pBPostData.YarnProgram != null)
        {
            passRunner.transform.gameObject.SetActive(true);
            passRunner.BluePrints = pBPostData.YarnProgram;
        }
        else
        {
            passRunner.transform.gameObject.SetActive(false);
        }
    }
}
