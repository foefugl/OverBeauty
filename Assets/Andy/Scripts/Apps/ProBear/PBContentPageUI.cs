using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PBContentPageUI : PostUIInstance
{
    public PBTradeUIInstance pbTradeUIInstance;

    private void Start()
    {
        if (pbTradeUIInstance != null)
        {
            pbTradeUIInstance.CurrentPost = (SCO_ProBearPost)CurrentPostData;
        } 
    }
    public void Return()
    {
        gameObject.SetActive(false);
        transform.parent.GetChild(0).gameObject.SetActive(true);
    }
    public void ResetData()
    {
        if (CurrentPostData.PostSetting.Avatar != null)
            PostUserAvatar.sprite = CurrentPostData.PostSetting.Avatar;
        else
            PostUserAvatar.enabled = false;

        PostUserName.text = CurrentPostData.PostSetting.UserName;
        PostContent.text = CurrentPostData.PostSetting.Content[0];
    }
}
