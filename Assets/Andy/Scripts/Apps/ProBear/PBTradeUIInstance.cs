using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Utility;
using TMPro;

public class PBTradeUIInstance : MonoBehaviour, IDialogueInteract
{
    public SCO_ProBearPost CurrentPost;
    public TextMeshProUGUI text;
    public DialogPassCatchRunner DialogRunner;
    public GameObject ContactPanel;
    private bool opening;
    
    private void Start()
    {
        if (CurrentPost.SaleType == PBSaleType.Buy)
        {
            if (CurrentPost.KeywordDatas.Length >= 0)
            {
                for (int i = 0; i < CurrentPost.KeywordDatas.Length; i++)
                {
                    var cueDetector = text.gameObject.AddComponent<KeywordDataDetector>();
                    cueDetector.data = CurrentPost.KeywordDatas[i];
                    DialogRunner.AfterSendResponseEvent.AddListener(cueDetector.ScanTxt);
                }
            }
        }
        else if (CurrentPost.SaleType == PBSaleType.Sale)
        {

        }

        
    }
    public void OnOffContactPanel()
    {
        opening = !opening;
        ContactPanel.SetActive(opening);
    }
    
    public void AddResponse(Post post)
    {
        text.text = post.Content[0];
    }
}
