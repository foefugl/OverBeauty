using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utility;

public class ProBearPostManager : CommunityPostManager
{
    public override GameObject AddPost(ScriptableObject _targetPost)
    {
        GameObject post = base.AddPost(_targetPost);
        SCO_ProBearPost bearpost = (SCO_ProBearPost)_targetPost;
        if (bearpost.EnterEvnets != null)
        {
            PBTitleUIInstance ui = post.GetComponent<PBTitleUIInstance>();
            ui.EnterPageEvent = bearpost.EnterEvnets;
        }

        return post;
    }
    public override void SendNotifyPost(Post _targetPost)
    {
        // ProBear don't send notification
        //base.SendNotifyPost(_targetPost);
    }
}
