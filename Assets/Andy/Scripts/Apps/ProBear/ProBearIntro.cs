using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProBearIntro : MonoBehaviour
{
    public Sprite OnLogo, OffLogo;
    [SerializeField] Image BGPanel, Logo;
    [SerializeField] Color OnColor, OffColor;
    [SerializeField] Button ActiveBtn, BrownBtn;
    private bool running;
    private void Start()
    {
        running = false;
        BrownBtn.interactable = false;
    }

    public void ONOFFSwitch()
    {
        running = !running;
        BrownBtn.interactable = running;

        if (running)
        {
            ActiveBtn.GetComponentInChildren<Text>().text = "����";
            BGPanel.color = OnColor;
            Logo.sprite = OnLogo;
        }
        else
        {
            ActiveBtn.GetComponentInChildren<Text>().text = "�Ұ�";
            BGPanel.color = OffColor;
            Logo.sprite = OffLogo;
        }
    }
}
