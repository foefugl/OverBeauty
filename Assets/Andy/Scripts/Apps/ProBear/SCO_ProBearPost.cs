using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Yarn.Unity;

public enum PBSaleType
{
    None,
    Buy,
    Sale
}

[CreateAssetMenu(fileName = "ProBearPost", menuName = "SCO/ProBearPost", order = 8)]
public class SCO_ProBearPost : SCO_Post
{
    public int Price;
    public YarnProgram[] YarnProgram;
    public PBSaleType SaleType;
    public KeywordData[] KeywordDatas;
    public UnityEvent EnterEvnets;
}