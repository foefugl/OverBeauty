using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WinkCommunityManager : CommunityPostManager
{
    [Space(10)]
    [Header("[ WinkCommunityManager ]")]
    public AppsUIController uIController;
    public WinkClubPostManager ClubPostManagerPrefab;
    public WinkClubThumbnailUIInstance PublicClubThumbailPrefab;
    public Transform ThumbnailTrans;
    public Transform ClubRoomsPanel;
    public Transform PreviewCoverBoard, InRoomCoverBoard;
    private Dictionary<string, WinkClubPostManager> clubsManager;

    protected override void Initial()
    {
        clubsManager = new Dictionary<string, WinkClubPostManager>();
        for (int i = 0; i < postsArray.Count; i++)
        {
            SCO_WinkClubRoom clubData = (SCO_WinkClubRoom)postsArray[i];
            WinkClubPostManager clubpostManager = Instantiate(ClubPostManagerPrefab, ClubRoomsPanel);
            clubpostManager.gameObject.name = clubData.PostSetting.UserName;
            clubpostManager.ClubRoomData = clubData;
            clubpostManager.transform.localPosition = Vector3.zero;
            
            for (int j = 0; j < clubData.DefaultPosts.Length; j++)
            {
                clubpostManager.postsArray.Add(clubData.DefaultPosts[j]);
            }

            clubsManager.Add(clubData.Key, clubpostManager);
        }

        base.Initial();
    }

    public override GameObject AddPost(ScriptableObject _targetPost)
    {
        SCO_Post scoPost = (SCO_Post)_targetPost;
        GameObject newPost = null;
        if (postsDic.ContainsKey(scoPost.Key))
        {
            //Debug.LogError("Contain same key");
            UpdatePostContent(scoPost);
            newPost = postsDic[scoPost.Key].gameObject;
        }
        else
        {
            SCO_WinkClubRoom clubRoomData = (SCO_WinkClubRoom)_targetPost;
            if (clubRoomData.ClubType == ClubType.Private)
            {
                PostUIInstance post = Instantiate(PostPrefab, ScrollTransform);
                postsDic.Add(scoPost.Key, post);

                var clubUi = (WinkClubUIInstance)post;
                clubUi.enterRoomEvent.AddListener(
                    delegate { SwitchPanel(scoPost.Key); }
                    );
                post.transform.SetAsFirstSibling();
                post.InitialDatas(scoPost);

                //SendNotifyPost(scoPost.PostSetting);
                newPost = postsDic[scoPost.Key].gameObject;
                newPost.transform.SetSiblingIndex(1);
            }
            else
            {
                PostUIInstance post = Instantiate(PublicClubThumbailPrefab, ThumbnailTrans);
                postsDic.Add(scoPost.Key, post);
                var clubUi = (WinkClubThumbnailUIInstance)post;
                clubUi.enterRoomEvent.AddListener(
                    delegate { SwitchPanel(scoPost.Key); }
                    );
                post.transform.SetAsFirstSibling();
                post.InitialDatas(scoPost);

                //SendNotifyPost(scoPost.PostSetting);
                newPost = postsDic[scoPost.Key].gameObject;
                newPost.transform.SetAsFirstSibling();
            }

            SettingClubPostAdder(newPost, clubRoomData);
        }
        
        return newPost;
    }

    private void SettingClubPostAdder(GameObject _clubInstance, SCO_WinkClubRoom _clubData)
    {
        if (_clubData.AttachPrefabs == null)
        {
            return;
        }

        WinkPostsManager _winkPostsManager = FindObjectOfType<WinkPostsManager>();
        if (_clubData.AttachPrefabs.Length != 0)
        {
            for (int i = 0; i < _clubData.AttachPrefabs.Length; i++)
            {
                var _prefab = _clubData.AttachPrefabs[i];
                var _prefabInstance = Instantiate(_prefab, _clubInstance.transform);
                RandomPost randomPost = _prefabInstance.GetComponent<RandomPost>();
                GroupPostAdder groupPostAdder = _prefabInstance.GetComponent<GroupPostAdder>();

                if (randomPost != null)
                {
                    randomPost.OnDutyManagers = new CommunityPostManager[] { clubsManager[_clubData.Key] };
                }
                if (groupPostAdder != null)
                {
                    groupPostAdder.OnDutyManagers = new CommunityPostManager[] { clubsManager[_clubData.Key] };
                }
            }
        }
    }

    public void ReturnPreviewPanel()
    {
        PreviewCoverBoard.SetAsLastSibling();
        ScrollTransform.parent.SetAsLastSibling();
        if (BaseUnderToolBar.Instance != null)
        {
            BaseUnderToolBar.Instance.ReturnBtnRemoveEvent(ReturnPreviewPanel);
        }

        //if(openedPRoom != "")
        //    personalRoomsDic[openedPRoom].messagerPivot.parent.gameObject.SetActive(false);
    }
    protected void SwitchPanel(string key)
    {
        PreviewCoverBoard.SetAsLastSibling();
        ClubRoomsPanel.SetAsLastSibling();

        InRoomCoverBoard.SetAsLastSibling();
        clubsManager[key].transform.SetAsLastSibling();

        if (BaseUnderToolBar.Instance != null)
        {
            BaseUnderToolBar.Instance.ReturnBtnRegistEvent(ReturnPreviewPanel);
        }
    }
}
