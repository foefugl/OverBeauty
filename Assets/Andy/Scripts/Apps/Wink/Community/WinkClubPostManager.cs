using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Utility;

public class WinkClubPostManager : CommunityPostManager
{
    [Space(10)]
    [Header("[ WinkClubPostManager ]")]
    public SCO_WinkClubRoom ClubRoomData;
    public Image Avatar;
    public Image Banner;
    public TextMeshProUGUI ClubName;
    public TextMeshProUGUI StarComment;

    private WinkPostsManager winkPostsManager;

    protected override void Initial()
    {
        winkPostsManager = FindObjectOfType<WinkPostsManager>();

        base.Initial();

        Avatar.sprite = ClubRoomData.PostSetting.Avatar;
        if (ClubRoomData.PostSetting.Photos.Length != 0)
        {
            Banner.sprite = ClubRoomData.PostSetting.Photos[0];
        }
 
        ClubName.text = ClubRoomData.PostSetting.UserName;
        float percentage = ClubRoomData.FansPercentageInOne();
        float starAmount = percentage * 5;
        StarComment.text = starAmount.ToString("0.0");
    }

    public override GameObject AddPost(ScriptableObject _targetPost)
    {
        GameObject newPost = base.AddPost(_targetPost);
        newPost.transform.SetSiblingIndex(2);

        if (winkPostsManager != null)
        {
            if (ClubRoomData.ClubType == ClubType.Public)
            {
                winkPostsManager.AddPost(_targetPost);
            }
        }
        
        return newPost;
    }
    public override void SendNotifyPost(Post _targetPost)
    {
        if (ClubRoomData.ClubType == ClubType.Private && !ClubRoomData.GotPass)
        {
            return;
        }

        base.SendNotifyPost(_targetPost);
    }
}
