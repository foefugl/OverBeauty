using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class WinkClubThumbnailUIInstance : PostUIInstance, IPointerClickHandler
{
    public Image Thumbnail;
    public UnityEvent enterRoomEvent;
    private SCO_WinkClubRoom clubRoomSetting;

    public override void InitialDatas(SCO_Post _sCO_Post)
    {
        base.InitialDatas(_sCO_Post);
        clubRoomSetting = (SCO_WinkClubRoom)_sCO_Post;
        Thumbnail.sprite = clubRoomSetting.Thumbnail;
    }
    public void OnPointerClick(PointerEventData eventData)
    {
        enterRoomEvent?.Invoke();
        Debug.Log("Enter Club " + clubRoomSetting.PostSetting.UserName);
    }
}
