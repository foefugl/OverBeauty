using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class WinkClubUIInstance : PostUIInstance, IPointerClickHandler
{
    public Image BannerImage;
    public Image FansAmountImage;
    public Text MaxFansAmount;
    public Text FansAmount;
    public Text ClubTypeTxt;
    public Text StarTxt;
    public UnityEvent enterRoomEvent;
    public string ClubPassWord { get { return clubRoomSetting.PassWord; } }

    private SCO_WinkClubRoom clubRoomSetting;

    private void Start()
    {
        RegistFansValue();
    }

    public override void InitialDatas(SCO_Post _sCO_Post)
    {
        base.InitialDatas(_sCO_Post);

        clubRoomSetting = (SCO_WinkClubRoom)_sCO_Post;
        BannerImage.sprite = clubRoomSetting.PostSetting.Photos[0];

        string type = "";
        if (clubRoomSetting.ClubType == ClubType.Private)
            type = "�p�K";
        else
            type = "���}";
        ClubTypeTxt.text = type;

        if (clubRoomSetting.ClubType == ClubType.Private)
        {
            clubRoomSetting.EnterEvent.AddListener(EnterPrivateRoom);
        }
        
        enterRoomEvent.AddListener(() => clubRoomSetting.EnterEvent?.Invoke());

        MaxFansAmount.text = clubRoomSetting.FansMax.ToString();
        //float percentage = ((float)clubRoomSetting.FansAmount / (float)clubRoomSetting.FansMax);
        float percentage = clubRoomSetting.FansPercentageInOne();
        FansAmountImage.fillAmount = percentage;
        float starAmount = percentage * 5;
        StarTxt.text = starAmount.ToString("0.0");
        percentage = percentage * 100;
        percentage = (int)percentage;
        FansAmount.text = clubRoomSetting.FansAmount + " (" + percentage + ") %";
    }

    private void RegistFansValue()
    {
        BasicDataValueManager.Instance.RegistClub(clubRoomSetting.PostSetting.UserName, AddMinusPercentage);
    }
    private void AddMinusPercentage(float _percentage)
    {
        float addMinusValue  = _percentage * (clubRoomSetting.FansMax * 0.01f);
        clubRoomSetting.FansAmount += addMinusValue;
        InitialDatas(clubRoomSetting);
    }
    public void EnterPrivateRoom()
    {
        clubRoomSetting.GotPass = true;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (!clubRoomSetting.GotPass && clubRoomSetting.ClubType == ClubType.Private)
        {
            PassWordNotifyPanel.Instance.Show(this);
            return;
        }

        enterRoomEvent?.Invoke();
        Debug.Log("Enter Club " + clubRoomSetting.PostSetting.UserName);
    }

    public void SetFanAmount(int _value)
    {
        clubRoomSetting.FansAmount = _value;
    }
}
