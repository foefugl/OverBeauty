using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class PostPointerClickEvent : MonoBehaviour, IPointerClickHandler
{
    public Action ClickEvent = () => 
    {
        Debug.LogError("Pointer Click");
    };
    public void OnPointerClick(PointerEventData eventData)
    {
        ClickEvent?.Invoke();
    }

}
