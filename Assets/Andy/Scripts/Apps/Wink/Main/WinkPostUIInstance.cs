using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using Utility;

public class WinkPostUIInstance : PostUIInstance, IDialogueInteract
{
    [Space(10)]
    [Header("[ WinkPost ]")]
    public Image Photo;
    public Text LikeAmount, CommentAmount, ShareAmount;
    public GameObject PhotoObj, SharePostObj;
    public Transform CommentParent;
    public Button CommentReplyBtn;
    public DialogPassCatchRunner DialogueRunner;
    public CommentUIInstance CommentPrefab;

    public override void InitialDatas(SCO_Post sCO_Post)
    {
        base.InitialDatas(sCO_Post);

        SCO_WinkPost winkPostSetting = (SCO_WinkPost)sCO_Post;

        if (winkPostSetting.SharePost == null)
        {
            if (PhotoObj != null)
                PhotoObj.SetActive(true);
            if (SharePostObj != null)
                SharePostObj.SetActive(false);
        }
        else
        {
            PhotoObj.SetActive(false);
            var sharePost = SharePostObj.GetComponent<WinkPostUIInstance>();
            sharePost.InitialDatas(winkPostSetting.SharePost);
            SharePostObj.SetActive(true);
        }

        PhotoSetting(winkPostSetting);
        SetPostCommentAmount(winkPostSetting);
        ReplayDialogSetting(winkPostSetting);
        CommentsSetting(winkPostSetting);
    }

    public void AddResponse(Post post)
    {
        CommentUIInstance comment = Instantiate(CommentPrefab, CommentParent);
        SCO_WinkPost winkPost = (SCO_WinkPost)CurrentPostData;
        comment.gameObject.name = winkPost.BluePrints[0].name + "-Comment";
        comment.UserName.text = post.UserName;
        comment.content.text = post.Content[0];

        WinkPostsManager.Instance.SendNotifyPost(post);
    }
    private void PhotoSetting(SCO_WinkPost _winkPost)
    {
        if (_winkPost.PostSetting.Photos.Length == 0)
        {
            Photo.enabled = false;
            Photo.sprite = null;

        }
        else
        {
            if (_winkPost.PostSetting.Photos[0] != null)
            {
                Photo.sprite = _winkPost.PostSetting.Photos[0];
            }

            if (_winkPost.PhotoMissionData != null)
            {
                if (_winkPost.PhotoMissionData.ActiveKey != "")
                {
                    var postPointer = Photo.gameObject.AddComponent<PostPointerClickEvent>();
                    postPointer.ClickEvent += () => 
                    {
                        MissionManager.Instance.MissionCompleted(_winkPost.PhotoMissionData.ActiveKey, _winkPost.PhotoMissionData.DutyIndex);
                    };
                }
            }
        }
    }
    private void CommentsSetting(SCO_WinkPost _winkPost)
    {
        for (int i = 0; i < _winkPost.Comments.Length; i++)
        {
            Post commentPost = _winkPost.Comments[i];
            CommentUIInstance comment = Instantiate(CommentPrefab, CommentParent);
            comment.UserName.text = commentPost.UserName;
            comment.content.text = commentPost.Content.ElementAtOrDefault(0);
            comment.LikeAmount.text = commentPost.LikeAmount.ToString();

            if (commentPost.missionData != null)
            {
                if (commentPost.missionData.ActiveKey != "")
                {
                    var chopdetect = comment.gameObject.AddComponent<CueChopDetector>();
                    chopdetect.data = new MissionData(commentPost.missionData.ActiveKey, commentPost.missionData.DutyIndex, commentPost.missionData.CueContent);
                }
            }
        }
    }
    private void ReplayDialogSetting(SCO_WinkPost _winkPost)
    {
        if (CommentReplyBtn == null)
        {
            return;
        }

        if (_winkPost.BluePrints == null)
        {
            CommentReplyBtn.gameObject.SetActive(false);
        }
        else
        {
            if (_winkPost.BluePrints.Length == 0)
            {
                CommentReplyBtn.gameObject.SetActive(false);
            }
            else
            {
                CommentReplyBtn.gameObject.SetActive(true);
                CommentReplyBtn.interactable = true;
            }

            DialogueRunner.BluePrints = _winkPost.BluePrints;
            DialogueRunner.gameObject.SetActive(true);
        }
    }
    private void SetPostCommentAmount(SCO_WinkPost _winkPost)
    {
        if (LikeAmount != null)
        {
            if (_winkPost.PostSetting.LikeAmount >= 1000)
            {
                double d_value = _winkPost.PostSetting.LikeAmount / 1000;
                double value = Math.Round(d_value, 1, MidpointRounding.AwayFromZero);
                string like = value.ToString() + "K";
                LikeAmount.text = like;
            }
            else
            {
                LikeAmount.text = _winkPost.PostSetting.LikeAmount.ToString();
            }
        }

        if (CommentAmount != null)
        {
            if (_winkPost.PostSetting.CommentAmount >= 1000)
            {
                double d_value = _winkPost.PostSetting.CommentAmount / 1000;
                double value = Math.Round(d_value, 1, MidpointRounding.AwayFromZero);
                string like = value.ToString() + "K";
                CommentAmount.text = like;
            }
            else
            {
                CommentAmount.text = _winkPost.PostSetting.CommentAmount.ToString();
            }
        }

        if (ShareAmount != null)
        {
            if (_winkPost.PostSetting.ShareAmount >= 1000)
            {
                double d_value = _winkPost.PostSetting.ShareAmount / 1000;
                double value = Math.Round(d_value, 1, MidpointRounding.AwayFromZero);
                string like = value.ToString() + "K";
                ShareAmount.text = like;
            }
            else
            {
                ShareAmount.text = _winkPost.PostSetting.ShareAmount.ToString();
            }
        }
    }
}
