using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Yarn.Unity;

public class WinkPostsManager : CommunityPostManager
{
    private static WinkPostsManager instance;
    public static WinkPostsManager Instance 
    {
        get 
        {
            return instance;
        }
    }
    public GameObject PersonalPage;
    private bool personalPageCloseDelay;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public override GameObject AddPost(ScriptableObject _targetPost)
    {
        GameObject newPost = base.AddPost(_targetPost);
        newPost.transform.SetSiblingIndex(1);
        return newPost;
    }
    [YarnCommand("SetLikeAmount")]
    public void SetLikeAmount(string comment, string amount)
    {
        StartCoroutine(WaitCommentCreatedSendLike(comment, amount));
    }
    public void ClosePersonalPage()
    {
        if (!personalPageCloseDelay)
        {
            PersonalPage.transform.SetAsFirstSibling();
        }
    }
    public void OpenPersonalPage()
    {
        PersonalPage.transform.SetAsLastSibling();
        StartCoroutine(WaitLoadPersonalPage());
    }
    IEnumerator WaitCommentCreatedSendLike(string comment, string amount)
    {
        yield return new WaitForSeconds(1);
        Debug.Log(comment);
        GameObject.Find(comment).GetComponent<CommentUIInstance>().SetLikeAmount(amount);
    }
    IEnumerator WaitLoadPersonalPage()
    {
        personalPageCloseDelay = true;
        yield return new WaitForSeconds(1f);
        personalPageCloseDelay = false;
    }
}