using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class FirstLogInPage : MonoBehaviour
{
    public Image LogoPanel;
    public InputField InputField;
    public GameObject loginInfoPage;
    public bool run;

    private void Update()
    {
        if (run)
        {
            run = false;
            StartLogIn();
        }
    }

    public void StartLogIn()
    {
        Color fadeColor = LogoPanel.color;
        DOTween.ToAlpha(() => fadeColor, x => fadeColor = x, 0, 1)
            .SetDelay(1.25f)
            .OnUpdate(() => 
            {
                LogoPanel.color = fadeColor;
            });
    }
    public void LogIn()
    {
        if (InputField.text != "")
        {
            Color fadeColor = LogoPanel.color;
            DOTween.ToAlpha(() => fadeColor, x => fadeColor = x, 1, 1)
                .OnUpdate(() =>
                {
                    LogoPanel.color = fadeColor;
                })
                .OnComplete(() =>
                {
                    loginInfoPage.SetActive(false);
                    Color fadeColor = LogoPanel.color;
                    DOTween.ToAlpha(() => fadeColor, x => fadeColor = x, 0, 1)
                        .OnUpdate(() =>
                        {
                            LogoPanel.color = fadeColor;
                        });
                });
        }
    }
}
