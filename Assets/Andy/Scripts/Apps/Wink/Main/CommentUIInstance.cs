using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Yarn.Unity;

public class CommentUIInstance : MonoBehaviour
{
    public Text UserName;
    public Text LikeAmount;
    public TextMeshProUGUI content;
    
    public void SetLikeAmount(string value)
    {
        Debug.Log("SetLikeAmount: " + value);
        int intvalue = int.Parse(value);
        StartCoroutine(DelayAddCount(intvalue));
    }
    IEnumerator DelayAddCount(int value)
    {
        int delay = Random.Range(1,6);
        int count = 0;
        while (count < value)
        {
            yield return new WaitForSeconds(delay);
            count++;
            LikeAmount.text = count.ToString();
            delay = Random.Range(1, 3);
        }
    }
}
