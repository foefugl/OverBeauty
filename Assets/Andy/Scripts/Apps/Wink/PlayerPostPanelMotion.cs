using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PlayerPostPanelMotion : MonoBehaviour
{
    public Transform targetPosTrans;
    private Vector3 oriPos;
    private bool opening;

    private void Start()
    {
        oriPos = transform.position;
    }

    public void OpenPostPanel()
    {
        opening = true;
        transform.DOMove(targetPosTrans.position, 0.5f).SetEase(Ease.OutBounce);
    }
    public void ClosePostPanel()
    {
        opening = false;
        transform.DOMove(oriPos, 0.25f);
    }
}
