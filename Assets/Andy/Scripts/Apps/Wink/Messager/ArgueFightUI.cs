using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class ArgueFightUI : MonoBehaviour
{
    public RectTransform ScrolRectTrans;
    public Image bloodImage;
    private Color oriColor;

    private void Start()
    {
        oriColor = bloodImage.color;
        StartCoroutine(UIMotion());
    }
    IEnumerator UIMotion()
    {
        Vector3 anchor = ScrolRectTrans.anchoredPosition;
        DOTween.Shake(() => anchor, x => anchor = x, 2, 10, 15)
            .OnUpdate(() => 
            {
                ScrolRectTrans.anchoredPosition = anchor;
            });
        yield return new WaitForSeconds(3f);
        bloodImage.DOColor(new Color(oriColor.r, oriColor.g, oriColor.b, 0.4f), 1);
    }
}
