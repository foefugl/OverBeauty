using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Utility;
using Yarn.Unity;

public class WinkPersonalRoomDialogue : PersonalRoomDialogue
{
    [Space(10)]
    [Header("[ WinkPersonalRoom ]")]
    public GameObject ArgueFightUI;
    public GameObject PlayerImageReply;
    public ChatRoomDateUIInstance ChatRoomDatePrefab;

    private string lastDate;
    private WinkMessageManager winkMessageManager;
    public override void AddResponse(Post post)
    {
        LatestPost = post;
        if (lastDate != GamePlayTime.Instance.DateTxt.text)
        {
            Debug.Log(GamePlayTime.Instance.DateTxt.text);
            lastDate = GamePlayTime.Instance.DateTxt.text;
            var chatRoomDate = Instantiate(ChatRoomDatePrefab, messagerPivot);
            chatRoomDate.SetDate(lastDate);
            chatRoomDate.gameObject.SetActive(true);
        }
        winkMessageManager.AddPersonalMessage(roomData.Key, post.UserName, post.Content[0]);
    }
    public IEnumerator InitialAtEndOfFrame()
    {
        //yield return new WaitForSeconds();
        yield return null;
        base.Start();
        winkMessageManager = GetComponentInParent<WinkMessageManager>();
        transform.SetParent(winkMessageManager.transform);
    }

    [YarnCommand("StartArgue")]
    public void StartArgue()
    {
        Debug.Log("StartArgue");
        ArgueFightUI.SetActive(true);
    }
    [YarnCommand("SendPhoto")]
    public void SendPhoto(string _key)
    {
        PlayerPostBag bag = GameObject.FindObjectOfType<PlayerPostBag>();
        SCO_Post sCO_Post = bag.FindPostData(_key);
        if (sCO_Post != null)
        {
            GameObject photoReply = Instantiate(PlayerImageReply, messagerPivot);
            photoReply.GetComponentInChildren<Image>().sprite = sCO_Post.PostSetting.Photos[0];
        }
    }
}
