using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class WinkMessageUIInstance : PostUIInstance
{
    [Space(10)]
    [Header("[ WinkMessageUIInstance ]")]
    public GameObject UpdateIcon;
    public Image TxtBG;
    public TextMeshProUGUI ContentText;
    private void Start()
    {
        StartCoroutine(DelayToReveal());
    }
    IEnumerator DelayToReveal()
    {
        yield return null;
        if (TxtBG != null)
        {
            TxtBG.enabled = true;
        }
        if (ContentText != null)
        {
            ContentText.enabled = true;
        }
    }
}
