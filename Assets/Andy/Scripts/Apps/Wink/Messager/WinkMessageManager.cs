using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Utility;
using TMPro;


public class WinkMessageManager : CommunityPostManager
{
    [Space(10)]
    [Header("[ WinkMessageManager ]")]
    public AppsUIController uIController;
    public GameObject WinkUnderToolBar;
    public Transform PreviewPanel;
    public Transform PersonalRoomPanel;
    public Transform PreviewCoverBoard, InRoomCoverBoard;
    public GameObject PMRoomStructPrefab;
    public PostUIInstance PMRoomUIPrefab;
    public PostUIInstance PMUIInstance, PlayerReplyInstance;

    [Space(10)]
    public GameObject UnreadIcon;
    public GameObject chatroom_Name;

    private string openedPRoom = "";
    private Action messClickEvent;
    private Dictionary<string, WinkPersonalRoomDialogue> personalRoomsDic;

    protected override void Initial()
    {
        personalRoomsDic = new Dictionary<string, WinkPersonalRoomDialogue>();

        for (int i = 0; i < postsArray.Count; i++)
        {
            AddPost(postsArray[i]);
        }
    }
    protected override void Update()
    {
        base.Update();
        if (openedPRoom != "")
        {
            WinkMessageUIInstance winkMes = (WinkMessageUIInstance)postsDic[openedPRoom];
            if (winkMes.UpdateIcon != null)
            {
                winkMes.UpdateIcon.SetActive(false);
                chatroom_Name.gameObject.SetActive(false);
            }
        }
    }

    public void ReturnPreviewPanel()
    {
        PreviewCoverBoard.SetAsLastSibling();
        PreviewPanel.SetAsLastSibling();

        GamePlayTime.Instance.TimeFreeze = false;
        if (BaseUnderToolBar.Instance != null)
        {
            BaseUnderToolBar.Instance.ReturnBtnRemoveEvent(ReturnPreviewPanel);
        }
        if (WinkUnderToolBar != null)
        {
            WinkUnderToolBar.SetActive(true);
        }
        openedPRoom = "";

        //if(openedPRoom != "")
        //    personalRoomsDic[openedPRoom].messagerPivot.parent.gameObject.SetActive(false);
    }
    protected void SwitchPanel(string key)
    {
        openedPRoom = key;
        PreviewCoverBoard.SetAsLastSibling();
        PersonalRoomPanel.SetAsLastSibling();
        InRoomCoverBoard.SetAsLastSibling();

        personalRoomsDic[key].messagerPivot.parent.SetAsLastSibling();
        GamePlayTime.Instance.TimeFreeze = true;
        if (BaseUnderToolBar.Instance != null)
        {
            BaseUnderToolBar.Instance.ReturnBtnRegistEvent(ReturnPreviewPanel);
        }
        if (WinkUnderToolBar != null)
        {
            WinkUnderToolBar.SetActive(false);
        }
    }

    public override GameObject AddPost(ScriptableObject _roomData)
    {
        SCO_WinkMessageRoom scoRoomData = (SCO_WinkMessageRoom)_roomData;
        if (postsDic.ContainsKey(scoRoomData.Key))
        {
            Debug.LogError("Contain same key: " + scoRoomData.Key);
            return postsDic[scoRoomData.Key].gameObject;
        }
        else
        {
            PostUIInstance roomPreviewUI = Instantiate(PMRoomUIPrefab, ScrollTransform);
            roomPreviewUI.transform.SetAsFirstSibling();
            roomPreviewUI.InitialDatas(scoRoomData.members[0]);

            postsDic.Add(scoRoomData.Key, roomPreviewUI);

            var clickEvent = roomPreviewUI.gameObject.AddComponent<PostPointerClickEvent>();
            clickEvent.ClickEvent += () =>SwitchPanel(scoRoomData.Key);


            GameObject personalRoom = Instantiate(PMRoomStructPrefab, PersonalRoomPanel);
            personalRoom.transform.SetAsFirstSibling();
            var winkPersonalDialogue = personalRoom.GetComponentInChildren<WinkPersonalRoomDialogue>();
            winkPersonalDialogue.roomData = scoRoomData;
            personalRoomsDic.Add(scoRoomData.Key, winkPersonalDialogue);

            StartCoroutine(winkPersonalDialogue.InitialAtEndOfFrame());


            for (int j = 0; j < scoRoomData.members.Count; j++)
            {
                for (int k = 0; k < scoRoomData.members[j].PostSetting.Content.Count; k++)
                {
                    PostUIInstance message = Instantiate(PMUIInstance, personalRoom.transform.GetChild(0));
                    message.InitialDatas(scoRoomData.members[j]);
                }   
            }
            return postsDic[scoRoomData.Key].gameObject;
        }
    }
    public override void UpdatePostContent(string _roomKey)
    {
        var latestPost = personalRoomsDic[_roomKey].LatestPost;
        // This one is correct one
        //postsDic[_key].PostUserName.text = personalRoomsDic[_key].roomData.RoomName;

        // This one is wrong one
        postsDic[_roomKey].PostUserName.text = personalRoomsDic[_roomKey].roomData.members[0].PostSetting.UserName;
        postsDic[_roomKey].PostContent.text = latestPost.Content.LastOrDefault();
        WinkMessageUIInstance winkMes = (WinkMessageUIInstance)postsDic[_roomKey];
        if (winkMes.UpdateIcon)
        {
            winkMes.UpdateIcon.SetActive(true);
        }

        SendNotifyPost(latestPost);
    }


    public void AddPersonalMessage(string _roomKey, string _memberName, string _content)
    {
        GameObject personalRoom = personalRoomsDic[_roomKey].gameObject;
        PostUIInstance message;
        if (_memberName == "Player")
        {
            message = Instantiate(PlayerReplyInstance, personalRoomsDic[_roomKey].messagerPivot);
            if (CurrentMData.ActiveKey != "")
            {
                var chopdetect = message.gameObject.AddComponent<CueChopDetector>();
                chopdetect.data = new MissionData(CurrentMData.ActiveKey, CurrentMData.DutyIndex, CurrentMData.CueContent);
                CueChopsObserver.Instance.RegistDetector(CurrentMData.ActiveKey, chopdetect);
            }
            Post post = new Post();
            post.UserName = _memberName;
            post.Content = new List<string>();
            post.Content.Add(_content);
            message.CurrentPostData = (SCO_Post)ScriptableObject.CreateInstance("SCO_Post");
            message.CurrentPostData.PostSetting = post;
            message.InitialDatas(message.CurrentPostData);
        }
        else
        {
            message = Instantiate(PMUIInstance, personalRoomsDic[_roomKey].messagerPivot);
            if (CurrentMData.ActiveKey != "")
            {
                var chopdetect = message.gameObject.AddComponent<CueChopDetector>();
                chopdetect.data = new MissionData(CurrentMData.ActiveKey, CurrentMData.DutyIndex, CurrentMData.CueContent);
                CueChopsObserver.Instance.RegistDetector(CurrentMData.ActiveKey, chopdetect);

            }
            if (CurrentKeywordData !=  null)
            {
                StartCoroutine(DelayOneFrameToMarkKeyword(message));
            }

            if (messClickEvent != null)
            {
                var clickEvent = message.gameObject.GetComponentInChildren<PostPointerClickEvent>();
                clickEvent.ClickEvent = messClickEvent;
            }
            // 直接串改聊天室成員資料的第一個成員去符合yarn人名設定
            personalRoomsDic[_roomKey].roomData.members[0].PostSetting.UserName = _memberName;
            SCO_WinkMessage chatData = (SCO_WinkMessage)personalRoomsDic[_roomKey].roomData.members.Single(s => s.PostSetting.UserName == _memberName);
            chatData.PostSetting.Content.Add(_content);
            message.InitialDatas(chatData);
            message.UpdateContentByIndex(chatData.ContentIndex);
            chatData.ContentIndex++;

            // Add other response message
            if (BaseUnderToolBar.Instance.CurrentApp != AppPanelName.WinkMessager)
            {
                UnreadIcon.SetActive(true);
            }
        }

        messClickEvent = null;
        CurrentMData.ActiveKey = "";
        personalRoomsDic[_roomKey].messagerPivot.GetComponent<VerticalScrollSetPos>().StartScrollToBottom();
        UpdatePostContent(_roomKey);
    }
    IEnumerator DelayOneFrameToMarkKeyword(PostUIInstance postUIInstance)
    {
        yield return new WaitForSeconds(0.125f);
        var cueDetector = postUIInstance.PostContent.gameObject.AddComponent<KeywordDataDetector>();
        cueDetector.data = CurrentKeywordData;
        CurrentKeywordData = null;
        //postUIInstance.PostContent.GetComponent<PostPointerClickEvent>().ClickEvent;
    }
    public void AddClickEventOnReply(Action _action)
    {
        messClickEvent = _action;
    }
}
