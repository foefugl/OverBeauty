using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ChatRoomDateUIInstance : MonoBehaviour
{
    public TextMeshProUGUI text;
    public void SetDate(string _date)
    {
        //string[] datas = _date.Split('/');
        //text.text = datas[1] + "/" + datas[2];
        text.text = _date;
    }
}
