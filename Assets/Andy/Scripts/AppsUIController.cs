using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[ExecuteInEditMode]
public class AppsUIController : MonoBehaviour
{
    public GameObject CoverBoard;

    [Serializable]
    public struct AppNameObjectPair
    {
        public AppPanelName Name;
        public GameObject AppObject;
        public UnityEvent OpenPanelEvent;
    }
    public AppPanelName CurrentPage;
    public AppNameObjectPair[] apps;

    public void SwitchApp(int _appNameIndex)
    {
        SwitchApp((AppPanelName)_appNameIndex);
        CurrentPage = (AppPanelName)_appNameIndex;
        BaseUnderToolBar.Instance.CurrentApp = (AppPanelName)_appNameIndex;
    }

    public void SwitchApp(AppPanelName _appName)
    {
        CoverBoard.transform.SetAsLastSibling();
        for (int i = 0; i < apps.Length; i++)
        {
            if (apps[i].Name == _appName)
            {
                apps[i].AppObject.transform.SetAsLastSibling();
                apps[i].OpenPanelEvent?.Invoke();
            }
        }
    }

}  
