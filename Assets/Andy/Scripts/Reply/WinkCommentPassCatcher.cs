using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinkCommentPassCatcher : DialogPassCatchRunner
{
    protected override IEnumerator DelaySendResponse()
    {
        SendDelay = Random.Range(5, 20);
        return base.DelaySendResponse();
    }
}
