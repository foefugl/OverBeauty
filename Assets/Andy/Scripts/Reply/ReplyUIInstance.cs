using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ReplyUIInstance : MonoBehaviour
{
    public string activeKey;
    public Text replyText;

    private Reply currentReply;

    public void SetData(Reply _reply)
    {
        currentReply = _reply;
        activeKey = _reply.activeKey;
        replyText.text = _reply.content;
    }
    public void ReplyTrigger()
    {
        Debug.Log(string.Format("Send reply '{0}' to catch pass system.", currentReply.content));
    }
}
