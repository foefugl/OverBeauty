using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ReplySystem : MonoBehaviour
{
    public GameObject UiContainer;
    public Text ResponseTxt;
    public ReplyUIInstance[] options;

    public void UpdateResponse(string content)
    {
        ResponseTxt.text = content;
    }
}
