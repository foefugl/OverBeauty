using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Yarn.Unity;
using Utility;

[RequireComponent(typeof(DialogueRunner))]
[RequireComponent(typeof(DialogueUI))]

public class DialogPassCatchRunner : MonoBehaviour
{
    public string targetNode;
    public bool StartNode;
    [Space(10)]
    public DialogueRunner dialogueRunner;
    public DialogueUI dialogueUI;
    public YarnProgram[] BluePrints;
    public bool RunImmediately;
    public Text responseUITxt;
    public Button dialogueActiveBtn;
    public GameObject PlayerTyping;
    public GameObject dialogueInteractObject;
    public GameObject replySystemPrefab;

    public UnityEvent AfterSendResponseEvent;

    [Space(10)]
    [Header("[ Custom Setting ]")]
    public bool OptionAutoReveal = false;

    private IDialogueInteract dialogueInteract;
    private PointerDetector pointerDetector;
    private ReplySystem replySystem;
    private GameObject replyObj;
    private bool optionsRunning;
    protected float SendDelay = 0.1f;
    private List<string> ContineNodeList;
    private bool SwitchONOFF;

    private void Start()
    {
        // ����ݨDComponent

        if (BluePrints == null)
            return;
        if (BluePrints.Length == 0)
            return;

        ContineNodeList = new List<string>();

        dialogueInteract = dialogueInteractObject.GetComponent<IDialogueInteract>();
        pointerDetector = FindObjectOfType<PointerDetector>();
        pointerDetector.HoveredChanged.AddListener(CheckCurrentHoverd);
        dialogueRunner.variableStorage = FindObjectOfType<InMemoryVariableStorage>();
        dialogueRunner.enabled = true;
        dialogueRunner.yarnScripts = BluePrints;
        dialogueRunner.textLanguage = LanguageController.Instance.YarnLanguage;
        gameObject.name = BluePrints[0].name;
        List<Button> btns = new List<Button>();
        
        replyObj = Instantiate(replySystemPrefab, GameObject.Find("DialogueSavePanel").transform);
        replyObj.transform.localScale = Vector3.one;
        replyObj.name = BluePrints[0].name + " - Dialogue";
        replySystem = replyObj.GetComponentInChildren<ReplySystem>();
        replySystem.UiContainer.SetActive(false);

        ReplyUIInstance[] replys = replySystem.options;
        for (int i = 0; i < replys.Length; i++)
        {
            var btn = replys[i].transform.GetComponent<Button>();
            btns.Add(btn);
        }
        dialogueUI.optionButtons = btns;
    }
    private void Update()
    {
        dialogueActiveBtn.interactable = optionsRunning;

        if (StartNode)
        {
            StartNode = false;
            dialogueRunner.StartDialogue(targetNode);
        }
    }
    [YarnCommand("StartDialogueAtNode")]
    public void StartDialogueAtNode(string _nodeName)
    {
        if (ContineNodeList.Contains(_nodeName))
        {
            return;
        }
        ContineNodeList.Add(_nodeName);
        dialogueRunner.StartDialogue(_nodeName);
    }
    

    public void StartReply()
    {

        if (PlayerTyping != null)
        {
            PlayerTyping.SetActive(true);
            PlayerTyping.transform.SetAsLastSibling();
        }

        if (optionsRunning)
        {
            Debug.Log("StartReply: " + SwitchONOFF);
            SwitchONOFF = !SwitchONOFF;
            replySystem.UiContainer.SetActive(optionsRunning);
        }
    }
    public void LineFinish()
    {
        //if (RunImmediately)
        //    SendDelay = 0;
        //else
        //    SendDelay = 1;
        

        StartCoroutine(DelaySendResponse());

    }
    public void OnOptionEnd()
    {
        optionsRunning = false;
        if (PlayerTyping != null)
            PlayerTyping.SetActive(false);
        replySystem.UiContainer.gameObject.SetActive(false);
        SwitchONOFF = false;
    }
    public void OnOptionStart()
    {
        optionsRunning = true;
        for (int i = 0; i < dialogueUI.optionButtons.Count; i++)
        {
            string optionContent = dialogueUI.optionButtons[i].GetComponent<ReplyUIInstance>().replyText.text;
            dialogueUI.optionButtons[i].onClick.AddListener(() =>
            SendReply(optionContent));
        }
        if (OptionAutoReveal)
        {
            Debug.Log("StartReply: " + SwitchONOFF);
            SwitchONOFF = !SwitchONOFF;
            replySystem.UiContainer.SetActive(optionsRunning);
        }
    }
    public void OnDialogueEnd()
    {
        replySystem.UiContainer.gameObject.SetActive(false);
    }

    private void SendReply(string _replyContent)
    {
        Post replyPost = new Post();
        replyPost.UserName = "Player";
        replyPost.Content = new List<string>();
        replyPost.Content.Add(_replyContent);
        dialogueInteract.AddResponse(replyPost);
    }
    private void SendResponse()
    {
        Post responsePost = new Post();
        string filteString = responseUITxt.text.Replace("\n", "");
        string[] responseStrings = filteString.Split(':');

        if (responseStrings.Length >= 2)
        {
            responsePost.UserName = responseStrings[0];
            responsePost.Content = new List<string>();
            responsePost.Content.Add(responseStrings[1]);
        }
        else
        {
            responsePost.Content = new List<string>();
            responsePost.Content.Add(responseStrings[0]);
        }
        
        dialogueInteract.AddResponse(responsePost);
        AfterSendResponseEvent?.Invoke();
    }
    private void CheckCurrentHoverd()
    {
        if (pointerDetector.CurrenHovered == null)
        {
            replySystem.UiContainer.SetActive(false);
        }
        else if (pointerDetector.CurrenHovered.transform.root.name != replySystem.transform.root.name)
        {
            replySystem.UiContainer.SetActive(false);
        }
    }
    [YarnCommand("SetSendDelay")]
    public void SetSendDelay(string _delay)
    {
        float d = float.Parse(_delay);
        SendDelay = d;
    }
    protected virtual IEnumerator DelaySendResponse()
    {   
        yield return new WaitForSeconds(SendDelay);
        SendResponse();
        dialogueUI.MarkLineComplete();

    }
}
