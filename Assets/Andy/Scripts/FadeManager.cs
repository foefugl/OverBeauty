using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using DG.Tweening;

public class FadeManager : MonoBehaviour
{
    private static FadeManager instance;
    public static FadeManager Instance 
    {
        get
        {
            return instance;
        }
    }

    public float Duration = 0.65f;
    public bool Visible = true;
    public Image FadePanel;
    public UnityEvent FadeOutevent;

    private Tween fadeTwn;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    public void Run()
    {
        if (!Visible)
            FadePanel.enabled = true;

        Visible = !Visible;

        Color targetColor = (!Visible) ? new Color(0, 0, 0, 0) : Color.black;
        if (fadeTwn != null)
        {
            if (fadeTwn.IsPlaying())
            {
                fadeTwn.Kill();
            }
        }
        fadeTwn = FadePanel.DOColor(targetColor, (Visible)? Duration : 1 )
            .OnComplete(() => 
            {
                if (Visible)
                {
                    FadeOutevent?.Invoke();
                    FadeOutevent.RemoveAllListeners();
                }
                else
                {
                    FadePanel.enabled = false;
                }
            });
    }
    public void RunOnce()
    {
        Visible = true;
        FadePanel.enabled = true;

        if (fadeTwn != null)
        {
            if (fadeTwn.IsPlaying())
            {
                fadeTwn.Kill();
            }
        }

        Color targetColor = (!Visible) ? new Color(0, 0, 0, 0) : Color.black;
        fadeTwn = FadePanel.DOColor(targetColor, (Visible) ? Duration : 1)
            .OnComplete(() =>
            {
                FadeOutevent?.Invoke();
                Visible = false;
                targetColor = new Color(0, 0, 0, 0);
                FadePanel.DOColor(targetColor, (Visible) ? Duration : 1);
            });
    }
}
