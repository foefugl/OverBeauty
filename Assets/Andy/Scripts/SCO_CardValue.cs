using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "CardValue", menuName = "SCO/CardValue", order = 5)]
public class SCO_CardValue : ScriptableObject
{
    public int Credibility;
    public int ManHunt;
    public int money;
}
