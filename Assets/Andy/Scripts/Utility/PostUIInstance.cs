using System.Collections;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PostUIInstance : MonoBehaviour
{
    [Header("[ PostUI ]")]
    public Image PostUserAvatar;
    public Text PostUserName;
    public Text TimePass;
    public TextMeshProUGUI PostContent;

    public SCO_Post CurrentPostData;
    private int contentIndex = 0;
    private float timePass = 0.0f;

    private void OnEnable()
    {
        if (TimePass != null)
        {
            StartCoroutine(TimeCounter());
        }
    }

    public virtual void InitialDatas(SCO_Post _sCO_Post)
    {
        CurrentPostData = _sCO_Post;

        if (PostUserAvatar != null)
        {
            if (_sCO_Post.PostSetting.Avatar != null)
                PostUserAvatar.sprite = _sCO_Post.PostSetting.Avatar;
            else
                PostUserAvatar.enabled = false;
        }
        
        if (PostUserName != null)
            PostUserName.text = _sCO_Post.PostSetting.UserName;

        if (PostContent != null)
            PostContent.text = _sCO_Post.PostSetting.Content.ElementAtOrDefault(contentIndex);
    }
    public virtual void UpdateContentPerStep()
    {
        if (contentIndex == CurrentPostData.PostSetting.Content.Count - 1)
        {
            Debug.LogError(gameObject.name + " currentPostData don't have next content.");
            return;
        }
        contentIndex++;
        PostContent.text = CurrentPostData.PostSetting.Content[contentIndex];
        timePass = 0;
    }
    public virtual void UpdateContentByIndex(int _index)
    {
        contentIndex = _index;
        PostContent.text = CurrentPostData.PostSetting.Content[contentIndex];
        timePass = 0;
    }
    public virtual void OverlayContentToIndex(int _index)
    {
        PostContent.text = "";
        contentIndex = _index;
        for (int i = 0; i < _index; i++)
        {
            PostContent.text += CurrentPostData.PostSetting.Content[i] + '\n';
        }
        timePass = 0;
    }
    IEnumerator TimeCounter()
    {
        while (true)
        {
            if (CurrentPostData != null)
            {
                if (CurrentPostData.PostSetting.Date != null)
                {
                    TimePass.text = CurrentPostData.PostSetting.Date;
                    break;
                }
            }
            yield return null;
            //yield return new WaitForSecondsRealtime(60);
            //timePass++;
            //TimePass.text = timePass.ToString() + "�����e";
        }
    }
}
