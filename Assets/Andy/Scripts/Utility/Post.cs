using UnityEngine;
using System;
using System.Collections.Generic;

namespace Utility
{
    [Serializable]
    public class Post
    {
        public string UserName;
        public string Date;
        public int LikeAmount;
        public int CommentAmount;
        public int ShareAmount;

        public Sprite Avatar;
        public Sprite[] Photos;
        public MissionData missionData;
        public KeywordData keywordData;

        [TextArea]
        public List<string> Content;
    }
}
