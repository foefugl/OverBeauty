using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoroutineActive : MonoBehaviour
{
    private void Start()
    {
        gameObject.name = "(~CoroutineActive)";
    }
    public void RegistCoroutne(IEnumerator _coroutine)
    {
        StartCoroutine(_coroutine);
    }
}
public class PrefabEventLibrary : MonoBehaviour
{
    public void MissionComplete(string _keyAndIndex)
    {
        var missionManager = FindObjectOfType<MissionManager>();

        string[] content = _keyAndIndex.Split(',');
        int index = 0;
        int.TryParse(content[1], out index);
        missionManager.MissionCompleted(content[0], index);
    }
    public void PublishClubNewPost(string _clubKeyAndpostKey)
    {
        GameObject newOb = new GameObject();
        CoroutineActive coroutineObj = newOb.AddComponent<CoroutineActive>();
        coroutineObj.RegistCoroutne(DelayPublishClubPost(1, _clubKeyAndpostKey));
    }
    public void StartDialog(string _dialogObjAndNodeName)
    {
        string[] content = _dialogObjAndNodeName.Split(',');
        GameObject dialogueObj = GameObject.Find(content[0]);
        DialogPassCatchRunner runner = dialogueObj.GetComponent<DialogPassCatchRunner>();
        runner.StartDialogueAtNode(content[1]);
    }
    public void OpenWinkPersonalPage()
    {
        BaseUnderToolBar.Instance.OpenApp(AppPanelName.WinkHome);
        var winkPosts = FindObjectOfType<WinkPostsManager>();
        winkPosts.OpenPersonalPage();
    }
    public void OpenKnockPage()
    {
        KnockManager.Instance.OpenCanvas();
    }
    IEnumerator DelayPublishClubPost(float _delay, string _clubKeyAndpostKey)
    {
        yield return new WaitForSeconds(_delay);
        string[] content = _clubKeyAndpostKey.Split(',');
        var clubPoster = FindObjectsOfType<ClubCommandPoster>();
        for (int i = 0; i < clubPoster.Length; i++)
        {
            if (clubPoster[i].OnDutyClub.Key == content[0])
            {
                clubPoster[i].PostAdd(content[1]);
            }
        }
    }
}
