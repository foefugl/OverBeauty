using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PersonalChatRoomMotion : MonoBehaviour
{
  //public GameObject room;
    
    private void Start()
    {
      //room = GameObject.Find("Content_per");
        transform.DOMoveX(500, 0.1f);
    }
    private void Update()
    {

        int currentOrder = transform.GetSiblingIndex();

        if (currentOrder == transform.parent.childCount-1)
        {
            ShowMotion();
        }
        else
        {
            HideMotion();
        }
    }
    private void ShowMotion()
    {
        transform.DOMoveX(0, 0.1f);
    }
    private void HideMotion()
    {
        transform.DOMoveX(500, 0.1f);
    }
}
