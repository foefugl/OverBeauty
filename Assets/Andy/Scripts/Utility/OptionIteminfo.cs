using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;


public class OptionIteminfo : MonoBehaviour, IPointerClickHandler
{
    public Text label;

    public void OnPointerClick(PointerEventData eventData)
    {
        PassWordNotifyPanel.Instance.InputTxtChanged(label.text);
    }
}
