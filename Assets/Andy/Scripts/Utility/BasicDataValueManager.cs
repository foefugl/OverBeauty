using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Yarn.Unity;

[System.Serializable]
public class DOTUnit
{
    public float Damage;
    public float Duration;
    public DOTUnit(float _damage, float _duration)
    {
        Damage = _damage;
        Duration = _duration;
    }
}

public class BasicDataValueManager : MonoBehaviour
{
    private static BasicDataValueManager instance;
    public static BasicDataValueManager Instance { get { return instance; } }
    [SerializeField] Text CredibilityTxt;
    [SerializeField] Text ManhuntTxt;
    [SerializeField] Text MoneyTxt;
    [SerializeField] InMemoryVariableStorage memoryStorage;
    [SerializeField] BasicValueNotifyPanel valueNotifyPanel;
    [SerializeField] SCO_ValueConfigure valueConfigure;

    private bool valueChange;
    private Dictionary<string, Action<float>> ClubFansDic;
    private Dictionary<string, List<DOTUnit>> DOTsDic;
    private float credibility, manHunt, money;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        ClubFansDic = new Dictionary<string, Action<float>>();
    }
    private void Start()
    {
        InitialDOTDic();

        CredibilityTxt.text = memoryStorage.GetValue("$credibility").AsNumber.ToString();
        ManhuntTxt.text = memoryStorage.GetValue("$manhunt").ToString();
        MoneyTxt.text = memoryStorage.GetValue("$money").ToString();
    }

    public void RegistClub(string clubName, Action<float> action)
    {
        ClubFansDic.Add(clubName, action);
        List<DOTUnit> fansDOTsList = new List<DOTUnit>();
        DOTsDic.Add(clubName, fansDOTsList);
    }

    // Update is called once per frame
    void Update()
    {
        float credibilityValue = memoryStorage.GetValue("$credibility").AsNumber;
        if (credibility != credibilityValue)
        {
            credibility = credibilityValue;
            valueChange = true;
        }
        float manValue = memoryStorage.GetValue("$manhunt").AsNumber;
        if (manHunt != manValue)
        {
            manHunt = manValue;
            valueChange = true;
        }
        float moneyValue = (int)memoryStorage.GetValue("$money").AsNumber;
        if (money != moneyValue)
        {
            valueChange = true;
            money = moneyValue;
        }

        if (valueChange)
        {
            valueChange = false;
            if (valueNotifyPanel != null)
            {
                valueNotifyPanel.ValueChangeNotify(credibility, manHunt, money);
            }
            
            if (manHunt >= 100)
            {
                valueNotifyPanel.Dead();
                GamePlayTime.Instance.TimeFreeze = true;
                ResetDOT_ExceptClub();
            }

            CredibilityTxt.text = credibility.ToString();
            ManhuntTxt.text = manHunt.ToString();
            MoneyTxt.text = money.ToString();
        }
    }
    //public void SetValue(SCO_CardValue cardValue)
    //{
    //    memoryStorage.SetValue("$credibility", credibility + cardValue.Credibility);
    //    memoryStorage.SetValue("$manhunt", manHunt + cardValue.ManHunt);
    //    memoryStorage.SetValue("$money", money + cardValue.money);
    //}

    public void DOTSetValue()
    {
        float[] valueAffected = { credibility, manHunt, money };
        for (int i = 0; i < DOTsDic.Count; i++)
        {
            var keyPair = DOTsDic.ElementAtOrDefault(i);
            List<DOTUnit> toRemove = new List<DOTUnit>();
            for (int j = 0; j < keyPair.Value.Count; j++)
            {
                double _damage = System.Math.Round(keyPair.Value[j].Damage / 24, 2, System.MidpointRounding.AwayFromZero);

                if (ClubFansDic.ContainsKey(keyPair.Key))
                {
                    ClubFansDic[keyPair.Key]?.Invoke((float)_damage * valueConfigure.FansDropMultiplier);
                }
                else
                {
                    memoryStorage.SetValue(keyPair.Key, valueAffected[i] + (float)_damage);
                }
                
                keyPair.Value[j].Duration -= (1/24);
                if (keyPair.Value[j].Duration <= 0)
                    toRemove.Add(keyPair.Value[j]);
            }

            // Remove duration 0 unit
            for (int j = 0; j < toRemove.Count; j++)
            {
                keyPair.Value.Remove(toRemove[j]);
            }
        }
    }
    private void InitialDOTDic()
    {
        DOTsDic = new Dictionary<string, List<DOTUnit>>();
        List<DOTUnit> credibilityDOTsList = new List<DOTUnit>();
        DOTsDic.Add("$credibility", credibilityDOTsList);

        List<DOTUnit> humanHuntDOTsList = new List<DOTUnit>();
        DOTsDic.Add("$manhunt", humanHuntDOTsList);

        List<DOTUnit> moneyDOTsList = new List<DOTUnit>();
        DOTsDic.Add("$money", moneyDOTsList);
    }
    private void ResetDOT_ExceptClub()
    {
        for (int i = 0; i < DOTsDic.Count; i++)
        {
            string key = DOTsDic.ElementAtOrDefault(i).Key;
            if (key == "$manhunt" ||
                key == "$credibility" ||
                key == "$money")
            {
                DOTsDic[key] = new List<DOTUnit>();
            }
        }
    }

    [YarnCommand("AddDOTUnitToList")]
    public void AddDOTUnitToList(string _key, string _value, string _duration)
    {
        float value = float.Parse(_value);
        float duration = float.Parse(_duration);

        if (_key != "$credibility" && _key != "$manhunt" && _key != "$money")
        {
            valueNotifyPanel.SetWarning((value < 0));
        }
        
        DOTUnit dOTUnit = new DOTUnit(value, duration);
        DOTsDic[_key].Add(dOTUnit);
    }
}
