using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utility;


public class CommunityPostManager : MonoBehaviour
{
    public List<ScriptableObject> postsArray;
    public PostUIInstance PostPrefab;
    public Transform ScrollTransform;
    public SCO_NotifyPost SoftwareDescrs;

    public MissionData CurrentMData;
    public KeywordData CurrentKeywordData;

    [Header("增加貼文")]
    [HideInInspector]
    public bool Add;
    [HideInInspector]
    public SCO_Post targetPost;

    [Header("新增/更新內文")]
    [HideInInspector]
    public bool UpdateContent;
    [HideInInspector]
    public string targetKey;

    protected Dictionary<string, PostUIInstance> postsDic;

    private void Start()
    {
        if (MissionManager.Instance != null)
            MissionManager.Instance.RegistManager(SoftwareDescrs.AppType.ToString(), this);

        postsDic = new Dictionary<string, PostUIInstance>();
        Initial();
    }
    protected virtual void Initial()
    {
        // Initial default posts
        for (int i = 0; i < postsArray.Count; i++)
        {
            AddPost(postsArray[i]);
        }
    }


    protected virtual void Update()
    {
        if (Add)
        {
            Add = false;
            AddPost(targetPost);
        }
        if (UpdateContent)
        {
            UpdateContent = !UpdateContent;
            UpdatePostContent(targetKey);
        }
    }
    
    public virtual void UpdatePostContent(string _key)
    {
        if (!postsDic.ContainsKey(_key))
        {
            Debug.LogError("Try to update undefined message.");
            return;
        }
        else
        {
            postsDic[_key].UpdateContentPerStep();
        }
    }
    public SCO_Post FindPostData(string _key)
    {
        if (postsDic.ContainsKey(_key))
        {
            return postsDic[_key].CurrentPostData;
        }
        else
        {
            return null;
        }
    }

    public virtual void UpdatePostContent(SCO_Post _targetPost)
    {
        if (!postsDic.ContainsKey(_targetPost.Key))
        {
            Debug.LogError("Try to update undefined message.");
            return;
        }
        else
        {
            PostUIInstance post =  postsDic[_targetPost.Key];
            post.InitialDatas(_targetPost);
        }
    }
    public virtual GameObject AddPost(ScriptableObject _targetPost)
    {
        SCO_Post scoPost = (SCO_Post)_targetPost;
        if (postsDic.ContainsKey(scoPost.Key))
        {
            UpdatePostContent(scoPost);
            return postsDic[scoPost.Key].gameObject;
        }
        else
        {
            PostUIInstance post = Instantiate(PostPrefab, ScrollTransform);

            postsDic.Add(scoPost.Key, post);
            post.transform.SetAsFirstSibling();
            post.InitialDatas(scoPost);

            if (scoPost.PostSetting.missionData != null)
            {
                if (scoPost.PostSetting.missionData.ActiveKey != "")
                {
                    var chopdetect = post.gameObject.AddComponent<CueChopDetector>();
                    chopdetect.data = new MissionData(scoPost.PostSetting.missionData.ActiveKey, scoPost.PostSetting.missionData.DutyIndex, scoPost.PostSetting.missionData.CueContent);
                }
            }
            if (scoPost.PostSetting.keywordData != null)
            {
                StartCoroutine(DelayOneFrameToMarkKeyword(post, scoPost.PostSetting.keywordData));
            }

            SendNotifyPost(scoPost.PostSetting);
            return postsDic[scoPost.Key].gameObject;
        }
    }
    public virtual void RemovePost(ScriptableObject _targetPost)
    {
        if (_targetPost == null)
        {
            return;
        }

        SCO_Post scoPost = (SCO_Post)_targetPost;
        if (postsDic.ContainsKey(scoPost.Key))
        {
            Destroy(postsDic[scoPost.Key].gameObject);
            postsDic.Remove(scoPost.Key);
        }
    }
    public void RemoveAllPost()
    {
        for (int i = 0; i < postsDic.Count; i++)
        {
            Destroy(postsDic.ElementAtOrDefault(i).Value.gameObject);
        }
        postsDic.Clear();
    }
    public virtual void SendNotifyPost(Post _targetPost)
    {
        SCO_NotifyPost sCO_NotifyPost = (SCO_NotifyPost)ScriptableObject.CreateInstance("SCO_NotifyPost");
        sCO_NotifyPost.SetData(SoftwareDescrs);
        sCO_NotifyPost.AppType = SoftwareDescrs.AppType;
        sCO_NotifyPost.PostSetting.UserName = _targetPost.UserName;

        if (_targetPost.Content != null)
        {
            if (_targetPost.Content.Count != 0)
            {
                sCO_NotifyPost.PostSetting.Content = new List<string>() { _targetPost.Content[0] };
            }
        }

        sCO_NotifyPost.PostSetting.Avatar = SoftwareDescrs.PostSetting.Avatar;
        NotifyPost notify = new NotifyPost(sCO_NotifyPost);
        if (NotifyManager.Instance != this)
        {
            NotifyManager.Instance.AddNotification(notify);
        }
    }
    IEnumerator DelayOneFrameToMarkKeyword(PostUIInstance postUIInstance, KeywordData _keyData)
    {
        yield return new WaitForSeconds(0.125f);
        var cueDetector = postUIInstance.PostContent.gameObject.AddComponent<KeywordDataDetector>();
        cueDetector.data = _keyData;
    }
}
