using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DataTimeTemp : MonoBehaviour
{
    public Text Date;
    public Text Time;

    // Update is called once per frame
    void Update()
    {
        string timeContent = System.DateTime.Now.ToString();
        string[] info = timeContent.Split(' ');
        Date.text = info[0];
        Time.text = info[2];
    }
}
