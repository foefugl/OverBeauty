using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using Yarn.Unity;

public class PassWordNotifyPanel : MonoBehaviour
{
    private static PassWordNotifyPanel instance;
    public static PassWordNotifyPanel Instance { get { return instance; } }
    public GameObject m_panel;
    public InputField InputField;
    public Dropdown DropDown;

    private PointerDetector pointerDetector;
    private WinkClubUIInstance currentClub;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        m_panel.SetActive(false);
    }
    private void Start()
    {
        pointerDetector = FindObjectOfType<PointerDetector>();
        pointerDetector.HoveredChanged.AddListener(CheckCurrentHoverd);
    }
    [YarnCommand("RegistEventInCue")]
    public void RegistEventInCue(string _activeKey, string _txt)
    {
        CueChopsObserver.Instance.RegistCueClickEvent(_activeKey, () => RegistNewPassWord(_txt));
    }
    public void RegistNewPassWord(string _txt)
    {
        bool readyAdded = false;
        for (int i = 0; i < DropDown.options.Count; i++)
        {
            if (DropDown.options[i].text == _txt)
            {
                readyAdded = true;
                break;
            }
        }
        if (readyAdded)
        {
            Debug.LogError("Pass word ready added.");
            return;
        }

        List<Dropdown.OptionData> new_options = new List<Dropdown.OptionData>();

        for (int i = 0; i < DropDown.options.Count; i++){
            new_options.Add(DropDown.options[i]);
        }

        Dropdown.OptionData newData = new Dropdown.OptionData(_txt);
        new_options.Add(newData);
        DropDown.ClearOptions();

        DropDown.options = new_options;
    }
    public void ConfirmPassword()
    {
        // Enter Room
        if (InputField.text == currentClub.ClubPassWord)
        {
            m_panel.SetActive(false);
            currentClub.enterRoomEvent?.Invoke();
        }
        // Block
        else
        {
            Vector3 getter = m_panel.transform.position;
            DOTween.Shake(() => getter, x => getter = x, 0.25f, 10, 30, 90, true, true)
                .OnUpdate(() => 
                {
                    m_panel.transform.position = getter;
                });
        }
    }
    public void InputTxtChanged(string _value)
    {
        InputField.text = _value;
    }

    public void Show(WinkClubUIInstance _club)
    {
        m_panel.SetActive(true);
        currentClub = _club;
    }
    private void CheckCurrentHoverd()
    {
        if (pointerDetector.CurrenHovered == null)
            m_panel.SetActive(false);
        else if (pointerDetector.CurrenHovered.transform.root.name != transform.root.name)
            m_panel.SetActive(false);
    }
}
