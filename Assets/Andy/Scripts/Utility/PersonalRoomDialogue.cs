using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Utility;
using Yarn.Unity;

public class PersonalRoomDialogue : MonoBehaviour, IDialogueInteract
{
    public Transform messagerPivot;
    public SCO_MessageRoom roomData;
    public Post LatestPost;
    public YarnProgram targetMap;

    protected DialogPassCatchRunner passCatchRunner;

    protected virtual void Start()
    {
        passCatchRunner = GetComponent<DialogPassCatchRunner>();
        passCatchRunner.BluePrints = roomData.bluePrints;
        LatestPost = roomData.members[0].PostSetting;
    }

    public virtual void AddResponse(Post post)
    {
        throw new System.NotImplementedException();
    }
}
