using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;
using Utility;

public class BasicValueNotifyPanel : MonoBehaviour
{
    public SCO_NotifyPost SoftwareDescrs;
    public GameObject ValueChangePanel;
    public GameObject WarningPanel;
    public GameObject DeadPanel;
    public TextMeshProUGUI warningTxt;
    
    private Queue<Action> uiMotions;

    [System.Serializable]
    public class ValueArrow
    {
        public float value;
        public Text Txt;
        public Image Icon;
        public Image Up;
        public Image Down;
        public void SetData()
        {
            Txt.text = value.ToString();
        }
    }

    public ValueArrow[] valueArrowPair;

    private void Start()
    {
        uiMotions = new Queue<Action>();
        //StartCoroutine(MotionQueueRunning(2));
    }

    public void SetWarning(bool decline)
    {
        Post post = new Post();
        post.UserName = "紅粉數目變化";
        if (decline)
            post.Content = new List<string>() { "紅粉開始 <color=red><b>下降</b></color>" };
        else
            post.Content = new List<string>() { "紅粉開始 <color=red><b>上升</b></color>" };

        SendNotifyPost(post);
        //uiMotions.Enqueue(() => WarningUIMotion(decline));
    }
    private void WarningUIMotion(bool decline)
    {
        WarningPanel.SetActive(true);
        WarningPanel.transform.localScale = Vector3.zero;
        WarningPanel.transform.DOScale(Vector3.one, 0.3f)
        .SetEase(Ease.OutBack)
        .OnComplete(() =>
        {
            WarningPanel.transform.DOScale(Vector3.zero, 0.3f)
            .SetEase(Ease.InBack)
            .SetDelay(1);
        });

        if (decline)
        {
            warningTxt.text = "紅粉開始 <color=red><b>下降</b></color>";
        }
        else
        {
            warningTxt.text = "紅粉開始 <color=red><b>上升</b></color>";
        }
    }
    public void Dead()
    {
        DeadPanel.transform.parent.gameObject.SetActive(true);
        DeadPanel.transform.localScale = Vector3.zero;
        DeadPanel.transform.DOScale(Vector3.one, 0.3f)
        .SetEase(Ease.OutBack);
    }
    public void CloseDeadPanel()
    {
        DeadPanel.transform.localScale = Vector3.zero;
    }

    public void ValueChangeNotify(float credibility, float manHunt, float money)
    {
        if (valueArrowPair[0].value != credibility)
        {
            Post creditPost = new Post();
            float creditValueAdjust = Mathf.Abs(credibility - valueArrowPair[0].value);
            if (valueArrowPair[0].value > credibility)
            {
                creditPost.UserName = "失去公信力";
                string message = "減少" + creditValueAdjust.ToString();
                creditPost.Content = new List<string>() { message };

            }
            else if (valueArrowPair[0].value < credibility)
            {
                creditPost.UserName = "獲得公信力";
                string message = "增加" + creditValueAdjust.ToString();
                creditPost.Content = new List<string>() { message };
            }
            SendNotifyPost(creditPost);
            valueArrowPair[0].value = credibility;
            valueArrowPair[0].SetData();
        }

        if (valueArrowPair[1].value == manHunt)
        {
            Post manHuntPost = new Post();
            float manHuntValueAdjust = Mathf.Abs(manHunt - valueArrowPair[1].value);
            if (valueArrowPair[1].value > manHunt)
            {
                manHuntPost.UserName = "肉搜值下降";
                string message = "減少" + manHuntValueAdjust.ToString();
                manHuntPost.Content = new List<string>() { message };

            }
            else if (valueArrowPair[1].value < manHunt)
            {
                manHuntPost.UserName = "肉搜值上升";
                string message = "增加" + manHuntValueAdjust.ToString();
                manHuntPost.Content = new List<string>() { message };
            }
            SendNotifyPost(manHuntPost);
            valueArrowPair[1].value = manHunt;
            valueArrowPair[1].SetData();
        }


        if (valueArrowPair[2].value == money)
        {
            Post moneyPost = new Post();
            float moneyValueAdjust = Mathf.Abs(money - valueArrowPair[2].value);
            if (valueArrowPair[2].value > money)
            {
                moneyPost.UserName = "花費蜜罐";
                string message = "花費" + moneyValueAdjust.ToString();
                moneyPost.Content = new List<string>() { message };

            }
            else if (valueArrowPair[2].value < money)
            {
                moneyPost.UserName = "賺取蜜罐";
                string message = "增加" + moneyValueAdjust.ToString();
                moneyPost.Content = new List<string>() { message };
            }
            SendNotifyPost(moneyPost);
            valueArrowPair[2].value = money;
            valueArrowPair[2].SetData();
        }
    }
    protected virtual void SendNotifyPost(Post _targetPost)
    {
        SCO_NotifyPost sCO_NotifyPost = (SCO_NotifyPost)ScriptableObject.CreateInstance("SCO_NotifyPost");
        sCO_NotifyPost.SetData(SoftwareDescrs);
        sCO_NotifyPost.AppType = SoftwareDescrs.AppType;
        sCO_NotifyPost.PostSetting.UserName = _targetPost.UserName;

        if (_targetPost.Content != null)
        {
            if (_targetPost.Content.Count != 0)
            {
                sCO_NotifyPost.PostSetting.Content = new List<string>() { _targetPost.Content[0] };
            }
        }

        sCO_NotifyPost.PostSetting.Avatar = SoftwareDescrs.PostSetting.Avatar;
        NotifyPost notify = new NotifyPost(sCO_NotifyPost);
        if (NotifyManager.Instance != this)
        {
            NotifyManager.Instance.AddNotification(notify);
        }
    }
    private void ValueChangeUIMotion(float credibility, float manHunt, float money)
    {
        ValueChangePanel.SetActive(true);
        ValueChangePanel.transform.localScale = Vector3.zero;
        ValueChangePanel.transform.DOScale(Vector3.one, 0.3f)
        .SetEase(Ease.OutBack)
        .OnComplete(() =>
        {
            ValueChangePanel.transform.DOScale(Vector3.zero, 0.3f)
            .SetEase(Ease.InBack)
            .SetDelay(1);
        });


        if (valueArrowPair[0].value > credibility)
        {
            valueArrowPair[0].Down.gameObject.SetActive(true);
            valueArrowPair[0].Up.gameObject.SetActive(false);
        }
        else if (valueArrowPair[0].value == credibility)
        {
            valueArrowPair[0].Down.gameObject.SetActive(false);
            valueArrowPair[0].Up.gameObject.SetActive(false);
        }
        else
        {
            valueArrowPair[0].Down.gameObject.SetActive(false);
            valueArrowPair[0].Up.gameObject.SetActive(true);
        }
        valueArrowPair[0].value = credibility;
        valueArrowPair[0].SetData();

        //-----------------------------------------------------------------

        if (valueArrowPair[1].value > manHunt)
        {
            valueArrowPair[1].Down.gameObject.SetActive(true);
            valueArrowPair[1].Up.gameObject.SetActive(false);
        }
        else if (valueArrowPair[1].value == manHunt)
        {
            valueArrowPair[1].Down.gameObject.SetActive(false);
            valueArrowPair[1].Up.gameObject.SetActive(false);
        }
        else
        {
            valueArrowPair[1].Down.gameObject.SetActive(false);
            valueArrowPair[1].Up.gameObject.SetActive(true);
        }
        valueArrowPair[1].value = manHunt;
        valueArrowPair[1].SetData();

        //-----------------------------------------------------------------

        if (valueArrowPair[2].value > money)
        {
            valueArrowPair[2].Down.gameObject.SetActive(true);
            valueArrowPair[2].Up.gameObject.SetActive(false);
        }
        else if (valueArrowPair[2].value == money)
        {
            valueArrowPair[2].Down.gameObject.SetActive(false);
            valueArrowPair[2].Up.gameObject.SetActive(false);
        }
        else
        {
            valueArrowPair[2].Down.gameObject.SetActive(false);
            valueArrowPair[2].Up.gameObject.SetActive(true);
        }
        valueArrowPair[2].value = money;
        valueArrowPair[2].SetData();
    }
    IEnumerator MotionQueueRunning(float delay)
    {
        while (true)
        {
            if (uiMotions.Count > 0)
            {
                var action = uiMotions.Dequeue();
                action?.Invoke();
                yield return new WaitForSeconds(delay);
            }
            yield return null;
        }
    }
}
