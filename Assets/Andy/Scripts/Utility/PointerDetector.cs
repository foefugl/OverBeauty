using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class PointerDetector : StandaloneInputModule
{

    [Header("�ۻs�\��")]
    public UnityEvent HoveredChanged;
    public GameObject CurrenHovered;

    private void Update()
    {
        if (GetCurrentFocusedGameObject() == null)
            return;

        if (Input.GetMouseButtonDown(0))
        {
            CurrenHovered = EventSystem.current.currentSelectedGameObject;
            HoveredChanged?.Invoke();
        }
    }
}
