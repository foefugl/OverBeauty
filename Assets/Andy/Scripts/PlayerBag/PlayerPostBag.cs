using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Yarn.Unity;
using Utility;

public class PlayerPostBag : CommunityPostManager
{
    [Space(10)]
    [Header("[ PlayerPostBag ]")]
    public PostUIInstance previewPost;
    public Transform PhotosPanel;
    public WinkPostsManager WinkMainPostManager;
    [SerializeField] Button PublishBtn;

    //[Header("[ Settle ]")]
    private InMemoryVariableStorage memoryVariableStorage;

    private List<SCO_Post> publishedPosts;
    private Button button;
    private Image image;
    private SCO_Post ReadyPostData;
    private bool opening;

    protected override void Initial()
    {
        base.Initial();

        for (int i = 0; i < postsDic.Count; i++)
        {
            var _data = postsDic.Values.ElementAtOrDefault(i);
            var bagGrid = (BagGridUIInstance)_data;
            bagGrid.WritePostEvent.AddListener(WritePost);
        }

        button = GetComponent<Button>();
        image = GetComponent<Image>();
        publishedPosts = new List<SCO_Post>();
        memoryVariableStorage = FindObjectOfType<InMemoryVariableStorage>();
    }
    public override void SendNotifyPost(Post _targetPost)
    {
        // PlayerPostBag don't send notification
        // base.SendNotifyPost(_targetPost);
    }
    public void ONOFFSwitchPanel()
    {
        opening = !opening;
        button.interactable = opening;
        image.raycastTarget = opening;
        PhotosPanel.gameObject.SetActive(opening);
    }
    public void ONOFFSwitchPanel(bool target)
    {
        opening = target;
        button.interactable = opening;
        image.raycastTarget = opening;
        PhotosPanel.gameObject.SetActive(opening);
    }
    public void WritePost(SCO_Post _targetPost)
    {
        ReadyPostData = _targetPost;
        previewPost.CurrentPostData = _targetPost;
        previewPost.InitialDatas(_targetPost);
        previewPost.GetComponentInChildren<PhotoAdjustWithAsset>();

        PublishBtn.interactable = true;
    }
    public void Publish()
    {
        if (ReadyPostData == null)
        {
            Debug.LogError("ReadyPostData can't be empty.");
            return;
        }

        publishedPosts.Add(ReadyPostData);
        WinkMainPostManager.AddPost(ReadyPostData);
        //previewPost.transform.parent.gameObject.SetActive(false);
        //ONOFFSwitchPanel();
        ReadyPostData = null;
        PublishBtn.interactable = false;

        CalculateSettleValue();
    }

    //公信力10 = 6 - 11
    //公信力30 = 7 - 12
    //公信力50 = 10 - 15
    //公信力80 = 13 - 18
    private void CalculateSettleValue()
    {
        float credibility = memoryVariableStorage.GetValue("$credibility").AsNumber;
        float result = 0;

        if (credibility <= 30)
        {
            result = Random.Range(6, 12);
        }
        else if (credibility > 30 && credibility <= 50)
        {
            result = Random.Range(7, 13);
        }
        else if (credibility > 50 && credibility <= 80)
        {
            result = Random.Range(10, 16);
        }
        else
        {
            result = Random.Range(13, 19);
        }

        float manhuntValue = memoryVariableStorage.GetValue("$manhunt").AsNumber;
        memoryVariableStorage.SetValue("$manhunt", manhuntValue + 30);

        if (result >= 13)
        {
            BasicDataValueManager.Instance.AddDOTUnitToList("仙仙騎士團", 5.ToString(), 14.ToString());
        }
    }
    public void DeletePost()
    {
        if (publishedPosts.Count == 0)
        {
            return;
        }

        for (int i = 0; i < publishedPosts.Count; i++)
        {
            WinkMainPostManager.RemovePost(publishedPosts[i]);
        }
        
    }
}
