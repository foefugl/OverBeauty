using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;


[System.Serializable]
public class PostUnityEvent : UnityEvent<SCO_Post> { }
public class BagGridUIInstance : PostUIInstance, IPointerClickHandler
{
    [Space(10)]
    [Header("BagGrid")]
    public Image photo;
    public PostUnityEvent WritePostEvent;

    public override void InitialDatas(SCO_Post _sCO_Post)
    {
        base.InitialDatas(_sCO_Post);
        photo.sprite = CurrentPostData.PostSetting.Photos[0];
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        WritePostEvent?.Invoke(CurrentPostData);
    }

}
