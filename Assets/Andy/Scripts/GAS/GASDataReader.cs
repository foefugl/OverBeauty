using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class GASDataReader : MonoBehaviour
{
    public string TargetURL;
    public int Page;
    public int Row;
    public int Column;
    public bool test;

    private void Update()
    {
        if (test)
        {
            test = false;
            StartCoroutine(DownloadCertaintGrid());
        }
    }
    public IEnumerator DownloadCertaintGrid()
    {
        WWWForm form = new WWWForm();
        form.AddField("page", Page);
        form.AddField("row", Row);
        form.AddField("column", Column);

        // Sending the request to API url with form object.
        using (UnityWebRequest www = UnityWebRequest.Post(TargetURL, form))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                // Done and get the response text.
                print(www.downloadHandler.text);
                Debug.Log("Form upload complete!");
            }
        }
    }

    IEnumerator DownloadWholePage()
    {
        WWWForm form = new WWWForm();
        form.AddField("method", "read");

        // Sending the request to API url with form object.
        using (UnityWebRequest www = UnityWebRequest.Post(TargetURL, form))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                // Done and get the response text.
                print(www.downloadHandler.text);
                Debug.Log("Form upload complete!");
            }
        }
    }
}
