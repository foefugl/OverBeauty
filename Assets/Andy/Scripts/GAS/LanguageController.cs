using UnityEngine;
using System.Collections;
using UnityEditor;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using Yarn.Unity;

public enum LanguageType
{
    Chinese,
    English
}

[ExecuteAlways]

public class LanguageController : MonoBehaviour
{
    private static LanguageController instance;
    public static LanguageController Instance 
    {
        get 
        {
            return instance;
        }
    }
    [SerializeField] LanguageType targetType;
    public bool UpdateContent;

    public string YarnLanguage
    {
        get
        {
            switch (targetType)
            {
                case LanguageType.Chinese:
                    return "zh-TW";
                case LanguageType.English:
                    return "en-US";
                default:
                    return "";
            }
        }
    }

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    private void OnValidate()
    {
        if (UpdateContent)
        {
            UpdateContent = false;
            DialogueContent();
            PostContentUpdate();
        }   
    }
    private void DialogueContent()
    {
        string[] guids = AssetDatabase.FindAssets("t:" + typeof(YarnProgram).Name);
        for (int i = 0; i < guids.Length; i++)
        {
            string path = AssetDatabase.GUIDToAssetPath(guids[i]);
            YarnProgram yarn = AssetDatabase.LoadAssetAtPath<YarnProgram>(path);
            StartCoroutine(yarn.DownloadContent());
        }
    }

    private void PostContentUpdate()
    {
        string[] guids = AssetDatabase.FindAssets("t:" + typeof(SCO_WinkPost).Name);
        for (int i = 0; i < guids.Length; i++)
        {
            string path = AssetDatabase.GUIDToAssetPath(guids[i]);

            SCO_WinkPost post = AssetDatabase.LoadAssetAtPath<SCO_WinkPost>(path);
            StartCoroutine(post.DownloadContent(targetType));
        }
    }
}
