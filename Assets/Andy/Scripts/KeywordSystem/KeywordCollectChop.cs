using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class KeywordCollectChop : MonoBehaviour, IPointerClickHandler
{
    public KeywordDataDetector belongDetector;
    public KeywordData data;
    public void OnPointerClick(PointerEventData eventData)
    {
        belongDetector.MarkKeyword(true);
        KeywordManager.Instance.Collect(data);
    }
}
