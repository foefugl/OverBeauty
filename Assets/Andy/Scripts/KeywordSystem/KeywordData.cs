using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(fileName = "KeywordData", menuName = "SCO/KeywordData", order = 9)]
public class KeywordData : ScriptableObject
{
    public string ActiveKey;
    public string KeywordTxt;
    public string Comment;
    public string Address;
    public Sprite photo;
    public UnityEvent KeywordClickEvent;
    public MissionData missionData;
}
