using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utility;

public class KeywordManager : MonoBehaviour
{
    private static KeywordManager instance;
    public static KeywordManager Instance 
    { 
        get 
        {
            if (instance == null)
            {
                instance = FindObjectOfType<KeywordManager>();
                if (instance == null)
                    instance = new GameObject("KeywordManager").AddComponent<KeywordManager>();

                instance.Initial();
                DontDestroyOnLoad(instance.gameObject);
            }

            return instance; 
        } 
    }

    private Dictionary<string, KeywordData> keywordsDic;
    public Dictionary<string, KeywordData> KeywordsDic 
    {
        get
        {
            return keywordsDic;
        }
    }

    public SCO_NotifyPost SoftwareDescrs;
    [Space(10)]
    [Header("[ Add Manual ]")]
    public bool AddManual;
    public KeywordData targetKeyword;
    private bool hasInitial = false;

    private void Start()
    {
        Initial();
    }
    private void Update()
    {
        if (AddManual)
        {
            AddManual = false;
            Collect(targetKeyword);
        }
    }

    public void Initial()
    {
        if (!hasInitial)
        {
            keywordsDic = new Dictionary<string, KeywordData>();
            hasInitial = true;
        }
    }

    public void Collect(KeywordData data)
    {
        if (keywordsDic.ContainsKey(data.ActiveKey))
            return;

        Debug.Log("Add keyword: " + data.ActiveKey);
        Post post = new Post();
        post.UserName = "��o����r";
        post.Content = new List<string>() { data.KeywordTxt };
        SendNotifyPost(post);
        keywordsDic.Add(data.ActiveKey, data);
    }

    protected virtual void SendNotifyPost(Post _targetPost)
    {
        SCO_NotifyPost sCO_NotifyPost = (SCO_NotifyPost)ScriptableObject.CreateInstance("SCO_NotifyPost");
        sCO_NotifyPost.SetData(SoftwareDescrs);
        sCO_NotifyPost.AppType = SoftwareDescrs.AppType;
        sCO_NotifyPost.PostSetting.UserName = _targetPost.UserName;

        if (_targetPost.Content != null)
        {
            if (_targetPost.Content.Count != 0)
            {
                sCO_NotifyPost.PostSetting.Content = new List<string>() { _targetPost.Content[0] };
            }
        }

        sCO_NotifyPost.PostSetting.Avatar = SoftwareDescrs.PostSetting.Avatar;
        NotifyPost notify = new NotifyPost(sCO_NotifyPost);
        if (NotifyManager.Instance != this)
        {
            NotifyManager.Instance.AddNotification(notify);
        }
    }
}
