using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class KeywordDataDetector : MonoBehaviour
{
    public KeywordData data;
    public TextMeshProUGUI text;

    // Start is called before the first frame update
    void Start()
    {
        text = gameObject.GetComponent<TextMeshProUGUI>();
        ScanTxt();
    }

    public void ScanTxt()
    {
        if (data == null || data.KeywordTxt == "" || data.ActiveKey == "" || text == null)
            return;

        if (text.text.Contains(data.KeywordTxt))
        {
            Debug.LogError("Contains");
            int index = text.text.IndexOf(data.KeywordTxt);

            text.ForceMeshUpdate();

            Vector3 pos = text.textInfo.characterInfo[index].bottomLeft;
            TextMeshProUGUI keywordTex = Instantiate(text, text.transform);
            keywordTex.rectTransform.pivot = Vector2.zero;
            keywordTex.text = data.KeywordTxt;
            keywordTex.color = new Color(keywordTex.color.r, keywordTex.color.g, keywordTex.color.b, 0);
            
            var components = keywordTex.GetComponents<Component>();
            for (int i = 0; i < components.Length; i++)
            {
                if (components[i].GetType() != text.GetType())
                {
                    Destroy(components[i]);
                }
                
            }
            //Destroy(keywordTex.GetComponent<KeywordDataDetector>());
            var chop = keywordTex.gameObject.AddComponent<KeywordCollectChop>();
            chop.belongDetector = this;
            chop.data = data;
            keywordTex.transform.localPosition = pos + Vector3.down * 20;

            keywordTex.ForceMeshUpdate();
            float xSize = (keywordTex.GetRenderedValues().x < 0) ? keywordTex.preferredWidth : keywordTex.GetRenderedValues().x;
            keywordTex.rectTransform.sizeDelta = new Vector2(xSize, keywordTex.preferredHeight);

            StartCoroutine(Delay());
            //MarkKeyword();
        }
    }
    public void MarkKeyword(bool collected = false)
    {
        if (text.text.Contains("<mark=ffff00aa>"))
        {
            string resetString = text.text.Replace("<mark=ffff00aa>", "");
            resetString.Replace("</mark>","");
            text.text = resetString;
        }

        string markColorSyn = (collected == true)? "<mark=#00000020>" : "<mark=ffff00aa>";
        string content = text.text;

        int lastIndex = content.IndexOf(data.KeywordTxt.Last());
        int firstIndex = content.IndexOf(data.KeywordTxt.First());

        if (lastIndex == text.text.Length - 1)
            content += "</mark>";
        else
            content = content.Insert(lastIndex + 1, "</mark>");

        content = content.Insert(firstIndex, markColorSyn);

        text.text = content;

        if (data.missionData != null && collected)
        {
            if (data.missionData.ActiveKey != "")
            {
                MissionManager.Instance.MissionCompleted(data.missionData.ActiveKey, data.missionData.DutyIndex);
            }
        }
    }
    IEnumerator Delay()
    {
        yield return new WaitForSeconds(0.2f);
        MarkKeyword();
    }
}
