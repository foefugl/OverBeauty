using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using DG.Tweening;

public enum AppPanelName
{
    WinkHome,
    WinkCommunity,
    WinkPost,
    WinkMessager,
    WinkPlayerInformation,

    ProbearIntro,
    ProbearHome,

    MoodyHome,

    AIHome,

    NoteHome,

    PhoneHome,

    Default
}

[System.Serializable]
public class AppScenePair
{
    public string SceneName;
    public AppPanelName AppType;
}

//[ExecuteAlways]
public class BaseUnderToolBar : MonoBehaviour
{
    private static BaseUnderToolBar instance;
    public static BaseUnderToolBar Instance
    {
        get
        {
            return instance;
        }
    }
    private AppPanelName currentApp = AppPanelName.PhoneHome;
    public AppPanelName CurrentApp
    {
        get
        {
            return currentApp;
        }
        set
        {
            if (currentApp != value)
            {
                AppChangeEvent?.Invoke(value);
            }
            currentApp = value;
        }
    }
    [SerializeField] GameObject UIElementsPivot;
    [SerializeField] Button HomeBtn;
    [SerializeField] Button ReturnBtn;
    [SerializeField] AppScenePair[] AppsScene;

    public Transform popoutPos;
    public Transform popinPos;
    public bool TestBtn;

    public UnityEvent<AppPanelName> AppChangeEvent;

    private SceneCavasControl[] canvasControls;
    private Dictionary<AppPanelName, string> sceneDataDic;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }

    }
    private void Start()
    {
        sceneDataDic = new Dictionary<AppPanelName, string>();
        //InitialLoadScenes();
    }
    public void InitialLoadScenes()
    {
        var scenes = SceneManager.GetAllScenes();
        List<Scene> sceneList = scenes.ToList();

        for (int i = 0; i < AppsScene.Length; i++)
        {
            Scene _scene = SceneManager.GetSceneByName(AppsScene[i].SceneName);
            if (!sceneList.Contains(_scene))
            {
                SceneManager.LoadScene(AppsScene[i].SceneName, LoadSceneMode.Additive);
            }
            sceneDataDic.Add(AppsScene[i].AppType, AppsScene[i].SceneName);
        }
        StartCoroutine(DelaySetActiveScene());
    }
    private void Update()
    {
        if (TestBtn)
        {
            TestBtn = false;
            InitialLoadScenes();
        }
    }

    public void BackHome()
    {
        StartCoroutine(TransformToOtherApp(AppPanelName.PhoneHome));
    }

    public void OpenApp(AppPanelName targetAppType)
    {
        StartCoroutine(TransformToOtherApp(targetAppType));
    }
    public void ReturnBtnRegistEvent(UnityAction _action)
    {
        ReturnBtn.onClick.AddListener(_action);
    }
    public void ReturnBtnRemoveEvent(UnityAction _action)
    {
        ReturnBtn.onClick.RemoveListener(_action);
    }

    public void HomeBtnRegistEvent(UnityAction _action)
    {
        HomeBtn.onClick.AddListener(_action);
    }
    public void HomeBtnRemoveEvent(UnityAction _action)
    {
        HomeBtn.onClick.RemoveListener(_action);
    }
    IEnumerator DelaySetActiveScene()
    {
        yield return null;
        var targetScene = SceneManager.GetSceneByName(sceneDataDic[CurrentApp]);
        SceneManager.SetActiveScene(targetScene);
        canvasControls = FindObjectsOfType<SceneCavasControl>();
        StartCoroutine(TransformToOtherApp(CurrentApp));
    }
    IEnumerator TransformToOtherApp(AppPanelName targetAppType)
    {
        if (!FadeManager.Instance.Visible)
            FadeManager.Instance.Run();

        yield return new WaitForSeconds(0.5f);
        SetCanvasOrder(targetAppType);
        yield return new WaitForSeconds(0.5f);
        FadeManager.Instance.Run();
    }
    private void SetCanvasOrder(AppPanelName targetAppType)
    {
        AppPanelName firstEnterType = targetAppType;
        if (targetAppType == AppPanelName.WinkCommunity ||
            targetAppType == AppPanelName.WinkMessager ||
            targetAppType == AppPanelName.WinkHome)
        {
            firstEnterType = AppPanelName.WinkHome;
            for (int i = 0; i < canvasControls.Length; i++)
            {
                Scene _scene = SceneManager.GetSceneByName(sceneDataDic[firstEnterType]);
                if (canvasControls[i].gameObject.scene == _scene)
                {
                    canvasControls[i].SwitchPage(targetAppType);
                }
            }
        }

        // Set every canvas back 
        for (int i = 0; i < sceneDataDic.Count; i++)
        {
            if (sceneDataDic.ElementAtOrDefault(i).Key != firstEnterType)
            {
                Scene _targetScene = SceneManager.GetSceneByName(sceneDataDic[firstEnterType]);
                Debug.LogError(_targetScene.name);
                for (int j = 0; j < canvasControls.Length; j++)
                {
                    if (canvasControls[j].gameObject.scene != _targetScene)
                    {
                        canvasControls[j].SetCanvasOrder(0);
                    }
                    else
                    {
                        canvasControls[j].SetCanvasOrder(1);
                        CurrentApp = canvasControls[j].GetCurrentPage();
                    }

                }
            }
        }
        if (firstEnterType == AppPanelName.PhoneHome)
        {
            PopOutHide();
        }
        else
        {
            PopOutShow();
        }
    }
    public void PopOutShow()
    {
        UIElementsPivot.transform.DOMove(popoutPos.position, 0.25f);
    }
    public void PopOutHide()
    {
        UIElementsPivot.transform.DOMove(popinPos.position, 0.25f);
    }
}
