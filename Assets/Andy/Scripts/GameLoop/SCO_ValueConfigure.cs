using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PostAffectValue
{
    public string Name;
    public string YarnKey;
    public float BottomValue = 0;
    public float Multiplier = 1;
}
[CreateAssetMenu(fileName = "SCO_ValueConfigure", menuName = "Config/ValueConfigure", order = 1)]
public class SCO_ValueConfigure : ScriptableObject
{
    public float Credit;
    public float ManHunt;
    public float Money;
    [Range(0, 10000)]
    public float GameTimeSpeed = 288;
    public float FansDropMultiplier = 1;
    public float RecoverValue = 100;
    public PostAffectValue[] PostAffectValues;
}
