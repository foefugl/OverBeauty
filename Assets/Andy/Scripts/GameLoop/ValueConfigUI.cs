using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Reflection;
using UnityEngine.UI;
using TMPro;

public class ValueConfigUI : MonoBehaviour
{
    public GameObject Panel;
    public GamePlayTime GameTimer;

    //public SCO_ValueConfigure sCO_Value;
    public ValueConfigurator configurator;
    public InputField creditTxt;
    public InputField manhuntTxt;
    public InputField moneyTxt;
    public InputField speedTxt;
    public InputField recoverTxt;
    public InputField FansDropValue;
    public InputField CreditBaseValue;
    public InputField CreditAmpValue;
    public InputField ManHuntBaseValue;
    public InputField ManHuntAmpValue;
    public InputField MoneyBaseValue;
    public InputField MoneyAmpValue;

    private bool Onoff;

    // Start is called before the first frame update
    void Start()
    {
        InitialUIValue();
    }

    private void Update()
    {
        SetValues();
    }

    private void SetValues()
    {
        float _credit = 0;
        if (float.TryParse(creditTxt.text, out _credit))
        {
            configurator.sCO_Values.Credit = Mathf.Clamp(_credit, 0, 10000);
            configurator.SetStorageValue("$credibility", configurator.sCO_Values.Credit);
        }
        else
        {
            creditTxt.text = configurator.sCO_Values.Credit.ToString();
        }

        float _manhunt = 0;
        if (float.TryParse(manhuntTxt.text, out _manhunt))
        {
            configurator.sCO_Values.ManHunt = Mathf.Clamp(_manhunt, 0, 100);
            configurator.SetStorageValue("$manhunt", configurator.sCO_Values.ManHunt);
        }
        else
        {
            manhuntTxt.text = configurator.sCO_Values.ManHunt.ToString();
        }

        float _money = 0;
        if (float.TryParse(moneyTxt.text, out _money))
        {
            configurator.sCO_Values.Money = Mathf.Clamp(_money, 0, int.MaxValue);
            configurator.SetStorageValue("$money", configurator.sCO_Values.Money);
        }
        else
        {
            moneyTxt.text = configurator.sCO_Values.Money.ToString();
        }

        float _speed = 0;
        if (float.TryParse(speedTxt.text, out _speed))
        {
            configurator.sCO_Values.GameTimeSpeed = Mathf.Clamp(_speed, 0, 10000);
        }
        else
        {
            speedTxt.text = configurator.sCO_Values.GameTimeSpeed.ToString();
        }

        float _recover = 0;
        if (float.TryParse(recoverTxt.text, out _recover))
        {
            configurator.sCO_Values.RecoverValue = Mathf.Clamp(_recover, 0, 100);
        }
        else
        {
            recoverTxt.text = configurator.sCO_Values.RecoverValue.ToString();
        }


        float _fansdrop = 0;
        if (float.TryParse(FansDropValue.text, out _fansdrop))
        {
            configurator.sCO_Values.FansDropMultiplier = Mathf.Clamp(_fansdrop, 0, int.MaxValue);
        }
        else
        {
            FansDropValue.text = configurator.sCO_Values.FansDropMultiplier.ToString();
        }

        float _creditbase = 0;
        if (float.TryParse(CreditBaseValue.text, out _creditbase))
        {
            configurator.sCO_Values.PostAffectValues[0].BottomValue = Mathf.Clamp(_creditbase, 0, int.MaxValue);
        }
        else
        {
            CreditBaseValue.text = configurator.sCO_Values.PostAffectValues[0].BottomValue.ToString();
        }

        float _creditamp = 0;
        if (float.TryParse(CreditAmpValue.text, out _creditamp))
        {
            configurator.sCO_Values.PostAffectValues[0].Multiplier = Mathf.Clamp(_creditamp, 0, int.MaxValue);
        }
        else
        {
            CreditAmpValue.text = configurator.sCO_Values.PostAffectValues[0].Multiplier.ToString();
        }

        float _manhuntbase = 0;
        if (float.TryParse(ManHuntBaseValue.text, out _manhuntbase))
        {
            configurator.sCO_Values.PostAffectValues[1].BottomValue = Mathf.Clamp(_manhuntbase, 0, int.MaxValue);
        }
        else
        {
            ManHuntBaseValue.text = configurator.sCO_Values.PostAffectValues[1].BottomValue.ToString();
        }

        float _manhuntamp = 0;
        if (float.TryParse(ManHuntAmpValue.text, out _manhuntamp))
        {
            configurator.sCO_Values.PostAffectValues[1].Multiplier = Mathf.Clamp(_manhuntamp, 0, int.MaxValue);
        }
        else
        {
            ManHuntAmpValue.text = configurator.sCO_Values.PostAffectValues[1].Multiplier.ToString();
        }

        float _moneybase = 0;
        if (float.TryParse(MoneyBaseValue.text, out _moneybase))
        {
            configurator.sCO_Values.PostAffectValues[2].BottomValue = Mathf.Clamp(_moneybase, 0, int.MaxValue);
        }
        else
        {
            MoneyBaseValue.text = configurator.sCO_Values.PostAffectValues[2].BottomValue.ToString();
        }

        float _moneyamp = 0;
        if (float.TryParse(MoneyAmpValue.text, out _moneyamp))
        {
            configurator.sCO_Values.PostAffectValues[2].Multiplier = Mathf.Clamp(_moneyamp, 0, int.MaxValue);
        }
        else
        {
            MoneyAmpValue.text = configurator.sCO_Values.PostAffectValues[2].Multiplier.ToString();
        }
    }

    private void InitialUIValue()
    {
        Debug.LogError("InitialUIValue");

        creditTxt.text = configurator.sCO_Values.Credit.ToString();
        manhuntTxt.text = configurator.sCO_Values.ManHunt.ToString();
        moneyTxt.text = configurator.sCO_Values.Money.ToString();

        speedTxt.text = configurator.sCO_Values.GameTimeSpeed.ToString();
        recoverTxt.text = configurator.sCO_Values.RecoverValue.ToString();

        FansDropValue.text = configurator.sCO_Values.FansDropMultiplier.ToString();

        CreditBaseValue.text = configurator.sCO_Values.PostAffectValues[0].BottomValue.ToString();
        CreditAmpValue.text = configurator.sCO_Values.PostAffectValues[0].Multiplier.ToString();

        ManHuntBaseValue.text = configurator.sCO_Values.PostAffectValues[1].BottomValue.ToString();
        ManHuntAmpValue.text = configurator.sCO_Values.PostAffectValues[1].Multiplier.ToString();

        MoneyBaseValue.text = configurator.sCO_Values.PostAffectValues[2].BottomValue.ToString();
        MoneyAmpValue.text = configurator.sCO_Values.PostAffectValues[2].Multiplier.ToString();
    }

    public void SwitchONOFF()
    {
        Onoff = !Onoff;
        GameTimer.TimeFreeze = Onoff;
        Panel.SetActive(Onoff);
    }
}
