using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SessionPosts
{
    public string Key;
    public List<ScriptableObject> PostClip;
}

public class GroupPostAdder : MonoBehaviour
{
    public string PostKey;
    public bool Add;

    public CommunityPostManager[] OnDutyManagers;
    public List<SessionPosts> PostClip;

    private Dictionary<string, List<ScriptableObject>> postClipDic;

    private void Start()
    {
        ResetRandClip();
    }
    private void Update()
    {
        if (Add)
        {
            Add = false;
            GroupAddPost(PostKey);
        }
    }

    public void ResetRandClip()
    {
        postClipDic = new Dictionary<string, List<ScriptableObject>>();
        for (int i = 0; i < PostClip.Count; i++)
        {
            postClipDic.Add(PostClip.ElementAt(i).Key, PostClip.ElementAt(i).PostClip);
        }
    }
    public void GroupAddPost(string _key)
    {
        var _clip = postClipDic[_key];
        for (int i = 0; i < OnDutyManagers.Length; i++)
        {
            var _manager = OnDutyManagers[i];
            for (int j = 0; j < _clip.Count; j++)
            {
                SCO_Post post = (SCO_Post)_clip[j];
                if (_manager.FindPostData(post.Key) != null)
                    continue;
                _manager.AddPost(post);
            }
            
        }
    }
}
