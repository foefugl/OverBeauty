using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class PhonePower : MonoBehaviour
{
    public Image BGPanel;
    public Button Btn;
    
    public void PowerOn()
    {
        Color targetColor = new Color(0, 0, 0, 0);
        Btn.interactable = false;
        Btn.image.DOColor(targetColor, 0.5f)
            .OnComplete(() =>
        {
            //BGPanel.gameObject.SetActive(false);
            Btn.gameObject.SetActive(false);
        });
        BGPanel.DOColor(targetColor, 2.5f)
            .OnComplete(() =>
            {
                BGPanel.gameObject.SetActive(false);
                ///Btn.gameObject.SetActive(false);
            });
            
    }
    private void Start()
    {
        Btn.image.DOColor(Color.white,1).SetLoops(100,LoopType.Yoyo) ;
        
    }
}
