using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Yarn.Unity;

public class StoryPipeOrder : MonoBehaviour
{
    private static StoryPipeOrder instance;
    public static StoryPipeOrder Instance 
    {
        get 
        {
            return instance;
        }
    }
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }
    public ScriptableObject FeyInsultPost;
    public ScriptableObject Mission1Post;
    public ScriptableObject Mission2Post;
    public ScriptableObject Mission3Post;
    public ScriptableObject Mission4Post;
    public ScriptableObject Mission5Post;
    public ScriptableObject FriendChatRoom;
    public ScriptableObject UnknownMesRoom;
    public ScriptableObject FeyFeyMesRoom;
    public ScriptableObject ChicPost;
    public KeywordData Mission5Keyword;
    public bool FeyInsultTrigger_1, FeyInsultTrigger_2;

    public bool TestBool;
    private WinkMessageManager winkMessageManager;

    private void Update()
    {
        if (TestBool)
        {
            TestBool = false;
            AddFeyMesRoom();
        }
    }

    public void Receive1210Message()
    {
        StartCoroutine(DelayAction(1, () =>
        {
            GamePlayTime.Instance.DateTxt.text = "12/10";

            DialogPassCatchRunner landlordDialogue = GameObject.Find("房東太太").GetComponent<DialogPassCatchRunner>();
            landlordDialogue.StartDialogueAtNode("Start_2");
            DialogPassCatchRunner bossDialogue = GameObject.Find("店長").GetComponent<DialogPassCatchRunner>();
            bossDialogue.StartDialogueAtNode("Start_2");

            winkMessageManager = FindObjectOfType<WinkMessageManager>();
            winkMessageManager.AddPost(FriendChatRoom); 
        }));
    }
    [YarnCommand("AddFeyInsultPost")]
    public void AddFeyInsultPost(string triggerName)
    {
        if (triggerName == "FeyInsultTrigger_1")
        {
            FeyInsultTrigger_1 = true;
        }
        else if (triggerName == "FeyInsultTrigger_2")
        {
            FeyInsultTrigger_2 = true;
        }

        if (FeyInsultTrigger_1 && FeyInsultTrigger_2)
        {
            StartCoroutine(DelayAction(5, () => 
            {
                WinkClubPostManager winkClubPost = GameObject.Find("仙仙騎士團").GetComponent<WinkClubPostManager>();

                WinkPostsManager winkPostsManager = FindObjectOfType<WinkPostsManager>();
                GameObject post = winkPostsManager.AddPost(FeyInsultPost);
                PostPointerClickEvent postPointer = post.AddComponent<PostPointerClickEvent>();
                postPointer.ClickEvent = () =>
                {
                    CutSceneManager.Instance.PlayCh1Video();
                };
                for (int i = 0; i < post.transform.childCount; i++)
                {
                    PostPointerClickEvent _pointer = post.transform.GetChild(i).gameObject.AddComponent<PostPointerClickEvent>();
                    _pointer.ClickEvent = () =>
                    {
                        CutSceneManager.Instance.PlayCh1Video();
                    };
                }
            }));
        }
    }
    [YarnCommand("InstallApp")]
    public void InstallApp()
    {
        winkMessageManager = FindObjectOfType<WinkMessageManager>();
        winkMessageManager.AddClickEventOnReply(() => 
        {
            AppInstaller.Instance.InstallApp(AppPanelName.ProbearHome, () => 
            {
                AddMission_34();
                RegistProbearMission4_EnterEvent();
            });
        });
    }
    private void AddMission_34()
    {
        StartCoroutine(DelayAction(2.25f, () =>
        {
            MissionManager.Instance.AddPost(Mission3Post);
            MissionManager.Instance.AddPost(Mission4Post);
        }));
        
    }
    private void RegistProbearMission4_EnterEvent()
    {
        var currentPBPages = FindObjectsOfType<PBTitleUIInstance>();
        currentPBPages[0].EnterPageEvent.AddListener(() =>
        {
            MissionManager.Instance.MissionCompleted("SCO_mMission_M4", 1);
        });
        currentPBPages[1].EnterPageEvent.AddListener(() =>
        {
            MissionManager.Instance.MissionCompleted("SCO_mMission_M4", 2);
            MissionManager.Instance.AddPost(Mission5Post);
            DialogPassCatchRunner friendADialogue = GameObject.Find("好友A").GetComponent<DialogPassCatchRunner>();
            friendADialogue.StartDialogueAtNode("Start_3");
        });
        currentPBPages[2].EnterPageEvent.AddListener(() =>
        {
            MissionManager.Instance.MissionCompleted("SCO_mMission_M4", 3);
        });
    }
    public void AddUnknownMesRoom()
    {
        winkMessageManager = FindObjectOfType<WinkMessageManager>();
        winkMessageManager.AddPost(UnknownMesRoom);
    }
    [YarnCommand("AddFeyMesRoom")]
    public void AddFeyMesRoom()
    {
        winkMessageManager = FindObjectOfType<WinkMessageManager>();
        winkMessageManager.AddPost(FeyFeyMesRoom);
    }
    [YarnCommand("StartFriendA_Start2")]
    public void StartFriendA_Start2() 
    {
        DialogPassCatchRunner friendADialogue = GameObject.Find("好友A").GetComponent<DialogPassCatchRunner>();
        friendADialogue.StartDialogueAtNode("Start_2");
    }
    [YarnCommand("AddChicPost")]
    public void AddChicPost()
    {
        WinkPostsManager winkPostsManager = FindObjectOfType<WinkPostsManager>();
        GameObject post = winkPostsManager.AddPost(ChicPost);
    }
    [YarnCommand("AddMission_1")]
    public void AddMission_1()
    {
        MissionManager.Instance.AddPost(Mission1Post);
    }
    [YarnCommand("Test")]
    public void Test()
    {
        Debug.Log("Test");
    }
    [YarnCommand("AddMission_2")]
    public void AddMission_2()
    {
        MissionManager.Instance.AddPost(Mission2Post);
    }
    [YarnCommand("PlaceMission5KeywordAtFriendMes")]
    public void PlaceMission5KeywordAtFriendMes()
    {
        WinkMessageManager winkMessage = FindObjectOfType<WinkMessageManager>();
        winkMessage.CurrentKeywordData = Mission5Keyword;
    }
    IEnumerator DelayAction(float _delay, Action _action)
    {
        yield return new WaitForSeconds(_delay);
        _action?.Invoke();
    }
}
