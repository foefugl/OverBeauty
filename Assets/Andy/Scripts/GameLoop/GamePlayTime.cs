using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Yarn.Unity;

[System.Serializable]
public class DateTime
{
    public int Year;
    public int Month;
    public int Day;
    public int Hour;
    public int Minute;
    public int Second;
}

public class GamePlayTime : MonoBehaviour
{
    private static GamePlayTime instance;
    public static GamePlayTime Instance { get { return instance; } }

    public bool TimeFreeze;
    public SCO_ValueConfigure ValueConfig;
    public DateTime DateTime;
    public Text DateTxt;
    public Text TimeTxt;

    [Space(10)]
    public UnityEvent MinutePass, HourPass, DayPass, WeekPass;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            //DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(this);
        }
    }
    private void Start()
    {
        StartCoroutine(Counting());
    }

    private void Update()
    {
        DateTxt.text = string.Format("{1}/{2}", DateTime.Year, DateTime.Month, DateTime.Day);
        string minuteString = (DateTime.Minute < 10) ? "0" + DateTime.Minute.ToString() : DateTime.Minute.ToString();
        string hourString = (DateTime.Hour < 10) ? "0" + DateTime.Hour.ToString() : DateTime.Hour.ToString();
        TimeTxt.text = string.Format("{0}:{1}", hourString, minuteString);
    }

    public void SetFreeze(bool flag)
    {
        TimeFreeze = flag;
    }
    IEnumerator Counting()
    {
        float counter = 0;

        while (true)
        {
            if (!TimeFreeze)
            {
                counter += Time.deltaTime * ValueConfig.GameTimeSpeed;
                DateTime.Second = Mathf.FloorToInt(counter);

                if (DateTime.Second >= 60)
                {
                    DateTime.Minute++;
                    DateTime.Second = 0;
                    counter = 0;

                    MinutePass?.Invoke();
                }
                if (DateTime.Minute >= 60)
                {
                    DateTime.Hour++;
                    DateTime.Minute = 0;

                    HourPass?.Invoke();
                }
                if (DateTime.Hour >= 24)
                {
                    DateTime.Day++;
                    DateTime.Hour = 0;
                    DayPass.Invoke();
                }

                if (DateTime.Day % 7 == 0)
                {
                    WeekPass?.Invoke();
                }

                if (DateTime.Day >= 31)
                {
                    DateTime.Month++;
                    DateTime.Day = 1;
                }
                if (DateTime.Month >= 13)
                {
                    DateTime.Year++;
                    DateTime.Month = 1;
                }
            }

            yield return null;
        }
    }
}
