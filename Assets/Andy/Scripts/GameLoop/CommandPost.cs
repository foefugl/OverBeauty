using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CommandPost : MonoBehaviour
{
    public CommunityPostManager OnDutyPostManager;
    public ScriptableObject[] PostClip;

    private Dictionary<string, ScriptableObject> postDic;
    private List<ScriptableObject> postList;
    // Start is called before the first frame update
    protected virtual void Initial()
    {
        postDic = new Dictionary<string, ScriptableObject>();
        for (int i = 0; i < PostClip.Length; i++)
        {
            SCO_Post postData = (SCO_Post)PostClip[i];
            postDic.Add(postData.Key, postData);
        }
    }

    public void PostAdd(string _key)
    {
        if (!postDic.ContainsKey(_key))
        {
            Debug.LogError(string.Format("CommandPost don't have {0} post already.", _key));
            return;
        }   
        OnDutyPostManager.AddPost(postDic[_key]);
        postDic.Remove(_key);
    }
}
