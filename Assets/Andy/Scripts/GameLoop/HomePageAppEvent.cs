using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HomePageAppEvent : MonoBehaviour
{
    public AppPanelName AppType;
    public void OpenApp()
    {
        BaseUnderToolBar.Instance.OpenApp(AppType);
    }
}
