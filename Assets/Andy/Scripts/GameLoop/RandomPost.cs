using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class RandCreatePost
{
    public bool Constant;
    public ScriptableObject Sco;
}

public enum TimeCountType
{
    Minute,
    Hour,
    Day,
    Year
}

public class RandomPost : MonoBehaviour
{
    [SerializeField] TimeCountType countType;
    public int countLimit;
    public CommunityPostManager[] OnDutyManagers;
    public List<RandCreatePost> PostClip;

    private List<ScriptableObject> clip;
    private int count;

    private void Start()
    {
        ResetRandClip();
        switch (countType)
        {
            case TimeCountType.Minute:
                GamePlayTime.Instance.MinutePass.AddListener(ActiveCounter);
                break;
            case TimeCountType.Hour:
                GamePlayTime.Instance.HourPass.AddListener(ActiveCounter);
                break;
            case TimeCountType.Day:
                GamePlayTime.Instance.DayPass.AddListener(ActiveCounter);
                break;
            case TimeCountType.Year:
                break;
            default:
                break;
        }
    }

    public void ActiveCounter()
    {
        if (clip == null)
        {
            return;
        }
        if (clip.Count == 0)
        {
            return;
        }

        count++;
        if (count >= countLimit)
        {
            count = 0;
            RandAdd();
        }
    }
    public void DeleteAllExistRandPost()
    {
        for (int i = 0; i < PostClip.Count; i++)
        {
            if (PostClip[i].Constant)
                continue;
            ManagersRemovePost(PostClip[i].Sco);
        }
    }
    public void ResetRandClip()
    {
        clip = new List<ScriptableObject>();
        for (int i = 0; i < PostClip.Count; i++)
        {
            clip.Add(PostClip[i].Sco);
        }
    }
    protected virtual void RandAdd()
    {
        if (clip.Count <= 0)
            return;

        int randindex = Random.Range(0, clip.Count);
        SCO_Post post = (SCO_Post)clip[randindex];

        ManagersAddPost(post);
        clip.Remove(clip[randindex]);
    }
    private void ManagersAddPost(SCO_Post _post)
    {
        for (int i = 0; i < OnDutyManagers.Length; i++)
        {
            var _manager = OnDutyManagers[i];
            if (_manager.FindPostData(_post.Key) == null)
            {
                _manager.AddPost(_post);
            }
        }
    }
    private void ManagersRemovePost(ScriptableObject _sco)
    {
        for (int i = 0; i < OnDutyManagers.Length; i++)
        {
            var _manager = OnDutyManagers[i];
            _manager.RemovePost(_sco);
        }
    }
}
