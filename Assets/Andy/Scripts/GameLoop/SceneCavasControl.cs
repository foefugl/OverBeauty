using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class SceneCavasControl : MonoBehaviour
{
    public string sceneName;
    [SerializeField] Canvas Canvas;
    [SerializeField] AppsUIController appUIPageControl;
    public UnityEvent FirstOpen;
    private bool firstOpen = true;

    public AppPanelName GetCurrentPage()
    {
        return appUIPageControl.CurrentPage;
    }
    public void SwitchPage(AppPanelName targetApp)
    {
        appUIPageControl.SwitchApp((int)targetApp);
    }

    public void SetCanvasOrder(int _order)
    {
        Canvas.sortingOrder = _order;
        if (firstOpen && _order == 1)
        {
            firstOpen = false;
            FirstOpen?.Invoke();
        }
    }
}
