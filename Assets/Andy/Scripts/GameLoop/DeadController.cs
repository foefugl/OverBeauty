using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Yarn.Unity;

public class DeadController : MonoBehaviour
{
    public BasicValueNotifyPanel valuePanel;
    public InMemoryVariableStorage MemoryVariableStorage;
    public FadeManager fadeManager;
    public PlayerPostBag playerPostBag;
    private RandomPost[] randPostManagers;

    public void ReLive()
    {
        StartCoroutine(ReLiveCoroutine());
    }
    public void FakeItemSetManHuntToZero()
    {
        valuePanel.CloseDeadPanel();
        MemoryVariableStorage.SetValue("$manhunt", 0);
        GamePlayTime.Instance.TimeFreeze = false;
    }
    IEnumerator ReLiveCoroutine()
    {
        fadeManager.Run();

        yield return new WaitForSeconds(2);

        MemoryVariableStorage.SetValue("$credibility", 0);
        MemoryVariableStorage.SetValue("$manhunt", 0);
        valuePanel.CloseDeadPanel();

        WinkPostsManager winkPostsManager = FindObjectOfType<WinkPostsManager>();
        winkPostsManager.RemoveAllPost();

        yield return new WaitForSeconds(2);

        transform.parent.gameObject.SetActive(false);
        fadeManager.Run();
        GamePlayTime.Instance.TimeFreeze = false;

        randPostManagers = FindObjectsOfType<RandomPost>();

        for (int i = 0; i < randPostManagers.Length; i++)
        {
            randPostManagers[i].ResetRandClip();
        }
    }
}
