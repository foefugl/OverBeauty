using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Yarn.Unity;

public class ValueConfigurator : MonoBehaviour
{
    public SCO_ValueConfigure sCO_Values;
    public InMemoryVariableStorage VariableStorage;

    private Dictionary<string, PostAffectValue> valueDic;

    private void Start()
    {
        DataInitial();
    }
    private void DataInitial()
    {
        valueDic = new Dictionary<string, PostAffectValue>();
        sCO_Values.Credit = VariableStorage.GetValue("$credibility").AsNumber;
        sCO_Values.ManHunt = VariableStorage.GetValue("$manhunt").AsNumber;
        sCO_Values.Money = VariableStorage.GetValue("$money").AsNumber;

        for (int i = 0; i < sCO_Values.PostAffectValues.Length; i++)
        {
            valueDic.Add(sCO_Values.PostAffectValues[i].Name, sCO_Values.PostAffectValues[i]);
        }
    }
    public void SetStorageValue(string key, float value)
    {
        VariableStorage.SetValue(key, value);
    }

    [YarnCommand("DialogSetValue")]
    public void DialogSetValue(string _name, string _affectValue)
    {
        float currentValue = VariableStorage.GetValue(valueDic[_name].YarnKey).AsNumber;
        float affectValue = float.Parse(_affectValue);
        float valueResult = affectValue * valueDic[_name].Multiplier + valueDic[_name].BottomValue;
        currentValue += valueResult;
        VariableStorage.SetValue(valueDic[_name].YarnKey, currentValue);

        if (valueDic[_name].YarnKey == "$credibility")
        {
            sCO_Values.Credit = currentValue;
        }
        else if(valueDic[_name].YarnKey == "$manhunt")
        {
            sCO_Values.ManHunt = currentValue;
        }
        else if (valueDic[_name].YarnKey == "$money")
        {
            sCO_Values.Money = currentValue;
        }
    }
}
