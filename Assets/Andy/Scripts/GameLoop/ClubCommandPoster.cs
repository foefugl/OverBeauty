using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClubCommandPoster : CommandPost
{
    public SCO_WinkClubRoom OnDutyClub;

    private void Start()
    {
        StartCoroutine(DelayInitial());
    }
    protected override void Initial()
    {
        base.Initial();
        var clubManagers = GetComponentsInChildren<WinkClubPostManager>();
        for (int i = 0; i < clubManagers.Length; i++)
        {
            if (clubManagers[i].ClubRoomData == OnDutyClub)
            {
                OnDutyPostManager = clubManagers[i];
            }
        }
    }
    IEnumerator DelayInitial()
    {
        yield return new WaitForSeconds(1);
        Initial();
    }
}
