using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class VerticalScrollSetPos : MonoBehaviour
{
    public VerticalLayoutGroup verticalLayout;
    public ScrollRect scroll;
    [Range(0, 1)]
    public float bar;
    private Tween twn;

    public void StartScrollToBottom()
    {
        StartCoroutine(CalculateLayoutWaitFrame());
    }

    private void ScrollToBottom()
    {
        float value = scroll.verticalNormalizedPosition;
        twn = DOTween.To(() => value, x => value = x, 0, 0.5f)
            .OnUpdate(() => 
            {
                scroll.verticalNormalizedPosition = value;
            });
    }
    IEnumerator CalculateLayoutWaitFrame()
    {
        yield return null;
        ScrollToBottom();
        verticalLayout.spacing = -1;
        yield return null;
        verticalLayout.spacing = 0;
    }
}
