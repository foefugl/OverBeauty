using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteAlways]
public class AdjustRtSizeByTarget : MonoBehaviour
{
    public RectTransform targetRt;
    public Vector2 mergin;
    public bool followH, followW;
    private RectTransform rt;

    // Start is called before the first frame update
    void Start()
    {
        rt = GetComponent<RectTransform>();
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 targetValue = rt.sizeDelta;

        if (followW)
        {
            targetValue.x = targetRt.sizeDelta.x + mergin.x;
        }
        if (followH)
        {
            targetValue.y = targetRt.sizeDelta.y + mergin.y;
        }
        rt.sizeDelta = targetValue;
    }
}
