using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class RtSizeFitText : MonoBehaviour
{
    public RectTransform rt;
    public TextMeshProUGUI text;
    public bool FollowW, FollowH;

    private void Update()
    {
        if (text == null)
        {
            text = GetComponent<TextMeshProUGUI>();
        }
        if (rt == null)
        {
            rt = GetComponent<RectTransform>();
        }

        float xSize = (FollowW) ? text.preferredWidth : rt.sizeDelta.x;
        float ySize = (FollowH) ? text.preferredHeight : rt.sizeDelta.y;
        rt.sizeDelta = new Vector2(xSize, ySize);
    }
}
