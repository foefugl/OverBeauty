using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class FoldTextField : MonoBehaviour
{
    public RectTransform PostPanel;
    public Text txt;
    public RectTransform MoreBtn;
    public float WidthLimit, HeightLimit;
    public bool Adjust;

    private RectTransform rt;
    private Vector2 oriSacle;
    private bool needMoreBtn;

    private void Start()
    {
        rt = transform.GetComponent<RectTransform>();
        oriSacle = txt.rectTransform.sizeDelta;

        if (txt.preferredHeight > HeightLimit)
        {
            Debug.Log("> HeightLimit");
            AdjustRect();
            LocateMoreButton();

            txt.rectTransform.sizeDelta = oriSacle;
        }
        else 
        {
            MoreBtn.gameObject.SetActive(false);
            float merginSize = txt.preferredHeight - rt.sizeDelta.y;
            //PostPanel.sizeDelta = new Vector2(PostPanel.sizeDelta.x, PostPanel.sizeDelta.y - merginSize);
            rt.sizeDelta = new Vector2(WidthLimit, txt.preferredHeight + 2);
        }
    }

    private void Update()
    {
        if (Adjust)
        {
            Adjust = false;
            AdjustRect();
        }
    }
    public void AdjustRect()
    {
        if (txt.preferredHeight > HeightLimit)
        {
            float merginSize = txt.preferredHeight - rt.sizeDelta.y;
            //PostPanel.sizeDelta = new Vector2(PostPanel.sizeDelta.x, PostPanel.sizeDelta.y + merginSize);
            rt.sizeDelta = new Vector2(WidthLimit, txt.preferredHeight);
        }
        if (needMoreBtn)
            MoreBtn.gameObject.SetActive(false);
    }
    private void LocateMoreButton()
    {
        Canvas.ForceUpdateCanvases();
        int lineCount = txt.cachedTextGenerator.lines.Count;
        if (lineCount > 2)
        {
            needMoreBtn = true;
        }

        for (int i = 0; i < lineCount; i++)
        {
            int startIndex = txt.cachedTextGenerator.lines[i].startCharIdx;
            int endIndex = (i == txt.cachedTextGenerator.lines.Count - 1) ? txt.text.Length
                : txt.cachedTextGenerator.lines[i + 1].startCharIdx;
            int length = endIndex - startIndex;

            string message = txt.text.Substring(startIndex, length);
            float lineWidth = CalculateLengthOfChar(message);

            if (needMoreBtn && i == 1)
                PlaceMoreBtn(lineWidth);
        }
    }
    private float CalculateLengthOfChar(string _message)
    {
        GameObject msgTemp = new GameObject();
        Text txtTemp = msgTemp.AddComponent<Text>();
        txtTemp.font = txt.font;
        txtTemp.fontSize = txt.fontSize;
        txtTemp.fontStyle = txt.fontStyle;
        txtTemp.text = _message;

        //Debug.Log(txtTemp.preferredWidth);
        StartCoroutine(DestroyTxtTempAfterOneFrame(msgTemp));

        return txtTemp.preferredWidth;
    }
    private void PlaceMoreBtn(float _lineWidth)
    {
        MoreBtn.gameObject.SetActive(true);
        MoreBtn.anchoredPosition = new Vector2(_lineWidth, -HeightLimit);
    } 
    IEnumerator DestroyTxtTempAfterOneFrame(GameObject _target)
    {
        yield return null;
        Destroy(_target);
    }
}
