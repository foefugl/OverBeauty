using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PhotoAdjustWithAsset : MonoBehaviour
{
    public Vector2 maxSize;
    public bool InAsset;
    private RectTransform rt;
    private Image image;

    // Start is called before the first frame update
    void Start()
    {
        rt = GetComponent<RectTransform>();
        image = GetComponent<Image>();

        SizeUpdate();
    }
    private void Update()
    {
        SizeUpdate();
    }
    private void SizeUpdate()
    {
        if (image.sprite == null)
        {
            rt.sizeDelta = Vector2.zero;
            return;
        }

        float x = image.mainTexture.width;
        float y = image.mainTexture.height;

        if (InAsset)
        {
            if (image.mainTexture.width > image.mainTexture.height)
            {
                y = maxSize.y;
                x = x * (y / image.mainTexture.height);
            }
            else
            {

                x = maxSize.x;
                y = y * (x / image.mainTexture.width);
            }
        }
        else
        {
            if (image.mainTexture.width > image.mainTexture.height)
            {
                if (x >= maxSize.x)
                    x = maxSize.x;
                y = y * (x / image.mainTexture.width);

            }
            else
            {
                if (y >= maxSize.y)
                    y = maxSize.y;

                x = x * (y / image.mainTexture.height);
            }
        }

        rt.sizeDelta = new Vector2(x, y);
    }
}
