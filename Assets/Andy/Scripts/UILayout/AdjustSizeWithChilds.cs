using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AdjustSizeWithChilds : MonoBehaviour
{
    public bool updatePerFrame;
    public VerticalLayoutGroup VerticalLayout;
    public GridLayoutGroup GridLayout;

    private RectTransform rt;
    private RectTransform[] childsRt;
    private int ChildCountInspect;

    private void Start()
    {
        rt = GetComponent<RectTransform>();
        VerticalLayout = GetComponent<VerticalLayoutGroup>();
        GridLayout = GetComponent<GridLayoutGroup>();
        childsRt = new RectTransform[rt.childCount];
        ChildCountInspect = rt.childCount;
        for (int i = 0; i < childsRt.Length; i++)
        {
            childsRt[i] = rt.GetChild(i).GetComponent<RectTransform>();
        }

        StartCoroutine(AdjustRectHeightAtEndOfFirstFrame());
    }

    private void LateUpdate()
    {
        if (!updatePerFrame)
            return;

        childsRt = new RectTransform[rt.childCount];
        float height = 0;

        if (GridLayout != null)
        {
            int validIndex = 0;
            for (int i = 0; i < childsRt.Length; i = i + GridLayout.constraintCount)
            {
                childsRt[i] = rt.GetChild(i).GetComponent<RectTransform>();
                if (childsRt[i].gameObject.activeSelf)
                {
                    validIndex++;
                    height += childsRt[i].sizeDelta.y;
                }
            }
            int pairCount = Mathf.CeilToInt(validIndex / GridLayout.constraintCount) + (validIndex % GridLayout.constraintCount);
            height += (pairCount * GridLayout.spacing.y);
            height += GridLayout.padding.bottom;
        }
        else if (VerticalLayout != null)
        {
            int validIndex = 0;
            for (int i = 0; i < childsRt.Length; i++)
            {
                childsRt[i] = rt.GetChild(i).GetComponent<RectTransform>();
                if (childsRt[i].gameObject.activeSelf)
                {
                    validIndex++;
                    height += childsRt[i].sizeDelta.y;
                }
            }
            int pairCount = Mathf.CeilToInt(validIndex / 2);
            height += (pairCount * VerticalLayout.spacing);
            height += VerticalLayout.padding.bottom;
        }
        else
        {
            for (int i = 0; i < childsRt.Length; i++)
            {
                int validIndex = 0;
                childsRt[i] = rt.GetChild(i).GetComponent<RectTransform>();
                if (childsRt[i].gameObject.activeSelf)
                {
                    validIndex++;
                    height += childsRt[i].sizeDelta.y;
                }
            }
        }

        rt.sizeDelta = new Vector2(rt.sizeDelta.x, height);
    }
    public void ReSize()
    {
        StartCoroutine(AdjustRectHeightAtEndOfFirstFrame());
    }
    private IEnumerator AdjustRectHeightAtEndOfFirstFrame()
    {
        yield return null;
        yield return null;
        yield return null;

        rt = GetComponent<RectTransform>();
        childsRt = new RectTransform[rt.childCount];
        float height = 0;

        if (GridLayout != null)
        {
            int validIndex = 0;
            for (int i = 0; i < childsRt.Length; i = i + GridLayout.constraintCount)
            {
                childsRt[i] = rt.GetChild(i).GetComponent<RectTransform>();
                if (childsRt[i].gameObject.activeSelf)
                {
                    validIndex++;
                    height += childsRt[i].sizeDelta.y;
                }
            }
            int pairCount = Mathf.CeilToInt(validIndex / GridLayout.constraintCount) + (validIndex % GridLayout.constraintCount);
            height += (pairCount * GridLayout.spacing.y);
            height += GridLayout.padding.bottom;
        }
        else if (VerticalLayout != null)
        {
            int validIndex = 0;
            for (int i = 0; i < childsRt.Length; i ++)
            {
                childsRt[i] = rt.GetChild(i).GetComponent<RectTransform>();
                if (childsRt[i].gameObject.activeSelf)
                {
                    validIndex++;
                    height += childsRt[i].sizeDelta.y;
                }
            }
            int pairCount = Mathf.CeilToInt(validIndex / 2);
            height += (pairCount * VerticalLayout.spacing);
            height += VerticalLayout.padding.bottom;
        }
        else
        {
            for (int i = 0; i < childsRt.Length; i ++)
            {
                childsRt[i] = rt.GetChild(i).GetComponent<RectTransform>();
                if (childsRt[i].gameObject.activeSelf)
                {
                    height += childsRt[i].sizeDelta.y;
                }
            }
        }

        rt.sizeDelta = new Vector2(rt.sizeDelta.x, height);
    }
}
