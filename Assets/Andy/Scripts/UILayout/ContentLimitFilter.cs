using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.EventSystems;

public class ContentLimitFilter : MonoBehaviour, IPointerClickHandler
{
    public Vector2 Limit;
    public Vector2 Mergin = new Vector2(0, 25);
    public bool Set_HLimit, Set_VLimit;
    public bool MessageMode;
    private bool expand;
    private RectTransform rt;
    private TextMeshProUGUI text;

    public void OnPointerClick(PointerEventData eventData)
    {
        expand = true;
    }

    // Start is called before the first frame update
    void Start()
    {
        text = GetComponent<TextMeshProUGUI>();
        rt = GetComponent<RectTransform>();
        SizeAdjust();
    }

    // Update is called once per frame
    void Update()
    {
        SizeAdjust();
    }
    private void SizeAdjust()
    {
        Vector2 resizeValue = new Vector2(rt.sizeDelta.x, text.preferredHeight + Mergin.y);
        if (MessageMode)
        {
            resizeValue = new Vector2(((text.preferredWidth < Limit.x) ? text.preferredWidth : Limit.x), text.preferredHeight + Mergin.y);
            //resizeValue = new Vector2( text.preferredWidth, text.preferredHeight + Mergin.y);
        }

        if (!expand)
        {
            if (Set_HLimit && text.preferredWidth > Limit.x)
            {
                resizeValue.x = Limit.x;
            }
            if (Set_VLimit && text.preferredHeight > Limit.y)
            {
                resizeValue.y = Limit.y;
            }
        }

        rt.sizeDelta = resizeValue;
    }
}
