using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PCBuildResolution : MonoBehaviour
{
    private void Awake()
    {
#if UNITY_STANDALONE
         Screen.SetResolution(564, 960, false);
         Screen.fullScreen = false;
#endif
    }
}
