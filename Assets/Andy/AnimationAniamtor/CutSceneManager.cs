using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.UI;

[System.Serializable]
public class StoryVideo
{
    public string Key;
    public bool Finish;
    public VideoClip Clip;
}

public class CutSceneManager : MonoBehaviour
{
    private static CutSceneManager instance;
    public static CutSceneManager Instance 
    {
        get
        {
            return instance;
        }
    }
    public bool Running;
    public GameObject AniPanel;
    public VideoPlayer MoviePlayer;
    public StoryVideo[] StoryVideos;

    private Dictionary<string, StoryVideo> StoryVideosDic;
    private string currentVideoName;
    private Action MovieEndAction;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        StoryVideosDic = new Dictionary<string, StoryVideo>();
        for (int i = 0; i < StoryVideos.Length; i++)
        {
            StoryVideosDic.Add(StoryVideos[i].Key, StoryVideos[i]);
        }
    }
    public void Skip()
    {
        Debug.Log("Skip Video");
        if (!FadeManager.Instance.Visible)
        {
            FadeManager.Instance.Run();
        }
        MovieEndAction?.Invoke();
    }

    public void PlayIntroVideo()
    {
        if (StoryVideosDic["Intro_1"].Finish)
        {
            return;
        }
        currentVideoName = "Ch_1";
        MovieEndAction = () =>
        {
            FadeManager.Instance.FadeOutevent.AddListener(() =>
            {
                AniPanel.SetActive(false);
                FadeManager.Instance.FadeOutevent.RemoveAllListeners();
                BaseUnderToolBar.Instance.InitialLoadScenes();
                NotifyManager.Instance.DelayStartPopOut();
                StoryPipeOrder.Instance.Receive1210Message();
            });
        };

        PlayVideo("Intro_1", MovieEndAction);
    }
    public void PlayCh1Video()
    {
        if (StoryVideosDic["Ch_1"].Finish)
        {
            return;
        }
        currentVideoName = "Ch_1";
        FadeManager.Instance.Run();
        StartCoroutine(WaitFadeFinish_PlayCh1());
    }
    IEnumerator WaitFadeFinish_PlayCh1()
    {
        MovieEndAction = () =>
        {
            FadeManager.Instance.FadeOutevent.AddListener(() =>
            {
                AniPanel.SetActive(false);
                FadeManager.Instance.Run();
                FadeManager.Instance.FadeOutevent.RemoveAllListeners();

                GamePlayTime.Instance.gameObject.SetActive(true);
                GamePlayTime.Instance.enabled = true;
                GamePlayTime.Instance.TimeFreeze = false;

                StoryPipeOrder.Instance.AddUnknownMesRoom();
            });
        };
        yield return new WaitUntil(() => FadeManager.Instance.Visible);
        
        PlayVideo("Ch_1", MovieEndAction);
    }
    public void PlayVideo(string _videoName, Action _action = null)
    {
        FadeManager.Instance.Run();
        AniPanel.SetActive(true);
        StoryVideosDic[_videoName].Finish = true;
        MoviePlayer.clip = StoryVideosDic[_videoName].Clip;
        _action?.Invoke();
    }
}
