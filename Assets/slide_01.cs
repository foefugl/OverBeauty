﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class slide_01 : MonoBehaviour, IEndDragHandler, IDragHandler, IBeginDragHandler
{
    // Start is called before the first frame update

    [Header("Content元件")]
    public GameObject content;

    public Image page;
    public Sprite two;
    public Sprite one;

    //節點數量
    int nodeCount = 0;
    //節點寬度/切換長度
    float nodeWidth = 0;

    //切換速度
    [Header("切換速度")]
    public float speed = 12;

    //當前所在節點
    [SerializeField]
    int curNode = 0;
    //目標節點
    int targetNode = 0;

    //當前觸控座標
    float curTouchPos = 0;
    //上一幀觸控座標
    float lastTouchPos = 0;
    //當前觸控狀態
    bool isTouch = false;

    //切換目標方向
    [SerializeField]
    bool switchTargetIsLeft = false;
    //當前偏移方向
    [SerializeField]
    bool offsetIsLeft = false;


    //是否到達目標
    bool isArrive = true;

    //切換瞬間的回撥
    public Action<int> switchCallback = null;

    //按下
    public void OnBeginDrag(PointerEventData eventData)
    {
        isTouch = true;
        lastTouchPos = eventData.position.x;
        curTouchPos = eventData.position.x;
    }
    //拖拽
    public void OnDrag(PointerEventData eventData)
    {
        curTouchPos = eventData.position.x;

        //設定滑動方向
        if (lastTouchPos - curTouchPos < 0)
        {
            switchTargetIsLeft = true;
        }

        else if (lastTouchPos - curTouchPos > 0)
        {
            switchTargetIsLeft = false;
        }


        lastTouchPos = eventData.position.x;
    }
    //鬆開
    public void OnEndDrag(PointerEventData eventData)
    {


        isTouch = false;

        //設為未到達
        isArrive = false;

        //更新節點資訊
        UpdateNodeData();

        //判斷當前節點偏移方向
        if (content.GetComponent<RectTransform>().anchoredPosition.x * -1 < curNode * nodeWidth)//如果當前UI座標 < 當前節點座標
        {
            //offsetIsLeft = "L";
            offsetIsLeft = true;
        }
        else
        {
            //offsetIsLeft = "R";
            offsetIsLeft = false;
        }



        //如果目標方向和偏移方向一致
        if ((switchTargetIsLeft == offsetIsLeft) &&
            ((switchTargetIsLeft && (curNode - 1 >= 0)) || //且 朝左時沒有超過最小值
            (!switchTargetIsLeft && curNode + 1 <= nodeCount - 1)))//且 朝右時沒有超過最大值   
        {
            //設定目標節點
            targetNode = switchTargetIsLeft ? curNode - 1 : curNode + 1;

            //更新當前節點
            curNode = targetNode;

            //觸發回撥
            switchCallback?.Invoke(targetNode);
        }
        //否則回到原始位置
        else
        {
            //回到當前節點
            targetNode = curNode;
        }
    }

    //獲取當前節點數
    public int GetCurNodeCount()
    {
        //更新節點資訊
        UpdateNodeData();
        return curNode;
    }
    //獲取總節點數
    public int GetMaxNodeCount()
    {
        //更新節點資訊
        UpdateNodeData();
        return nodeCount;
    }

    //前往指定節點
    public bool ToAppointNode(int appointNode)
    {
        //更新節點資訊
        UpdateNodeData();

        if (appointNode < 0 || appointNode >= nodeCount)
        {
            return false;
        }
        isArrive = false;

        //觸發回撥
        if (appointNode != curNode)
            switchCallback?.Invoke(appointNode);

        //設定目標節點
        targetNode = appointNode;
        //更新當前節點
        curNode = targetNode;

        return true;
    }



    //註冊回撥
    public void SubscribeCallback(Action<int> newCallback)
    {
        switchCallback = newCallback;
    }
    //清空回撥
    public void UnsubscribeCallback()
    {
        switchCallback = null;
    }


    //更新節點總數和每個節點的寬度
    void UpdateNodeData()
    {
        //獲取節點總數（所有子類）
        nodeCount = content.transform.childCount;
        //獲取每個節點的寬度
        nodeWidth = content.GetComponent<RectTransform>().rect.width / nodeCount;
    }


    void Update()
    {
        //如果 不在點選狀態 且 沒有到達目標節點
        if (!isTouch && !isArrive)
        {
            //前往目標節點
            Vector2 targetPos = new Vector2(targetNode * nodeWidth * -1, content.GetComponent<RectTransform>().anchoredPosition.y);


            content.GetComponent<RectTransform>().anchoredPosition = Vector2.Lerp(content.GetComponent<RectTransform>().anchoredPosition, targetPos, Time.deltaTime * speed);

            if (switchTargetIsLeft)
            {

            }


            //如果很靠近
            if (Mathf.Abs(content.GetComponent<RectTransform>().anchoredPosition.x - targetPos.x) <= 0.05)
                isArrive = true;
        }
        if (curNode==1)
        {
            page.sprite = two;
        }
        else
        {
            page.sprite = one;
        }

    }
}
