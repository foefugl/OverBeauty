using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class the_next_post : MonoBehaviour
{
    public float Time_run = 0f ;
    [Header("間隔的時間")]
    public int Time_stop ;//間隔的時間
    [Header("下一則貼文")]
    public GameObject next_post;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(Time.deltaTime);
        if (Time_run >= Time_stop)
        {
            CancelInvoke("time");
            next_post.SetActive(true);
        }
    }

    public void time()
    {
        Time_run += Time.deltaTime;
    }
    public void click()
    {
        InvokeRepeating("time", 0, Time.deltaTime);

    }

}
