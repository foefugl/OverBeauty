using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class AppsUIController_Sonia : MonoBehaviour
{
    public enum AppPanelName
    {
        WinkHome,
        WinkCommunity,
        Post,
        WinkMessager,
        PlayerInformation
    }

    public AppPanelName CurrentActiveApp = AppPanelName.WinkHome;
    public GameObject CoverBoard;

    [Serializable]
    public struct AppNameObjectPair
    {
        public AppPanelName Name;
        public GameObject AppObject;
        public UnityEvent OpenPanelEvent;
    }
    public AppNameObjectPair[] apps;

    private void OnValidate()
    {
        SwitchApp(CurrentActiveApp);
    }
    public void SwitchApp(int _appNameIndex)
    {
        SwitchApp((AppPanelName)_appNameIndex);
    }

    public void SwitchApp(AppPanelName _appName)
    {
        CoverBoard.transform.SetAsLastSibling();
        for (int i = 0; i < apps.Length; i++)
        {
            if (apps[i].Name == _appName)
            {
                apps[i].AppObject.transform.SetAsLastSibling();
                apps[i].OpenPanelEvent?.Invoke();
            }
        }
    }
}
