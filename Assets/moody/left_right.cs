using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class left_right : MonoBehaviour
{
    public GameObject all;
    public GameObject card;
    public GameObject black_card;
    public Text text_ans;
    private GameObject copy;
    public float a = 0;//判定時間
    bool b = false;//執行一次的東西
    public int card_number;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (card.transform.position.x > (Screen.width / 2) + 100)//右
        {
            //spri.color = Color.red;
            card.transform.DORotate(new Vector3(0, 0, -15), 0.5f);
            switch(card_number)
            {
                case 1:
                    text_ans.text = "當然是老媽";
                    break;
                case 2:
                    text_ans.text = "當然是聰明 金子到哪都會發亮!";
                    break;
                case 3:
                    text_ans.text = "其他人的性命和我無關";
                    break;
                case 4:
                    text_ans.text = "哇 我真的好驚訝";
                    break;
                case 5:
                    text_ans.text = "眼不見為淨";
                    break;
                case 6:
                    text_ans.text = "早餐要吃蛋餅還是三明治?";
                    break;
                case 7:
                    text_ans.text = "天崖何處無芳草 沒必要吊死在一棵樹上";
                    break;
                case 8:
                    text_ans.text = "哈囉 在叫我嗎?";
                    break;
                case 9:
                    text_ans.text = "快進攻啊!!";
                    break;
                case 10:
                    text_ans.text = "絕對是言語攻擊 還記得那年初夏，我在學校...(以下簡略一千字)";
                    break;

            }
            if (!Input.GetMouseButton(0))
            {
                right();
            }
        }
        else if (card.transform.position.x < (Screen.width / 2) - 100)//左
        {
            //spri.color = Color.green;
            card.transform.DORotate(new Vector3(0, 0, 15), 0.2f);
            switch (card_number)
            {
                case 1:
                    text_ans.text = "絕對救情婦";
                    break;
                case 2:
                    text_ans.text = "長得好看就可以讓全世界的聰明人為我打工拉~";
                    break;
                case 3:
                    text_ans.text = "我願意當唯一的犧牲者";
                    break;
                case 4:
                    text_ans.text = "你是很認真在反串嗎???";
                    break;
                case 5:
                    text_ans.text = "長臉上吧 還是身體要緊";
                    break;
                case 6:
                    text_ans.text = "你牙痛的時候，痛楚是在你的口裡，還是你的腦裡？";
                    break;
                case 7:
                    text_ans.text = "驀然回首發現你在燈火闌珊處 他會發現你的好的";
                    break;
                case 8:
                    text_ans.text = "有夢最美";
                    break;
                case 9:
                    text_ans.text = "真好 恭喜耶";
                    break;
                case 10:
                    text_ans.text = "過來人的經驗告訴我一定是身體疼痛";
                    break;

            }
            if (!Input.GetMouseButton(0))
            {
                left();
            }
        }
        else
        {
            //spri.color = Color.white;
            card.transform.DORotate(new Vector3(0, 0, 0), 0.2f);
        }
        if(a==3.5)
        {
            Destroy(all);
        }//刪除
        if(a==3)
        {
            text_ans.DOFade(0, 0.5f);
            black_card.transform.DOScaleX(0, 0.4f);
        }
    }
    public void left()
    {
        text_ans.transform.position = new Vector3(Screen.width / 2, Screen.height / 2 + 100, 0);
        card.transform.DOMoveX(-Screen.width, 1);
        switch (card_number)
        {
            case 1:
                text_ans.text = "萬惡淫為首! 連情婦都出現了 也太荒淫無度!";
                break;
            case 2:
                text_ans.text = "神答覆! 不能再更贊同了!";
                break;
            case 3:
                text_ans.text = "真是一個大聖人";
                break;
            case 4:
                text_ans.text = "你什麼意思!";
                break;
            case 5:
                text_ans.text = "雖然很難看  但很有道理";
                break;
            case 6:
                text_ans.text = "哲學但普通 我猜你沒有好好看題目";
                break;
            case 7:
                text_ans.text = "好! 我會繼續好好加油的!";
                break;
            case 8:
                text_ans.text = "唉! 會不會說話啊!";
                break;
            case 9:
                text_ans.text = "有甚麼好恭喜的!她男朋友也在我們這組!";
                break;
            case 10:
                text_ans.text = "看來我該依靠身體的疼痛來緩解我隱隱作痛的心......";
                break;

        }
        if (!b)
        {
            b = true;
            InvokeRepeating("gogo", 0, 0.5f);
            //Invoke("gogo", 2);//刪除的時間
            //copy = Instantiate(all, all.transform.position, all.transform.rotation, all.transform.parent);
        }
    }//方法左
    public void right()
    {
        text_ans.transform.position = new Vector3(Screen.width / 2 , Screen.height / 2 + 100, 0);
        switch (card_number)
        {
            case 1:
                text_ans.text = "百善孝為先 你真是優良典範";
                break;
            case 2:
                text_ans.text = "我可以肯定你會是一個孤獨的天才";
                break;
            case 3:
                text_ans.text = "也太自私了";
                break;
            case 4:
                text_ans.text = "對吧對吧 嚇死我了!";
                break;
            case 5:
                text_ans.text = "哇 你的答案和某個網美一模一樣耶!";
                break;
            case 6:
                text_ans.text = "天啊  你真是天才";
                break;
            case 7:
                text_ans.text = "你懂甚麼!她是我的女神!";
                break;
            case 8:
                text_ans.text = "野生帥哥嗎?給虧嗎?";
                break;
            case 9:
                text_ans.text = "好! 我絕對要從惡魔手中救下我的女神!";
                break;
            case 10:
                text_ans.text = "嗚嗚嗚嗚嗚  太難過了的故事了。希望你能好起來，然後拿硫酸......(以下簡略五百字)";
                break;

        }
        card.transform.DOMoveX(Screen.width*2, 1);
        if (!b)
        {            
            b = true;
            InvokeRepeating("gogo", 0, 0.5f);
            //copy = Instantiate(all, all.transform.position, all.transform.rotation, all.transform.parent);
        }
    }//方法右
    void gogo()
    {
        a += 0.5f;
    }
}
