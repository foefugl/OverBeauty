using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class left_right_English : MonoBehaviour
{
    public GameObject all;
    public GameObject card;
    public GameObject black_card;
    public Text text_ans;
    private GameObject copy;
    public float a = 0;//判定時間
    bool b = false;//執行一次的東西
    public int card_number;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (card.transform.position.x > (Screen.width / 2) + 100)//右
        {
            //spri.color = Color.red;
            card.transform.DORotate(new Vector3(0, 0, -15), 0.5f);
            switch(card_number)
            {
                case 1:
                    text_ans.text = "Of crourse is mommy!";
                    break;
                case 2:
                    text_ans.text = "Of course it is smart, gold shines everywhere!";
                    break;
                case 3:
                    text_ans.text = "Other people's lives is not my business.";
                    break;
                case 4:
                    text_ans.text = "Wow! I'm so surprise!";
                    break;
                case 5:
                    text_ans.text = "Out of sight, out of mind.";
                    break;
                case 6:
                    text_ans.text = "Do you want an quiche or a sandwich for breakfast?";
                    break;
                case 7:
                    text_ans.text = "Don't give up a forest for one tree.";
                    break;
                case 8:
                    text_ans.text = "Hi;) You wanted to see me?";
                    break;
                case 9:
                    text_ans.text = "Ask her out!!";
                    break;
                case 10:
                    text_ans.text = "Definitely is verbal attack. Remember that summer, I was at school...(1000 words are omitted below......)";
                    break;

            }
            if (!Input.GetMouseButton(0))
            {
                right();
            }
        }
        else if (card.transform.position.x < (Screen.width / 2) - 100)//左
        {
            //spri.color = Color.green;
            card.transform.DORotate(new Vector3(0, 0, 15), 0.2f);
            switch (card_number)
            {
                case 1:
                    text_ans.text = "It must be girlfriend!";
                    break;
                case 2:
                    text_ans.text = "Pretty face can make smart people all over the world work for me~";
                    break;
                case 3:
                    text_ans.text = "I am willing to be the only victim.";
                    break;
                case 4:
                    text_ans.text = "Are you concern troll???";
                    break;
                case 5:
                    text_ans.text = "Face, body is more important.";
                    break;
                case 6:
                    text_ans.text = "When you have a toothache, is the pain in your mouth or in your head?";
                    break;
                case 7:
                    text_ans.text = "Suddenly turning my head,she's there instead where the lights glow dimly.";
                    break;
                case 8:
                    text_ans.text = "Having a dream is the most beautiful thing.";
                    break;
                case 9:
                    text_ans.text = "Niiiice,congratulations.";
                    break;
                case 10:
                    text_ans.text = "Experience tells me it must be physical pain.";
                    break;

            }
            if (!Input.GetMouseButton(0))
            {
                left();
            }
        }
        else
        {
            //spri.color = Color.white;
            card.transform.DORotate(new Vector3(0, 0, 0), 0.2f);
        }
        if(a==3.5)
        {
            Destroy(all);
        }//刪除
        if(a==3)
        {
            text_ans.DOFade(0, 0.5f);
            black_card.transform.DOScaleX(0, 0.4f);
        }
    }
    public void left()
    {
        text_ans.transform.position = new Vector3(Screen.width / 2, Screen.height / 2 + 100, 0);
        card.transform.DOMoveX(-Screen.width, 1);
        switch (card_number)
        {
            case 1:
                text_ans.text = "How daren you to give up your mom?";
                break;
            case 2:
                text_ans.text = "I can’t agree with you more!";
                break;
            case 3:
                text_ans.text = "You are really a great saint";
                break;
            case 4:
                text_ans.text = "What do you mean!";
                break;
            case 5:
                text_ans.text = "It's ugly, but it makes sense.";
                break;
            case 6:
                text_ans.text = "Meh,I guess you didn't read the question carefully.";
                break;
            case 7:
                text_ans.text = "OK! I will do my best!";
                break;
            case 8:
                text_ans.text = "Man!You are not being a good friend.";
                break;
            case 9:
                text_ans.text = "That's not a good thing!Her boyfriend is also in our group!";
                break;
            case 10:
                text_ans.text = "It seems that I should relieve my heartache with physical pain...";
                break;

        }
        if (!b)
        {
            b = true;
            InvokeRepeating("gogo", 0, 0.5f);
            //Invoke("gogo", 2);//刪除的時間
            //copy = Instantiate(all, all.transform.position, all.transform.rotation, all.transform.parent);
        }
    }//方法左
    public void right()
    {
        text_ans.transform.position = new Vector3(Screen.width / 2 , Screen.height / 2 + 100, 0);
        switch (card_number)
        {
            case 1:
                text_ans.text = "Filial piety is the foundation of all virtues.You set a good example for everyone.";
                break;
            case 2:
                text_ans.text = "I'm sure you will be a lonely genius.";
                break;
            case 3:
                text_ans.text = "You are too selfish!";
                break;
            case 4:
                text_ans.text = "Right, right? It scared me to death!";
                break;
            case 5:
                text_ans.text = "Wooow!Your answer is exactly the same as an influencer!";
                break;
            case 6:
                text_ans.text = "OMG! What's a genius you are!";
                break;
            case 7:
                text_ans.text = "You don't know anything!She is my goddess!";
                break;
            case 8:
                text_ans.text = "Hey handsome guy.Would u like to hang out with me?";
                break;
            case 9:
                text_ans.text = "OK! I definitely save my goddess from the devil!";
                break;
            case 10:
                text_ans.text = "Cry.So sad.Hope you can get better,then take sulfuric acid......(1000 words are omitted below......)";
                break;

        }
        card.transform.DOMoveX(Screen.width*2, 1);
        if (!b)
        {            
            b = true;
            InvokeRepeating("gogo", 0, 0.5f);
            //copy = Instantiate(all, all.transform.position, all.transform.rotation, all.transform.parent);
        }
    }//方法右
    void gogo()
    {
        a += 0.5f;
    }
}
