using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.EventSystems;

public class UNI_AI : MonoBehaviour, IDragHandler
{
    public RectTransform a;
    public Canvas canvas;
    public Vector3 myAI_POS;
    public float myDurationTime;
    public bool is_open=false;
    public GameObject speak;

    public UnityEvent OpenEvent, CloseEvent;
    // Start is called before the first frame update

    public void OnDrag(PointerEventData eventData)
    {
        a.anchoredPosition += eventData.delta / canvas.scaleFactor;
    }

    void Start()
    {
        a = GetComponent<RectTransform>();
        canvas = GameObject.Find("Canvas").GetComponent<Canvas>();
        myAI_POS = transform.position;
        WhenFingerUp();

    }
    public void WhenFingerUp()
    {
        //判斷 AI現在在 手機畫面左邊還是右邊
        if (myAI_POS.x > (Screen.width / 2)) {
            //C右邊
            transform.DOMove(new Vector3(Screen.width - 100, myAI_POS.y, 0), myDurationTime, true);
        }
        else {
            //C左邊
            transform.DOMove(new Vector3(100, myAI_POS.y, 0), myDurationTime, true);
            //transform.do
        }
    }

    // Update is called once per frame
    void Update()
    {
        myAI_POS = transform.position;
        if (Input.GetMouseButtonUp(0)&&is_open==false) 
        {
            WhenFingerUp();
        }
        if(is_open==true)
        {
            transform.DOMove(new Vector3(100, Screen.height - 200, 0), myDurationTime, true);
        }
    }
    
    public void open_ai()
    {
        is_open = true;
        speak.transform.DOScale(1,0.2f);
        OpenEvent?.Invoke();
    }
    public void close_ai()
    {
        is_open = false;
        speak.transform.DOScale(0, 0.2f);
        CloseEvent?.Invoke();
    }
}
