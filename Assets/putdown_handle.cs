using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.EventSystems;

public class putdown_handle : MonoBehaviour, IDragHandler
{
    public RectTransform a;
    public Canvas canvas;
    public Vector3 handle;
    public float myDurationTime;

    private bool forceFold;
    // Start is called before the first frame update

    public void OnDrag(PointerEventData eventData)
    {
        a.anchoredPosition += eventData.delta / canvas.scaleFactor;
        
    }

    void Start()
    {
        a = GetComponent<RectTransform>();
        handle = transform.position;
        WhenFingerUp();

    }
    public void WhenFingerUp()
    {
        if (handle.y > (Screen.height / 2 + 200))
        {
            //C�W��
            transform.DOMove(new Vector3(Screen.width / 2, Screen.height, 0), myDurationTime, true);
        }
        else
        {
            //C�U��
            transform.DOMove(new Vector3(Screen.width / 2, 0, 0), myDurationTime, true);
            //transform.do
        }
    }
    public void MoveUp()
    {
        forceFold = true;
        transform.DOMove(new Vector3(Screen.width / 2, Screen.height, 0), myDurationTime, true)
            .OnComplete(() => 
            {
                forceFold = false;
            });
    }

    // Update is called once per frame
    void Update()
    {
        if (forceFold)
        {
            return;
        }
        handle = transform.position;
        if (Input.GetMouseButtonUp(0))
        {
            WhenFingerUp();
        }
        a.anchoredPosition = new Vector2(0, a.anchoredPosition.y);
    }
}
