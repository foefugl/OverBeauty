using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class beauty_rad : MonoBehaviour
{
    public Rigidbody2D rad;
    public Camera cam;
    Vector2 mousePos;
    public GameObject koko;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        mousePos = cam.ScreenToWorldPoint(Input.mousePosition);
        if (Input.GetMouseButton(0))
        {
            koko.SetActive(true);
        }
        else
        {
            koko.SetActive(false);
        }


    }
    private void FixedUpdate()
    {
        Vector2 lookDir = mousePos - rad.position;
        float angle = Mathf.Atan2(lookDir.y, lookDir.x) * Mathf.Rad2Deg - 90f;
        rad.rotation = angle;
    }


}
